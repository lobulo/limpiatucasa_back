<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Hash;



class Worker extends Model
{

    public function getFullNameAttribute() {
        return "{$this->name} {$this->last_name}";
    }

    public function workingDays() {
        return $this->hasMany(WorkingDay::class);
    } 

    public function works(){
        return $this->belongsToMany(Work::class,'workers_works','worker_id','work_id');
    }

    public function getNextWork(){
        return $this->works()->where('work_status_id','=',1)->where('date','<',\Carbon\Carbon::now())->orderBy('date','DESC')->first();
    }

    public function generateUser($request){
        $new_user = new User();
        $new_user->email = $request->get('email');
        $new_user->name = $request->get('name');
        $new_user->phone = $request->get('phone');
        $new_user->role_id = 4;
        $new_user->password = Hash::make(env('DEFAULT_PASSWORD','ltc2018++'));
        $new_user->save();
    }

    //Get list of business day in the month
    public static function getWorkDays($month){
        $workdays = array();
        $type = CAL_GREGORIAN;
        if(intval($month) > 12 ) { 
            $year = strval(date('Y')+1);
            $month = strval( $month % 12 );
         }else {
             $year = date('Y');
             $month = strval( $month );
         }
        $day_count = cal_days_in_month($type, $month, $year); // Get the amount of days

        //loop through all days
        for ($i = 1; $i <= $day_count; $i++) {
                $date = $year.'-'.$month.'-'.$i; //format date
                $get_name = date('l', strtotime($date)); //get week day
                $day_name = substr($get_name, 0, 3); // Trim day name to 3 chars
                //if not a weekend add day to array
                if($day_name != 'Sun' && $day_name != 'Sat'){
                    $workdays[] = $date;
                }

        }
        return $workdays;
    }

    //Get list of business day in the month
    public static function getAllDays($month){
        $workdays = array();
        $type = CAL_GREGORIAN;
        if(intval($month) > 12 ) { 
            $year = strval(date('Y')+1);
            $month = strval( $month % 12 );
         }else {
             $year = date('Y');
             $month = strval( $month );
         }
        $day_count = cal_days_in_month($type, $month , $year); // Get the amount of days

        //loop through all days
        for ($i = 1; $i <= $day_count; $i++) {
                $date = $year.'-'.$month.'-'.$i; //format date
                $get_name = date('l', strtotime($date)); //get week day
                $day_name = substr($get_name, 0, 3); // Trim day name to 3 chars
                //if not a weekend add day to array
                // if($day_name != 'Sun' && $day_name != 'Sat'){
                $workdays[] = $date;
                // }
        }
        return $workdays;
    }


    public static function getWorkersAvailables($date){
        $resp = [];
        $workers = Worker::join("working_days","working_days.worker_id","workers.id")->where("working_days.date","=",$date)->groupBy("workers.id")->get();
        foreach($workers as $worker){
            $w = Worker::find($worker->worker_id);
            if($w->works()->where("works.date",$date)->get()->count()==0){
                $resp []= $w;
            }
        }
        return $resp;
    }
}
