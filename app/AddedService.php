<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class AddedService extends Model
{
    protected $table = 'added_services';
}
