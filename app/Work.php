<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class Work extends Model
{
    public function houseType(){
        $this->belongsTo(HouseType::class,'house_types_id'); 
    }
    public function workers(){
        return $this->belongsToMany(Worker::class,'workers_works','work_id','worker_id');
    }

    public function addWorkers($qty){
        $workers = Worker::all();
        // $workingDays = WorkingDay::where('date','=',$this->date)->groupBy('working_days.worker_id')->get();
        $count=0;
        foreach($workers as $worker){
            if($count < $qty){
                //Check if the worker has asigned a work and if is available for working that day
                if($worker->works()->get()->where('date',$this->date.":00")->count()==0 && $worker->workingDays()->get()->where('date',$this->date)->count()>0){
                    $this->workers()->attach($worker->id);
                    $count++;
                }
            }else{
                break;
            }
        }
        $this->save();
    }

    public function service(){
        return $this->belongsTo("App\Service","service_types_id");
    }
/*     public function addworks(){
        
    } */
}
