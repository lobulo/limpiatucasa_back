<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CombinationServices extends Model
{
    protected $table = 'combination_services';
}
