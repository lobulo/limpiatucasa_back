<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkerAddedServices extends Model
{
    protected $table = 'worker_added_services';
}
