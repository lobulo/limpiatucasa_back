<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class WorkingDay extends Model
{
    public function worker(){
        return $this->belongsTo(Worker::class);
    }

}
