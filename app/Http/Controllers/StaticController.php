<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Hash;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Events\BreadDataAdded;
use App\WorkerQuantity;
Use App\CombinationServices;
Use App\WorkerAddedServices;
Use App\AddedService;
Use App\Service;
use App\Coupon;
use App\Work;
use App\Worker;
use App\WorkingDay;

use DB;

// Para conectarnos a Woocommerce API REST
require base_path() . '/vendor/autoload.php';
use Automattic\WooCommerce\Client;


class StaticController extends Controller
{
    /* public function registro(Request $request){
        $new_user = new User();
        $new_user->email = $request->get('email');
        $new_user->name = $request->get('name');
        $new_user->phone = $request->get('phone');
        $new_user->password = Hash::make($request->get('password'));
        $new_user->save();
    } */
    public function registro($cli){
        $new_user = new User();
        $new_user->email = $cli['email'];
        $new_user->name = $cli['name'];
        $new_user->phone = $cli['phone'];
        $new_user->password = Hash::make($cli['pass']);
        $new_user->save();
    }

    public function logout(Request $request) {
      Auth::logout();
      return redirect('/');
    }

    public function addWork(Request $request) {
        $slug = 'works';
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        // $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }
        $request->merge(['date'=>'']);
        if (!$request->has('_validate')) {
            foreach($request->get("dates") as $date){
                $new_request = $request->all();
                $new_request['date'] = $date.":00";
                $request->replace($new_request);
                if($this->getQtyWorkersEnabledByDate($date)>=$request->get("quantity")){
                    $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

                    ## added by Abel -- Guardamos los id de las ordenes en un Array para mandar a la API de Wordpress
                    $orders_id[]=$data->id;

                    //assign workers
                    $data->addWorkers($request->get("quantity"));
                    event(new BreadDataAdded($dataType, $data));
                }
                else{
                    $request->session()->flash('alert-danger', 'Lo sentimos. No existen la cantidad de trabajadores para las fechas agendadas');
                    return redirect("/");
                }
            }

            if ($request->ajax()) {
                return response()->json(['success' => true, 'data' => $data]);
            }

            if($request->get('registration') && $request->get('registration')=="1"){
                //check if user exist
                $user = User::where('email','=',$request->get('email'))->count();
                if($user > 0){
                    return view('registro')->with('error','Email ya existe');
                }
                else{
                    $this->registro($request);
                }
            }

            ## added by Abel -- iremos al metodo payment y  alli, con la API Wordpress guardaremos la orden a pagar
            return $this->payment($data,$orders_id); //se enviara el array con las ordenes agendadas, estas se guardaran como items(productos) de la orden wordpress

        }
    }

    public function reserveWorks($sche, $serv, $client, $disc, $total){
        $orders_id=[];
        $now = date('Y/m/d h:m:s');

        foreach ($sche as $work) {
            if(isset($work['price_added_services']))
                $price_temp = intval($serv['price'] + $work['price_added_services']);
            else
                $price_temp = intval($serv['price']);
            $price = intval($price_temp  - ($price_temp * ($disc/100)));
            $date = $work['datetime'].':00';
            //GUARDAMOS EL  WORK EN LA BASE DE DATOS
            $id_work = DB::table('works')->insertGetId([
                'house_types_id' => $serv['house_types_id'],
                'rooms' => $serv['rooms'],
                'bathrooms' => $serv['bathrooms'],
                'date' => $date,
                'service_types_id' => $serv['service_id'],
                'address'=>$client['address'],
                'phone'=>$client['phone'],
                'city'=>$client['city'],
                'email'=>$client['email'],
                'work_status_id'=>1,
                'description'=>$client['description'],
                'price'=> $price,
                'created_at'=>$now,
                'name'=>$client['name'],
                'notification_status'=>0
            ]);
            $orders_id[]=$id_work;

            //GUARDAMOS EL ID_WORK JUNTO CON LA CANTIDAD DE TRABAJADORES REQUERIDA
            $workers = WorkingDay::join('workers','workers.id','=','working_days.worker_id')->where('date','=',$date)->get();
            $count=0;
            foreach($workers as $worker){
                if($count < $work['workers']){
                    $ww =  DB::select("select ww.worker_id from workers_works as ww JOIN works on ww.work_id=works.id where works.date='$date' and ww.worker_id=$worker->worker_id");
                    if(!$ww) {
                        DB::table('workers_works')->insert([ 'worker_id'=>$worker->worker_id, 'work_id'=>$id_work ]);
                        $count++;
                    }
                }else{
                    break;
                }
            }


            //GUARDAMOS added_services SI EXISTEN EN ESTE WORK
            foreach ($work['added_services'] as $as) {
                DB::table('added_services_works')->insert([ 'added_service_id'=>$as['id'], 'work_id'=>$id_work, 'created_at'=>$now ]);
            }
            //SI SE DECLARO EL PASS, QUIERE DECIR QUE ELIGIERON REGISTRARSE
            if(isset($client['pass'])){
                $user = User::where('email','=',$client['email'])->count();
                if($user > 0){
                    //Esta mal esto, ya que el usuario esta comprando/agendando, no puede estar el registro en la misma acción, si entra a este if toda lo que agendaría no se realizaría
                }
                else{
                    $this->registro($client);
                }
            }
        }
        //LLAMAMOS AL METODO PAYMENT y  alli, con la API Wordpress guardaremos la orden a pagar
        return $this->payment($client,$orders_id,$total); //se enviara el array con las ordenes agendadas, estas se guardaran como items(productos) de la orden wordpress
    }

    public function amountWorkers(Request $request){
        $rooms = $request->get("rooms");
        $bath = $request->get("baths");
        $house_type = $request->get("house_type");
        $amount = $request->get('amount_sevices');

        // $qty= WorkerQuantity::where('house_types_id','=',$house_type)->where('rooms','=',$rooms)->where('bathrooms','=',$bath)->get();
        $qty=WorkerQuantity::where('house_types_id','=',$house_type)->where('rooms','=',$rooms)->where('bathrooms','=',$bath)->where('services_quantity','=',$amount)->first();
        if($qty){
            return response()->json(['status'=>'success','data'=>$qty->toArray()]);

        }else{
            return response()->json(['status'=>'success','data'=>null]);
        }
    }

    // Método que calcula el precio total, verificando que la cantidad de trabajadores requerida por los servicios seleccionados, esten disponibles en los dias agendados
    public function priceAndWorkerDisponibility(Request $request){     
        $option = $request->get('option');
        $token = $request->get("_token");
        $arr_service['rooms'] = $request->get("rooms");
        $arr_service['bathrooms'] = $request->get("baths");
        $arr_service['house_types_id'] = $request->get("house_type");
        $arr_service['service_id'] = intval($request->get('main_service'));
        $schedules = json_decode($_POST['schedules']);
        $service_price=0;
        $total_price = 0;
        $partial_price = 0;
        $total_discount = 0;
        $total_discount = 0;
        $discount = 0;
        $qty_schedules = 0;
        $total_price_added_services = 0;
        $total_price_main_services = 0;
        $arr_schedules=[];

        //si el servicio es planchado, no hay que calcular combinaciones de numero de habitaciones y precios
        if(intval($arr_service['service_id']) == 4){
            //por ahora no comprobaremos la  disponibilidad, ya que eso ya se hizo al setear el calendario, dejaremos esta optimozacion para despues (PENDIENTE-PENDING)
            $reg_price_service = Service::where('id','=',intval($arr_service['service_id']))->select('price')->first();
            $service_price = $reg_price_service->price;
            foreach ($schedules as $schedule) {
                if($schedule != null ){
                    $datetime = $schedule->calendar.' '.$schedule->hour;
                    $qty_schedules++;
                    $arr_schedules[]=array('datetime'=>$datetime, 'workers'=>1, 'added_services'=>[]);
                }
            }
            // return response()->json(['status'=>'vamos bien', 'schedules'=>$arr_schedules]);

            if($qty_schedules>3) $discount=10;
            $partial_price = ($service_price * $qty_schedules);
            $total_discount = $partial_price * $discount/100;
            $total_price = $partial_price - $total_discount;
            if($option === 'calculate'){
                return response()->json(['status'=>true,'house_type_id'=>$arr_service['house_types_id'],'main_service_id'=>$arr_service['service_id'],'rooms'=>$arr_service['rooms'],'baths'=>$arr_service['bathrooms'],'service_price'=>$service_price,'partial_price'=>$partial_price, 'total_price'=>$total_price, 'discount'=>$discount,'total_discount'=>$total_discount, 'schedules'=>$arr_schedules]);
            }else{
                $arr_service['price']=$service_price;
                $arr_client=[];
                $arr_client['name']=$request->get('name');
                $arr_client['address']=$request->get('address');
                $arr_client['city']=$request->get('comuna');
                $arr_client['phone']=$request->get('phone');
                $arr_client['email']=$request->get('email');
                $arr_client['description']=$request->get('observations');
                if($request->get('observations')!='') $arr_client['observations']=$request->get('observations');
                if($request->get('pass')!='') $arr_client['pass']=$request->get('pass');
                $reserve = $this->reserveWorks($arr_schedules,$arr_service,$arr_client,$discount,$total_price);
                return response()->json(['status'=>true, 'url_order_paid'=>$reserve, 'total_price'=>$total_price,'data_cliente'=>$arr_client]);
            }
        }
        //obtenemos el precio del servicio segun la cantidad de habitaciones y baños
        $reg_price_workers = CombinationServices::where('house_types_id','=',$arr_service['house_types_id'])
        ->where('num_rooms','=',$arr_service['rooms'])
        ->where('num_bath','=',$arr_service['bathrooms'])
        ->where('service_id','=',$arr_service['service_id'])
        ->first();
        if($reg_price_workers){
            $service_price = $reg_price_workers->price;
            //recorremos los schedules,
            //primero para verificar si existe disponibilidad de workers por fecha,
            //despues para obtener los servicios adicioanles,
            //y por ultimo para ver si cambia la cantidad de trabajadores
            foreach ($schedules as $schedule) {
                if($schedule != null ){
                    $workers = $reg_price_workers->qty_workers;
                    $qty_schedules++;
                    $datetime = $schedule->calendar.' '.$schedule->hour;
                    $enabled_workers = $this->getQtyWorkersEnabledByDate($datetime);
                    if($reg_price_workers->qty_workers <= $enabled_workers ){
                        //Comprobamos en tabla worker_added_services, si existe otra cantidad requerida de trabajadores, según la  cantidad de servicios adicionales
                        $qty_added_services = count($schedule->added_services);
                        $reg_new_workers = WorkerAddedServices::where('combination_services_id','=',$reg_price_workers->id)->where('qty_added_services','=',$qty_added_services)->first();
                        if($reg_new_workers){ //volvemos a preguntar la cantidad requerida de trabajadores
                            $workers = $reg_new_workers->qty_workers;
                            if($reg_new_workers->qty_workers > $enabled_workers )
                                return response()->json(['status'=>false,'msg'=>"Disponibilidad de Agentes agotada para horario: $schedule->calendar $schedule->hour. Obvie o cambie la fecha para seguir agendando"]);
                        }
                        //creamos un array con el horario y added_services (nombre y precio)
                        $arr_added_services = [];
                        $price_added_services=0;
                        foreach ($schedule->added_services as  $value) {
                            // $query = AddedService::where('id','=',intval($value))->select('name','price')->toSql();
                            // $arr_added_services[] =$query;
                            $reg_added_services = AddedService::where('id','=',intval($value))->select('name','price')->first();
                            $arr_added_services[]=array('id'=>intval($value),'name'=>$reg_added_services->name, 'price'=>$reg_added_services->price);
                            $price_added_services+= $reg_added_services->price;
                        }
                        $total_price_added_services+=$price_added_services;
                        $arr_schedules[]=array('datetime'=>$datetime, 'workers'=>$workers, 'added_services'=>$arr_added_services, 'price_added_services'=>$price_added_services);
                    }else{
                        return response()->json(['status'=>false,'msg'=>"Disponibilidad de Agentes agotada para horario: $schedule->calendar $schedule->hour. Obvie o cambie la fecha para seguir agendando"]);
                    }
                }
            }
            if($qty_schedules>3) $discount=10;
            $partial_price = ($reg_price_workers->price * $qty_schedules) + $total_price_added_services;
            $total_discount = $partial_price * $discount/100;
            $total_price = $partial_price - $total_discount;
            if($option === 'calculate'){
                return response()->json(['status'=>true,'house_type_id'=>$arr_service['house_types_id'],'main_service_id'=>$arr_service['service_id'],'rooms'=>$arr_service['rooms'],'baths'=>$arr_service['bathrooms'],'service_price'=>$service_price,'partial_price'=>$partial_price, 'total_price'=>$total_price, 'discount'=>$discount,'total_discount'=>$total_discount, 'schedules'=>$arr_schedules]);
            }else{
                $arr_service['price']=$service_price;
                $arr_client=[];
                $arr_client['name']=$request->get('name');
                $arr_client['address']=$request->get('address');
                $arr_client['city']=$request->get('comuna');
                $arr_client['phone']=$request->get('phone');
                $arr_client['email']=$request->get('email');
                $arr_client['description']=$request->get('observations');
                if($request->get('observations')!='') $arr_client['observations']=$request->get('observations');
                if($request->get('pass')!='') $arr_client['pass']=$request->get('pass');
                
                $reserve = $this->reserveWorks($arr_schedules,$arr_service,$arr_client,$discount,$total_price);
                return response()->json(['status'=>true, 'url_order_paid'=>$reserve, 'total_price'=>$total_price,'data_cliente'=>$arr_client]);
            }
        }else {
            return response()->json(['status'=>false,'msg'=>"Combinación de Servicio / numero de Habitaciones / numero de baños No Econtrada. Intente con otra combinación"]);
        }
    }

    public function coupon(Request $request){
        if($request->isMethod('post')){
          $coupon = Coupon::where('code','=',$request->code)->first();
          if ($coupon) {
            return response()->json(['status'=>'success','discount'=>$coupon->discount]);
          } else {
            return response()->json(['status'=>'success','data'=>null]);
          }
        }

    }

    public function payment($cli, $orders_id, $order_price) {
        foreach( $orders_id as $order_id ){
            //$line_items[]= array( 'product_id' => $order_id, 'variation_id' => $order_id, 'quantity' => 2 );
            $line_items[]= array( 'product_id' => 1154, 'quantity' => $order_id );
        }

        //key worpress.lob
        // ck_2c5e901c6f063ff5c7b7bcb1fcf02804eb4126c8
        // cs_d13e93af9cdac427d33c3822c2bf819c7bc221f8
        
        $url_commerce = "https://www.limpiatucasa.cl"; 		
         $woocommerce = new Client(
                'https://www.limpiatucasa.cl/', 
                'ck_e8aec6b9bc08d34e837f3aab17662e42d3357210', 
                'cs_d6b9e0e0931fab8347c7ad1dda9a063f3b2b24fc',
                [
                    'wp_api' => true,
                    'version' => 'wc/v2',
                ]
        );
        /*
        $url_commerce = 'http://wordpress.lob/';
        $woocommerce = new Client(
            'http://wordpress.lob/', 
            'ck_2c5e901c6f063ff5c7b7bcb1fcf02804eb4126c8', 
            'cs_d13e93af9cdac427d33c3822c2bf819c7bc221f8',
            [
                'wp_api' => true,
                'version' => 'wc/v3',
            ]
         );*/
        $data = [
            'payment_method' => 'transbank',
            'payment_method_title' => 'Webpay payment',
            'set_paid' => false,
            'billing' => [
                    'first_name' => $cli['name'],
                    'last_name' => '',
                    'address_1' => $cli['address'],
                    'address_2' => '',
                    'city' => $cli['city'],
                    'state' => '',
                    'postcode' => '',
                    'country' => 'CL',
                    'email' => $cli['email'],
                    'phone' => $cli['phone']
            ],
            'shipping' => [
                'first_name' => $cli['name'],
                'last_name' => '',
                'address_1' => $cli['address'],
                'address_2' => '',
                'city' => $cli['city'],
                'state' => '',
                'postcode' => '',
                'country' => 'CL'
            ],
            'line_items' => $line_items,
            'shipping_lines' => [
                    [
                            'method_id' => 'flat_rate',
                            'method_title' => 'Flat Rate',
                            'total' => strval($order_price)
                    ]
            ]
        ];

        $order_pending = $woocommerce->post('orders', $data);
        $url_order = $url_commerce."/checkout/order-pay/".$order_pending->id."/?key=".$order_pending->order_key;
        //guardamos la order_wc en todas order_id agendadas en esta reserva
        // echo $url_order;
        // die();

        return $url_order;
    }

    public function pending(Request $request){
      $id = 1;
      return view('pending', compact('id'));
    }

    public function paid(Request $request){
      $id = 1;
      return view('paid', compact('id'));
    }

    public function getQtyWorkersEnabledByDate($datetime){
        $available_workers = DB::select("select count(*) as qty from workers INNER JOIN working_days ON working_days.worker_id=workers.id WHERE working_days.date='".$datetime."'");
        $reserved_workers = DB::select("select count(*) as qty from works INNER JOIN workers_works ON works.id=workers_works.work_id WHERE date='".$datetime."'");
        return ($available_workers[0]->qty - $reserved_workers[0]->qty);
    }

    public function reserva(){
        $register_added_services = AddedService::select('id','name','price')->get();
        $register_services = Service::select('id','name','price')->get();
        // $register_added_services = AddedService::all('id','name','price');
        $disabled_dates = [];
        $disabled_dates['am']=$this->getDisabledDates('08:30:00');
        $disabled_dates['pm']=$this->getDisabledDates('14:00:00');
        return view('reserva',['disabled_dates'=>$disabled_dates, 'object_services'=>$register_services, 'object_added_services'=>$register_added_services]);
    }

    private function getDisabledDates($time){
        //The amount of works on the date has to be less than the Enableds workers to work that day.
        // $dates = DB::select("select date(date) as working_date from working_days where TIME(date) = '".$time."'");
        // foreach($dates as $date){
        // }
        $disabled_dates=[];
        $dates = array_merge(Worker::getAllDays(date("m")),Worker::getAllDays(date("m")+1));
        // $dates = Worker::getAllDays(date("m"));
        foreach($dates as $date){
            // $working_days = WorkingDay::where('date',$date." ".$time)->count();
            // $works = Work::where('date',$date." ".$time)->count();
            $workers_enabled = $this->getQtyWorkersEnabledByDate($date." ".$time);
            if($workers_enabled < 1){
                $disabled_dates[]=$date;
            }
        }
        return $disabled_dates;
        // return $dates;
    }
}
