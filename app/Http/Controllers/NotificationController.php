<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Database\Schema\SchemaManager;
use Auth;
use Hash;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Events\BreadDataAdded;
use URL;
use App\WorkingDay;
use App\Work;
use App\Worker;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class NotificationController extends Controller
{
  //CAPTURA DE DATOS
  public function notification(Request $request){
    $next_works =[];
    Carbon::setLocale('es');
    $today = Carbon::now()->startOfDay()->toDateTimeString();
    $tomorrow = Carbon::tomorrow()->endOfDay()->toDateTimeString();

    // $works = Work::select('id','date','address','email','phone','city','name')->where('notification_status', '0')->where('work_status_id', 2)->get();
    $works = Work::select('id','date','address','email','phone','city','name')->where('notification_status', '0')->whereBetween('date', array( $today, $tomorrow))->where('work_status_id', 2)->get();

    if ($works) {
      return $this->send($works);
    }
  }

  //ENVÍO de SMS y CORREO
  public function send($works){
    foreach ($works as $key => $work) {
      // ###################################### //
      // ########## Envíos a CLIENTES  ######## //
      // ###################################### //
      $email= $work->email;
      $phone = "569".$work->phone;
      $name = $work->name;
      $text_client = "Te recordamos que mañana tienes agendado un servicio de Limpieza en:  ".$work->address.", ".$work->city;

      //EMAIL
      if ($email){
        $data_email = array(
          'dst_email'=>  $email,
          'subject'  =>  'Notificación Limpia tu Casa',
          'email_content' => "Hola $name. \n$text_client. \nSaludos. \nEquipo de Limpiatucasa'",
          'sender' => 'Limpia tu casa',
          'src_email' => 'reserva@limpiatucasa.cl'
        );
        $data_email_string = json_encode($data_email);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.connectus.cl/api_v1/send_email');
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_email_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, 'dfff7820a7a44796885ccc9686076a85:144e08b111c746d3aaf69f2189aa3107');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Accept: application/json',
          'Content-Type: application/json')
        );
        if(curl_exec($ch) === false){
          echo 'Curl error: ' . curl_error($ch);
        }else{
          echo "mail enviado correctamente a cliente: ".$email;
        }
        curl_close($ch);
      }
      //SMS
     if ($phone) {
        $data_sms = array('dst_number' => $phone,
        'sms_content' => "¡Hola $name! $text_client. Saludos. Equipo LTC");
        $data_sms_string = json_encode($data_sms);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.connectus.cl/api_v1/send_sms');
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_sms_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, 'dfff7820a7a44796885ccc9686076a85:144e08b111c746d3aaf69f2189aa3107');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Accept: application/json',
          'Content-Type: application/json')
        );
        if(curl_exec($ch) === false){
          echo 'Curl error: ' . curl_error($ch);
        }else{
          echo "sms enviado correctamente";
        }
        curl_close($ch);
      }
      // ###################################### //
      // ########## Envíos a Agentes  ######### //
      // ###################################### //

      $workers = Work::where('id',$work->id)->first()->workers()->get();

      foreach ($workers as $key => $worker) {

        $nombreWorker = $worker->name." ".$worker->last_name;
        $textWorker = "Te recordamos que mañana tienes agendado un trabajo de limpieza en ".$work->address.", ".$work->city;
        $emailWorker = $worker->email;
        // $emailWorker = 'ofaber22@hotmail.com';
        $phoneWorker = "569".$worker->phone;
        // $phoneWorker = "56949607845";

        //EMAIL
        if ($emailWorker){
          $data_email = array(
            'dst_email'=>  $emailWorker,
            'subject'  =>  "Hola $nombreWorker \n$textWorker. \nSaludos. \nEquipo de Limpiatucasa'",
            'email_content' => 'contenido de prueba',
            'sender' => 'Limpia tu casa',
            'src_email' => 'reserva@limpiatucasa.cl'
          );
          $data_email_string = json_encode($data_email);
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, 'https://api.connectus.cl/api_v1/send_email');
          curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $data_email_string);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_USERPWD, 'dfff7820a7a44796885ccc9686076a85:144e08b111c746d3aaf69f2189aa3107');
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
          curl_setopt($ch, CURLOPT_HEADER, 1);
          curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json')
          );
          if(curl_exec($ch) === false){
            echo 'Curl error: ' . curl_error($ch);
          }else{
            echo "<br>mail enviado correctamente a agente: ".$emailWorker;
          }
          curl_close($ch);          
        }
        //SMS
        if ($phoneWorker) {
          $data = array('dst_number' => $phoneWorker,
          'sms_content' => "Hola $nombreWorker! $textWorker. Saludos. Equipo LTC.");
          $data_string = json_encode($data);
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, 'https://api.connectus.cl/api_v1/send_sms');
          curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_USERPWD, 'dfff7820a7a44796885ccc9686076a85:144e08b111c746d3aaf69f2189aa3107');
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
          curl_setopt($ch, CURLOPT_HEADER, 1);
          curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json')
          );
          if(curl_exec($ch) === false){
            echo 'Curl error: ' . curl_error($ch);
          }
          curl_close($ch);
        }
      }

      //update work status
      Work::where('id', $work->id)->update(['notification_status' => 1]);
    }
  }
}
