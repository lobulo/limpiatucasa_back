<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Work;

class ApiWorksController extends Controller
{
    public function getAll(){
        $work_status = Work::all();
        return $work_status;
    }

    public function getPaidWorks(){
        $paid_works = Work::where('work_status_id', 2)->get();
        return $paid_works;
    }
    
	public function changeWorkStatus(Request $request){
		$orders_changed = [];
		 //cambiamos el estado de las ordenes a pagadas
			//$tipo = gettype($request->orders_id);
			$orders_array=explode('*',$request->orders_id);
			if($request->status == 'paid'){
				foreach( $orders_array as $order_id){
					//$order_to_change = Work::where('id',$request->order_id)->update(array('work_status_id' => 2)); //Esta opcion retorna 1, y la opcion siguiente retorna el registro modificado
					$order_to_change = Work::find($order_id);					
					if($order_to_change){
						$order_to_change->work_status_id = 2;
						$order_to_change->save();
						$orders_changed[]=$order_id; // puede que esto este de mas, la idea es que siempre actualice ya que la orde se recorre solo una vez y deberia estar impaga
					}
				}
			}else{
				foreach( $orders_array as $order_id){
					//$order_to_change = Work::where('id',$request->order_id)->update(array('work_status_id' => 2)); //Esta opcion retorna 1, y la opcion siguiente retorna el registro modificado
					$order_to_delete = Work::find($order_id);
					$order_to_delete->delete();
					DB::table('workers_work')->where('work_id','=',$order_id)->delete();
					DB::table('added_services_works')->where('work_id','=',$order_id)->delete();
				}
			}
			return 'true';
	}
}
