<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "StaticController@reserva");

Route::get('/registro', 'StaticController@registro');
Route::post('/registro', 'StaticController@registro');
Route::post('/add_work', 'StaticController@addWork');
Route::post('/orden', 'StaticController@addWork');


Route::get('/reserva',"StaticController@reserva");
Route::get('logout', 'StaticController@logout');

Route::post('/amount_workers','StaticController@amountWorkers');
Route::post('/price_workers','StaticController@priceAndWorkerDisponibility');


Route::post('/validate_coupon','StaticController@coupon');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

//notificaciones
Route::get('/notification', 'NotificationController@notification');
Route::get('/payment', 'WorksController@payment');

Route::get('/pendiente', 'StaticController@pending');
Route::get('/exito', 'StaticController@paid');

Route::get('/exito', function(){
	echo "<h1>PAGO EXITOSO</h1>";
});

Route::get('/error_pago', function(){
        echo "<h1>NO SE PUDO REALIZAR LA TRANSACION</h1>";
});

