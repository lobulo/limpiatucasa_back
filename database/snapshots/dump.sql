
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `added_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `added_services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `added_services` WRITE;
/*!40000 ALTER TABLE `added_services` DISABLE KEYS */;
INSERT INTO `added_services` VALUES (1,'una hora de planchado',6000,NULL,NULL),(2,'otra hora de planchado',12000,NULL,NULL),(3,'Limpieza interior de refrigerador',6000,NULL,NULL),(4,'Limpieza interior de horno',6000,NULL,NULL);
/*!40000 ALTER TABLE `added_services` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `added_services_works`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `added_services_works` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `added_service_id` int(11) NOT NULL,
  `work_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `added_services_works` WRITE;
/*!40000 ALTER TABLE `added_services_works` DISABLE KEYS */;
INSERT INTO `added_services_works` VALUES (1,2,5,NULL,NULL),(2,3,5,NULL,NULL),(3,2,6,NULL,NULL),(4,1,6,NULL,NULL),(5,2,7,NULL,NULL),(6,1,7,NULL,NULL),(7,1,8,NULL,NULL),(8,2,8,NULL,NULL),(9,1,9,NULL,NULL),(10,2,9,NULL,NULL),(11,1,10,NULL,NULL),(12,1,11,NULL,NULL),(13,2,11,NULL,NULL),(14,1,12,NULL,NULL),(15,2,12,NULL,NULL),(16,1,13,NULL,NULL),(17,2,13,NULL,NULL),(18,1,14,NULL,NULL),(19,2,14,NULL,NULL),(20,1,15,NULL,NULL),(21,2,15,NULL,NULL),(22,1,16,NULL,NULL),(23,1,17,NULL,NULL),(24,2,17,NULL,NULL),(25,1,18,NULL,NULL),(26,1,19,NULL,NULL),(27,2,19,NULL,NULL),(28,1,20,NULL,NULL),(29,2,20,NULL,NULL),(30,1,21,NULL,NULL),(31,2,21,NULL,NULL),(32,1,22,NULL,NULL),(33,2,22,NULL,NULL),(34,1,23,NULL,NULL),(35,2,23,NULL,NULL),(36,1,24,NULL,NULL),(37,1,25,NULL,NULL),(38,2,25,NULL,NULL),(39,1,26,NULL,NULL),(40,2,26,NULL,NULL),(41,1,27,NULL,NULL),(42,2,27,NULL,NULL),(43,1,28,NULL,NULL),(44,2,28,NULL,NULL),(45,1,29,NULL,NULL),(46,2,29,NULL,NULL),(47,1,30,NULL,NULL),(48,2,30,NULL,NULL),(49,1,33,NULL,NULL),(50,1,34,NULL,NULL),(51,1,35,NULL,NULL),(52,2,36,NULL,NULL),(53,1,37,NULL,NULL),(54,1,38,NULL,NULL),(55,1,39,NULL,NULL),(56,1,40,NULL,NULL),(57,2,41,NULL,NULL),(58,2,42,NULL,NULL),(59,2,43,NULL,NULL),(60,2,44,NULL,NULL),(61,2,45,NULL,NULL),(62,1,46,NULL,NULL),(63,1,47,NULL,NULL),(64,1,48,NULL,NULL),(65,1,49,NULL,NULL),(66,1,50,NULL,NULL),(67,1,51,NULL,NULL),(68,1,52,NULL,NULL),(69,1,53,NULL,NULL),(70,1,54,NULL,NULL),(71,1,55,NULL,NULL),(72,1,56,NULL,NULL),(73,1,57,NULL,NULL),(74,1,58,NULL,NULL),(75,1,59,NULL,NULL),(76,1,60,NULL,NULL),(77,1,61,NULL,NULL),(78,1,62,NULL,NULL),(79,1,63,NULL,NULL),(80,1,64,NULL,NULL),(81,1,65,NULL,NULL),(82,1,1,NULL,NULL),(83,1,2,NULL,NULL),(84,1,3,NULL,NULL);
/*!40000 ALTER TABLE `added_services_works` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `combination_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `combination_services` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `house_types_id` int(11) DEFAULT NULL,
  `num_rooms` int(11) DEFAULT NULL,
  `num_bath` int(11) DEFAULT NULL,
  `qty_workers` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=424 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `combination_services` WRITE;
/*!40000 ALTER TABLE `combination_services` DISABLE KEYS */;
INSERT INTO `combination_services` VALUES (1,1,1,1,1,20000,1),(7,1,1,2,1,22000,1),(12,1,1,3,1,24000,1),(17,1,2,1,1,22000,1),(22,1,2,2,1,24000,1),(27,1,2,3,2,26000,1),(31,1,3,1,1,24000,1),(37,1,3,2,1,26000,1),(42,1,3,3,1,28000,1),(47,1,3,4,2,36000,1),(52,1,4,1,1,26000,1),(57,1,4,2,1,28000,1),(62,1,4,3,2,36000,1),(67,1,4,4,2,38000,1),(72,1,4,5,2,40000,1),(77,1,5,1,1,28000,1),(82,1,5,2,2,36000,1),(87,1,5,3,2,38000,1),(92,1,5,4,2,40000,1),(97,1,5,5,2,42000,1),(102,1,5,6,2,44000,1),(107,1,6,1,2,42000,1),(112,1,6,2,2,38000,1),(117,1,6,3,2,40000,1),(122,1,6,4,2,42000,1),(127,1,6,5,2,44000,1),(132,1,6,6,2,46000,1),(138,2,1,1,1,18000,1),(143,2,1,2,1,19000,1),(148,2,1,3,1,21000,1),(153,2,2,1,2,19000,1),(158,2,2,2,1,21000,1),(163,2,2,3,1,23000,1),(168,2,3,1,1,21000,1),(173,2,3,2,1,23000,1),(178,2,3,3,1,25000,1),(183,2,3,4,2,34000,1),(188,2,4,1,1,23000,1),(193,2,4,2,1,25000,1),(198,2,4,3,2,34000,1),(203,2,4,4,2,37000,1),(208,2,4,5,2,39000,1),(213,2,5,1,1,25000,1),(218,2,5,2,2,34000,1),(223,2,5,3,2,37000,1),(228,2,5,4,2,38000,1),(233,2,5,5,2,40000,1),(238,2,5,6,3,41000,1),(243,2,6,1,2,34000,1),(248,2,6,2,2,37000,1),(253,2,6,3,2,38000,1),(258,2,6,4,2,40000,1),(263,2,6,5,2,41000,1),(268,2,6,6,2,42000,1),(273,2,1,1,2,83160,2),(274,2,1,2,2,92400,2),(275,2,1,3,2,101640,2),(276,2,2,1,2,92400,2),(277,2,2,2,2,101640,2),(278,2,2,3,3,110880,2),(279,2,3,1,2,101640,2),(280,2,3,2,3,110880,2),(281,2,3,3,3,120120,2),(282,2,3,4,4,129360,2),(283,2,4,1,3,110880,2),(284,2,4,2,3,120120,2),(285,2,4,3,4,129360,2),(286,2,4,4,4,138600,2),(287,2,4,5,5,147840,2),(288,2,5,1,3,120120,2),(289,2,5,2,4,129360,2),(290,2,5,3,4,138600,2),(291,2,5,4,5,147840,2),(292,2,5,5,5,157080,2),(293,2,5,6,5,166320,2),(294,2,6,1,4,129360,2),(295,2,6,2,4,138600,2),(296,2,6,3,5,147840,2),(297,2,6,4,5,157080,2),(298,2,6,5,5,166320,2),(299,2,6,6,5,175560,2),(300,1,1,1,2,95634,2),(301,1,1,2,3,106260,2),(302,1,1,3,3,116886,2),(303,1,2,1,2,106260,2),(304,1,2,2,3,116886,2),(305,1,2,3,4,127512,2),(306,1,3,1,3,116886,2),(307,1,3,2,4,127512,2),(308,1,3,3,4,138138,2),(309,1,3,4,4,148764,2),(310,1,4,1,4,127512,2),(311,1,4,2,4,138138,2),(312,1,4,3,4,148764,2),(313,1,4,4,4,159390,2),(314,1,4,5,5,170016,2),(315,1,5,1,4,138138,2),(316,1,5,2,5,148764,2),(317,1,5,3,5,159390,2),(318,1,5,4,5,170016,2),(319,1,5,5,5,180642,2),(320,1,5,6,6,191268,2),(321,1,6,1,5,148764,2),(322,2,1,1,1,20000,2),(323,2,1,2,1,22000,2),(324,1,6,2,5,159390,2),(325,2,1,3,1,24000,2),(326,1,6,3,5,170016,2),(327,1,6,4,5,180642,2),(328,2,2,1,1,22000,2),(329,1,6,5,6,191268,2),(330,2,2,2,1,24000,2),(331,2,2,3,2,26000,2),(332,1,6,6,6,201894,2),(333,2,3,1,1,24000,2),(334,2,3,2,1,26000,2),(335,2,1,1,2,99792,3),(336,2,1,2,2,110880,3),(337,2,1,3,3,121968,3),(338,2,2,1,2,110880,3),(339,2,2,2,3,121968,3),(340,2,2,3,3,133056,3),(341,2,3,1,3,121968,3),(342,2,3,2,3,133056,3),(343,2,3,3,3,144144,3),(344,2,3,4,4,155232,3),(345,2,4,1,3,133056,3),(346,2,4,2,3,144144,3),(347,2,4,3,4,155232,3),(348,2,4,4,4,166320,3),(349,2,4,5,5,177408,3),(350,2,5,1,3,144144,3),(351,2,5,2,4,155232,3),(352,2,5,3,4,166320,3),(353,2,5,4,5,177408,3),(354,2,5,5,5,188496,3),(355,2,5,6,5,199584,3),(356,2,6,1,4,155232,3),(357,2,6,2,4,166320,3),(358,2,6,3,5,177408,3),(359,2,6,4,5,188496,3),(360,2,6,5,5,199584,3),(361,2,6,6,5,210672,3),(362,1,1,1,2,106153,3),(363,1,1,2,3,116886,3),(364,1,1,3,3,128574,3),(365,1,2,1,3,116886,3),(366,1,2,2,3,128574,3),(367,1,2,3,4,140263,3),(368,1,3,1,3,128574,3),(369,1,3,2,4,140263,3),(370,1,3,3,4,151951,3),(371,1,3,4,5,163640,3),(372,1,4,1,4,140263,3),(373,1,4,2,4,151951,3),(374,1,4,3,5,163640,3),(375,1,4,4,5,175329,3),(376,1,4,5,5,187017,3),(377,1,5,1,4,151951,3),(378,1,5,2,5,163640,3),(379,1,5,3,5,175329,3),(380,1,5,4,5,187017,3),(381,1,5,5,6,198706,3),(382,2,1,1,2,99792,3),(383,1,5,6,6,210394,3),(384,2,1,2,2,110880,3),(385,1,6,1,5,163640,3),(386,2,1,3,3,121968,3),(387,1,6,2,5,175329,3),(388,2,2,1,2,110880,3),(389,1,6,3,5,187017,3),(390,2,2,2,3,121968,3),(391,2,2,3,3,133056,3),(392,1,6,4,6,198706,3),(393,1,6,5,6,210394,3),(394,2,3,1,3,121968,3),(395,2,3,2,3,133056,3),(396,1,6,6,6,222083,3),(397,2,3,3,3,144144,3),(398,2,3,4,4,155232,3),(399,2,4,1,3,133056,3),(400,2,4,2,3,144144,3),(401,2,4,3,4,155232,3),(402,2,4,4,4,166320,3),(403,2,4,5,5,177408,3),(404,2,5,1,3,144144,3),(405,2,5,2,4,155232,3),(406,2,5,3,4,166320,3),(407,2,5,4,5,177408,3),(408,2,5,5,5,188496,3),(409,2,5,6,5,199584,3),(410,2,6,1,4,155232,3),(411,2,6,2,4,166320,3),(412,2,6,3,5,177408,3),(413,2,6,4,5,188496,3),(414,2,6,5,5,199584,3),(415,2,6,6,5,210672,3),(416,1,1,1,2,106153,3),(417,1,1,2,3,116886,3),(418,1,1,3,3,128574,3),(419,1,2,1,3,116886,3),(420,1,2,2,3,128574,3),(421,1,2,3,4,140263,3),(422,1,3,1,3,128574,3),(423,1,3,2,4,140263,3);
/*!40000 ALTER TABLE `combination_services` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `coupons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `coupons` WRITE;
/*!40000 ALTER TABLE `coupons` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupons` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `data_rows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `data_rows` WRITE;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO `data_rows` VALUES (1,1,'id','number','ID',1,0,0,0,0,0,NULL,1),(2,1,'name','text','Nombre',1,1,1,1,1,1,NULL,2),(3,1,'email','text','Email',1,1,1,1,1,1,NULL,3),(4,1,'password','password','Contraseña',1,0,0,1,1,0,NULL,14),(5,1,'remember_token','text','Remember Token',0,0,0,0,0,0,NULL,4),(6,1,'created_at','timestamp','Created At',0,0,1,0,0,0,NULL,5),(7,1,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,6),(8,1,'avatar','image','Avatar',0,0,0,0,0,0,NULL,7),(9,1,'user_belongsto_role_relationship','relationship','Role',0,0,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}',12),(10,1,'user_belongstomany_role_relationship','relationship','Roles',0,0,1,0,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}',13),(12,1,'settings','hidden','Settings',0,0,0,0,0,0,NULL,15),(13,2,'id','number','ID',1,0,0,0,0,0,'',1),(14,2,'name','text','Name',1,1,1,1,1,1,'',2),(15,2,'created_at','timestamp','Created At',0,0,0,0,0,0,'',3),(16,2,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'',4),(17,3,'id','number','ID',1,0,0,0,0,0,NULL,1),(18,3,'name','text','Name',1,0,1,1,1,1,NULL,2),(19,3,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,3),(20,3,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,4),(21,3,'display_name','text','Display Name',1,0,1,1,1,1,NULL,5),(22,1,'role_id','text','Role',1,0,1,1,1,1,NULL,11),(23,4,'id','text','Id',1,0,0,0,0,0,NULL,1),(24,4,'name','text','Nombre',1,1,1,1,1,1,NULL,2),(25,4,'last_name','text','Apellido',1,1,1,1,1,1,NULL,3),(26,4,'phone','text','Teléfono',0,1,1,1,1,1,NULL,4),(27,4,'rut','text','Rut',1,1,1,1,1,1,NULL,5),(28,4,'created_at','timestamp','Creado',0,1,1,1,0,1,NULL,7),(29,4,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,9),(30,4,'worker_hasone_working_day_relationship','relationship','Días',0,0,0,1,1,1,'{\"model\":\"App\\\\WorkingDay\",\"table\":\"working_days\",\"type\":\"hasOne\",\"column\":\"id\",\"key\":\"id\",\"label\":\"id\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}',11),(31,4,'deleted_at','timestamp','Deleted At',0,0,0,0,0,0,NULL,10),(32,5,'id','text','Id',1,0,0,0,0,0,NULL,3),(34,5,'rooms','number','Cantidad de habitaciones',1,1,1,1,1,1,NULL,5),(35,5,'bathrooms','number','Cantidad de baños',1,1,1,1,1,1,NULL,6),(36,5,'date','datetime','Fecha y Hora de trabajo',1,1,1,1,1,1,NULL,7),(38,5,'address','text','Dirección',1,1,1,1,1,1,NULL,9),(39,5,'phone','text','Teléfono',1,1,1,1,1,1,NULL,10),(40,5,'city','text','Comuna',1,1,1,1,1,1,NULL,11),(41,5,'email','text','Email',1,1,1,1,1,1,NULL,12),(42,5,'created_at','timestamp','Creado',0,1,1,1,0,1,NULL,13),(43,5,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,14),(45,5,'work_hasone_house_type_relationship','relationship','Tipo de vivienda',0,1,1,1,1,1,'{\"model\":\"App\\\\HouseType\",\"table\":\"house_types\",\"type\":\"belongsTo\",\"column\":\"house_types_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}',17),(46,5,'house_types_id','select_dropdown','Tipo de vivienda',1,0,1,0,1,0,'{\"default\":\"1\"}',4),(47,5,'work_hasone_service_relationship','relationship','Servicios',0,1,1,1,1,1,'{\"model\":\"App\\\\Service\",\"table\":\"services\",\"type\":\"belongsTo\",\"column\":\"service_types_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}',18),(48,5,'service_types_id','text','Service Types Id',1,0,0,0,1,0,NULL,8),(50,5,'work_belongsto_work_status_relationship','relationship','Estado',0,1,1,1,0,1,'{\"model\":\"App\\\\WorkStatus\",\"table\":\"work_status\",\"type\":\"belongsTo\",\"column\":\"work_status_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}',19),(51,5,'work_status_id','text','Estado',1,1,1,1,1,1,NULL,16),(52,5,'work_hasmany_worker_relationship','relationship','Trabajadores',0,1,0,1,1,1,'{\"model\":\"App\\\\Worker\",\"table\":\"workers\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"workers_works\",\"pivot\":\"1\",\"taggable\":\"on\"}',20),(54,5,'work_belongstomany_added_service_relationship','relationship','Servicios adicionales',0,1,1,1,1,1,'{\"model\":\"App\\\\AddedService\",\"table\":\"added_services\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"added_services_works\",\"pivot\":\"1\",\"taggable\":\"0\"}',22),(55,5,'description','text_area','Descripción',0,1,1,1,1,1,NULL,15),(56,5,'price','text','Valor',0,1,1,1,1,1,NULL,2),(57,4,'email','text','Email',0,1,1,1,1,1,NULL,6),(58,5,'name','text','Nombre',0,1,1,1,1,1,NULL,1),(59,4,'start_month','select_dropdown','Mes de inicio',0,1,1,1,1,1,'{\"default\":\"Enero\",\"options\":{\"1\":\"Enero\",\"2\":\"Febrero\",\"3\":\"Marzo\",\"4\":\"Abril\",\"5\":\"Mayo\",\"6\":\"Junio\",\"7\":\"Julio\",\"8\":\"Agosto\",\"9\":\"Septiembre\",\"10\":\"Octubre\",\"11\":\"Noviembre\",\"12\":\"Diciembre\"}}',8),(60,1,'last_name','text','Apellido',0,1,1,1,1,1,NULL,8),(61,1,'phone','text','Teléfono',0,1,1,1,1,1,NULL,9),(62,5,'notification_status','hidden','Notification Status',1,0,0,0,0,0,NULL,21),(63,1,'rut','text','Rut',0,1,1,1,1,0,NULL,10);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `data_types` WRITE;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` VALUES (1,'users','users','Usuario','Usuarios','voyager-person','TCG\\Voyager\\Models\\User','TCG\\Voyager\\Policies\\UserPolicy',NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-07-10 11:36:54','2018-07-11 08:59:47'),(2,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','',1,0,NULL,'2018-07-10 11:36:54','2018-07-10 11:36:54'),(3,'roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-07-10 11:36:54','2018-10-15 22:45:40'),(4,'workers','workers','Agente','Agentes',NULL,'App\\Worker',NULL,'WorkersController',NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-07-11 07:58:33','2018-08-12 01:21:40'),(5,'works','works','Trabajo','Trabajos',NULL,'App\\Work',NULL,'WorksController',NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-07-26 11:36:46','2018-08-06 11:35:03');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `house_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `house_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `house_types` WRITE;
/*!40000 ALTER TABLE `house_types` DISABLE KEYS */;
INSERT INTO `house_types` VALUES (1,'Casa',NULL,NULL),(2,'Departamento',NULL,NULL);
/*!40000 ALTER TABLE `house_types` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` VALUES (1,1,'Inicio','','_self','voyager-boat','#000000',NULL,1,'2018-07-10 11:36:54','2018-10-15 22:45:58','voyager.dashboard','null'),(3,1,'Clientes','','_self','voyager-person','#000000',NULL,3,'2018-07-10 11:36:54','2018-10-15 23:04:47','voyager.users.index','null'),(5,1,'Tools','','_self','voyager-tools',NULL,NULL,6,'2018-07-10 11:36:54','2018-07-28 01:46:35',NULL,NULL),(6,1,'Menu Builder','','_self','voyager-list',NULL,5,1,'2018-07-10 11:36:54','2018-07-11 07:58:52','voyager.menus.index',NULL),(7,1,'Database','','_self','voyager-data',NULL,5,2,'2018-07-10 11:36:54','2018-07-11 07:58:52','voyager.database.index',NULL),(8,1,'Compass','','_self','voyager-compass',NULL,5,3,'2018-07-10 11:36:54','2018-07-11 07:58:52','voyager.compass.index',NULL),(9,1,'BREAD','','_self','voyager-bread',NULL,5,4,'2018-07-10 11:36:54','2018-07-11 07:58:52','voyager.bread.index',NULL),(10,1,'Settings','','_self','voyager-settings',NULL,NULL,7,'2018-07-10 11:36:54','2018-07-28 01:46:35','voyager.settings.index',NULL),(11,1,'Hooks','','_self','voyager-hook',NULL,5,5,'2018-07-10 11:36:54','2018-07-11 07:58:52','voyager.hooks',NULL),(12,1,'Agentes','','_self','voyager-people','#000000',NULL,4,'2018-07-11 07:58:33','2018-08-12 01:54:41','voyager.workers.index','null'),(13,1,'Ordenes de trabajo','','_self','voyager-hammer','#000000',NULL,5,'2018-07-26 11:36:47','2018-07-31 11:00:45','voyager.works.index','null');
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'admin','2018-07-10 11:36:54','2018-07-10 11:36:54');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_01_01_000000_add_voyager_user_fields',1),(4,'2016_01_01_000000_create_data_types_table',1),(5,'2016_05_19_173453_create_menu_table',1),(6,'2016_10_21_190000_create_roles_table',1),(7,'2016_10_21_190000_create_settings_table',1),(8,'2016_11_30_135954_create_permission_table',1),(9,'2016_11_30_141208_create_permission_role_table',1),(10,'2016_12_26_201236_data_types__add__server_side',1),(11,'2017_01_13_000000_add_route_to_menu_items_table',1),(12,'2017_01_14_005015_create_translations_table',1),(13,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',1),(14,'2017_03_06_000000_add_controller_to_data_types_table',1),(15,'2017_04_21_000000_add_order_to_data_rows_table',1),(16,'2017_07_05_210000_add_policyname_to_data_types_table',1),(17,'2017_08_05_000000_add_group_to_settings_table',1),(18,'2017_11_26_013050_add_user_role_relationship',1),(19,'2017_11_26_015000_create_user_roles_table',1),(20,'2018_03_11_000000_add_user_settings',1),(21,'2018_03_14_000000_add_details_to_data_types_table',1),(22,'2018_03_16_000000_make_settings_value_nullable',1),(23,'2018_07_11_195041_create_data_rows_table',2),(24,'2018_07_11_195041_create_data_types_table',2),(25,'2018_07_11_195041_create_menu_items_table',2),(26,'2018_07_11_195041_create_menus_table',2),(27,'2018_07_11_195041_create_permission_role_table',2),(28,'2018_07_11_195041_create_permissions_table',2),(29,'2018_07_11_195041_create_roles_table',2),(30,'2018_07_11_195041_create_settings_table',2),(31,'2018_07_11_195041_create_translations_table',2),(32,'2018_07_11_195041_create_user_roles_table',2),(33,'2018_07_11_195041_create_workers_table',2),(34,'2018_07_11_195043_add_foreign_keys_to_data_rows_table',2),(35,'2018_07_11_195043_add_foreign_keys_to_menu_items_table',2),(36,'2018_07_11_195043_add_foreign_keys_to_permission_role_table',2),(37,'2018_07_11_195043_add_foreign_keys_to_user_roles_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1),(1,2),(1,3),(1,4),(2,1),(2,2),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(11,3),(12,1),(12,3),(13,1),(13,3),(14,1),(14,3),(15,1),(15,3),(16,1),(16,3),(17,1),(17,3),(18,1),(18,3),(19,1),(19,3),(20,1),(20,3),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(27,3),(28,1),(28,3),(29,1),(29,3),(30,1),(30,3),(31,1),(31,3),(32,1),(32,3),(32,4),(33,1),(33,3),(33,4),(34,1),(34,3),(36,1),(36,3),(37,3);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'browse_admin',NULL,'2018-07-10 11:36:54','2018-07-10 11:36:54'),(2,'browse_bread',NULL,'2018-07-10 11:36:54','2018-07-10 11:36:54'),(3,'browse_database',NULL,'2018-07-10 11:36:54','2018-07-10 11:36:54'),(4,'browse_media',NULL,'2018-07-10 11:36:54','2018-07-10 11:36:54'),(5,'browse_compass',NULL,'2018-07-10 11:36:54','2018-07-10 11:36:54'),(6,'browse_menus','menus','2018-07-10 11:36:54','2018-07-10 11:36:54'),(7,'read_menus','menus','2018-07-10 11:36:54','2018-07-10 11:36:54'),(8,'edit_menus','menus','2018-07-10 11:36:54','2018-07-10 11:36:54'),(9,'add_menus','menus','2018-07-10 11:36:54','2018-07-10 11:36:54'),(10,'delete_menus','menus','2018-07-10 11:36:54','2018-07-10 11:36:54'),(11,'browse_roles','roles','2018-07-10 11:36:54','2018-07-10 11:36:54'),(12,'read_roles','roles','2018-07-10 11:36:54','2018-07-10 11:36:54'),(13,'edit_roles','roles','2018-07-10 11:36:54','2018-07-10 11:36:54'),(14,'add_roles','roles','2018-07-10 11:36:54','2018-07-10 11:36:54'),(15,'delete_roles','roles','2018-07-10 11:36:54','2018-07-10 11:36:54'),(16,'browse_users','users','2018-07-10 11:36:54','2018-07-10 11:36:54'),(17,'read_users','users','2018-07-10 11:36:54','2018-07-10 11:36:54'),(18,'edit_users','users','2018-07-10 11:36:54','2018-07-10 11:36:54'),(19,'add_users','users','2018-07-10 11:36:54','2018-07-10 11:36:54'),(20,'delete_users','users','2018-07-10 11:36:54','2018-07-10 11:36:54'),(21,'browse_settings','settings','2018-07-10 11:36:54','2018-07-10 11:36:54'),(22,'read_settings','settings','2018-07-10 11:36:54','2018-07-10 11:36:54'),(23,'edit_settings','settings','2018-07-10 11:36:54','2018-07-10 11:36:54'),(24,'add_settings','settings','2018-07-10 11:36:54','2018-07-10 11:36:54'),(25,'delete_settings','settings','2018-07-10 11:36:54','2018-07-10 11:36:54'),(26,'browse_hooks',NULL,'2018-07-10 11:36:54','2018-07-10 11:36:54'),(27,'browse_workers','workers','2018-07-11 07:58:33','2018-07-11 07:58:33'),(28,'read_workers','workers','2018-07-11 07:58:33','2018-07-11 07:58:33'),(29,'edit_workers','workers','2018-07-11 07:58:33','2018-07-11 07:58:33'),(30,'add_workers','workers','2018-07-11 07:58:33','2018-07-11 07:58:33'),(31,'delete_workers','workers','2018-07-11 07:58:33','2018-07-11 07:58:33'),(32,'browse_works','works','2018-07-26 11:36:46','2018-07-26 11:36:46'),(33,'read_works','works','2018-07-26 11:36:46','2018-07-26 11:36:46'),(34,'edit_works','works','2018-07-26 11:36:46','2018-07-26 11:36:46'),(36,'delete_works','works','2018-07-26 11:36:46','2018-07-26 11:36:46'),(37,'add_works','works',NULL,NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'super_admin','Super Administrador','2018-07-10 11:36:54','2018-07-28 02:00:42'),(2,'user','Cliente','2018-07-10 11:36:54','2018-07-10 11:36:54'),(3,'admin','Administrador','2018-07-28 02:01:17','2018-07-28 02:01:17'),(4,'worker','Agente','2018-08-27 12:26:13','2018-08-27 12:26:13');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (1,'Limpieza de mantención',NULL,NULL,19000),(2,'Aseo Profundo Desocupado',NULL,NULL,25000),(3,'Aseo Profundo Amoblado',NULL,NULL,10000),(4,'Servicio Planchado: 4hrs.',NULL,NULL,24500);
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `services_works`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services_works` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `work_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `services_works` WRITE;
/*!40000 ALTER TABLE `services_works` DISABLE KEYS */;
/*!40000 ALTER TABLE `services_works` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'site.title','Site Title','Limpia tu casa','','text',1,'Site'),(2,'site.description','Site Description','Limpia tu casa','','text',2,'Site'),(3,'site.logo','Site Logo','settings/August2018/VCnQaRYCGjFBjCYJcpUJ.png','','image',3,'Site'),(4,'site.google_analytics_tracking_id','Google Analytics Tracking ID',NULL,'','text',4,'Site'),(5,'admin.bg_image','Admin Background Image','settings/August2018/Y6ZOMIzby70j1FtcezqP.jpg','','image',5,'Admin'),(6,'admin.title','Admin Title','Limpia tu casa','','text',1,'Admin'),(7,'admin.description','Admin Description',NULL,'','text',2,'Admin'),(8,'admin.loader','Admin Loader','settings/July2018/GY6YbzIymUce8DlYZSDE.gif','','image',3,'Admin'),(9,'admin.icon_image','Admin Icon Image','settings/August2018/P2ydbdb10KHZrKLfLyC4.png','','image',4,'Admin'),(10,'admin.google_analytics_client_id','Google Analytics Client ID (used for admin dashboard)',NULL,'','text',1,'Admin');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL DEFAULT '2',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `rut` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (14,1,'Administrador','administrador@lobulo.cl','users/default.png','$2y$10$M3OzPcBJsd6W3yrtSG8ef.WMMgH5LqoGPU15SJr982yFmHXPegnQi','tLFdUm638zoZpDj9XpE8IbLWwI487jvyo2O85nbuHNtih4iPpSxuxFa5iuSL',NULL,NULL,NULL,'2018-10-14 16:32:48','2018-10-14 16:32:56',NULL),(19,4,'Agente 1','agente1@lobulo.cl','users/default.png','$2y$10$jIVKIjoT6tjdJccCXW3CU.bRC4S8TRu2w6l2tSFEkyEU8YvxErtSa','Um2QeCfQMKGHQSlrR5o2fFhEThFbPwcEhicM3AUJ4fp0xXjR7mRbsroiieFZ','Lóbulo','48836614',NULL,'2018-10-15 22:50:18','2018-10-15 22:56:38',NULL),(20,4,'Agente 2','agente2@lobulo.cl','users/default.png','$2y$10$Cgra85/pIP6hUUGRq9Kk6eF7eV1n/PAXA9pIp/5SmPaROoVIwhZOO',NULL,NULL,'48835514',NULL,'2018-10-15 23:06:25','2018-10-15 23:06:25',NULL),(21,2,'Sergio Ávila','sergio@lobulo.cl','users/default.png','$2y$10$xccLgs0uxdNsJk8AST1myurx707umLU3uJkcUraEmOP0Gu186ZECK','hq6diRwTcE1dy120VpStX15sbnuD23Fo5VEdlwSc59csAiBZY4Kzay9spOw4',NULL,'48836614',NULL,'2018-10-15 23:13:38','2018-10-15 23:13:38',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `work_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `work_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `work_status` WRITE;
/*!40000 ALTER TABLE `work_status` DISABLE KEYS */;
INSERT INTO `work_status` VALUES (1,'No pagado',NULL,NULL),(2,'Aceptado',NULL,NULL),(3,'Realizado',NULL,NULL),(4,'Cancelado',NULL,NULL);
/*!40000 ALTER TABLE `work_status` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `worker_added_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `worker_added_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qty_workers` tinyint(4) DEFAULT NULL,
  `combination_services_id` int(11) DEFAULT NULL,
  `qty_added_services` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `worker_added_services` WRITE;
/*!40000 ALTER TABLE `worker_added_services` DISABLE KEYS */;
INSERT INTO `worker_added_services` VALUES (1,2,143,4),(2,2,148,3),(3,2,148,4),(4,2,153,4),(5,2,158,3),(6,2,158,4),(7,2,163,3),(8,2,163,4),(9,2,168,3),(10,2,168,4),(11,2,173,3),(12,2,173,4),(13,2,178,2),(14,2,178,3),(15,2,178,4),(16,2,188,4),(17,2,193,2),(18,2,193,3),(19,2,193,4),(20,2,213,2),(21,2,213,3),(22,2,213,4),(23,3,223,4),(24,3,228,2),(25,3,228,3),(26,3,228,4),(27,3,233,2),(28,3,233,3),(29,3,233,4),(30,3,238,2),(31,3,238,3),(32,3,238,4),(33,3,248,4),(34,3,253,2),(35,3,253,3),(36,3,253,4),(37,3,258,2),(38,3,258,3),(39,3,258,4),(40,3,263,2),(41,3,263,3),(42,3,263,4),(43,3,268,2),(44,3,268,3),(45,3,268,4),(46,2,7,3),(47,2,7,4),(48,2,12,3),(49,2,12,4),(50,2,17,3),(51,2,17,4),(52,2,22,3),(53,2,22,4),(54,2,27,3),(55,2,27,4),(56,2,31,3),(57,2,31,4),(58,2,37,2),(59,2,37,3),(60,2,37,4),(61,2,42,2),(62,2,42,3),(63,2,42,4),(64,3,47,3),(65,3,47,4),(66,2,52,2),(67,2,52,3),(68,2,52,4),(69,2,57,2),(70,2,57,3),(71,2,57,4),(75,3,62,3),(76,3,62,4),(77,3,67,3),(78,3,67,4),(79,3,72,3),(80,3,72,4),(81,2,77,2),(82,2,77,3),(83,2,77,4),(84,3,82,3),(85,3,82,4),(86,3,87,3),(87,3,87,4),(88,3,92,2),(89,3,92,3),(90,3,92,4),(91,3,97,2),(92,3,97,3),(93,3,97,4),(94,3,102,2),(95,3,102,3),(96,3,102,4),(97,3,107,2),(98,3,107,3),(99,3,107,4),(100,3,112,3),(101,3,112,4),(102,3,117,2),(103,3,117,3),(104,3,117,4),(105,3,122,2),(106,3,122,3),(107,3,122,4),(108,3,127,2),(109,3,127,3),(110,3,127,4),(111,3,132,2),(112,3,132,3),(113,3,132,4);
/*!40000 ALTER TABLE `worker_added_services` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `workers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rut` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(192) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_month` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `workers` WRITE;
/*!40000 ALTER TABLE `workers` DISABLE KEYS */;
INSERT INTO `workers` VALUES (29,'Agente 1','Lobulo','48836614','17466145-4','agente1@lobulo.cl',10,'2018-10-15 22:50:00','2018-10-15 22:50:51',NULL),(30,'Agente 2','Lóbulo','48835514','18456123-3','agente2@lobulo.cl',11,'2018-10-15 23:06:25','2018-10-15 23:06:25',NULL);
/*!40000 ALTER TABLE `workers` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `workers_works`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workers_works` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `worker_id` int(11) DEFAULT NULL,
  `work_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `workers_works` WRITE;
/*!40000 ALTER TABLE `workers_works` DISABLE KEYS */;
INSERT INTO `workers_works` VALUES (19,29,4),(20,29,5);
/*!40000 ALTER TABLE `workers_works` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `working_days`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `working_days` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `worker_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=618 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `working_days` WRITE;
/*!40000 ALTER TABLE `working_days` DISABLE KEYS */;
INSERT INTO `working_days` VALUES (179,27,'2018-10-01 08:30:00',NULL,NULL),(180,27,'2018-10-02 08:30:00',NULL,NULL),(181,27,'2018-10-03 08:30:00',NULL,NULL),(182,27,'2018-10-04 08:30:00',NULL,NULL),(183,27,'2018-10-05 08:30:00',NULL,NULL),(184,27,'2018-10-08 08:30:00',NULL,NULL),(185,27,'2018-10-09 08:30:00',NULL,NULL),(186,27,'2018-10-10 08:30:00',NULL,NULL),(187,27,'2018-10-11 08:30:00',NULL,NULL),(188,27,'2018-10-12 08:30:00',NULL,NULL),(189,27,'2018-10-15 08:30:00',NULL,NULL),(190,27,'2018-10-16 08:30:00',NULL,NULL),(191,27,'2018-10-17 08:30:00',NULL,NULL),(192,27,'2018-10-18 08:30:00',NULL,NULL),(193,27,'2018-10-19 08:30:00',NULL,NULL),(194,27,'2018-10-22 08:30:00',NULL,NULL),(195,27,'2018-10-23 08:30:00',NULL,NULL),(196,27,'2018-10-24 08:30:00',NULL,NULL),(197,27,'2018-10-25 08:30:00',NULL,NULL),(198,27,'2018-10-26 08:30:00',NULL,NULL),(199,27,'2018-10-29 08:30:00',NULL,NULL),(200,27,'2018-10-30 08:30:00',NULL,NULL),(201,27,'2018-10-31 08:30:00',NULL,NULL),(202,27,'2018-11-01 08:30:00',NULL,NULL),(203,27,'2018-11-02 08:30:00',NULL,NULL),(204,27,'2018-11-05 08:30:00',NULL,NULL),(205,27,'2018-11-06 08:30:00',NULL,NULL),(206,27,'2018-11-07 08:30:00',NULL,NULL),(207,27,'2018-11-08 08:30:00',NULL,NULL),(208,27,'2018-11-09 08:30:00',NULL,NULL),(209,27,'2018-11-12 08:30:00',NULL,NULL),(210,27,'2018-11-13 08:30:00',NULL,NULL),(211,27,'2018-11-14 08:30:00',NULL,NULL),(212,27,'2018-11-15 08:30:00',NULL,NULL),(213,27,'2018-11-16 08:30:00',NULL,NULL),(214,27,'2018-11-19 08:30:00',NULL,NULL),(215,27,'2018-11-20 08:30:00',NULL,NULL),(216,27,'2018-11-21 08:30:00',NULL,NULL),(217,27,'2018-11-22 08:30:00',NULL,NULL),(218,27,'2018-11-23 08:30:00',NULL,NULL),(219,27,'2018-11-26 08:30:00',NULL,NULL),(220,27,'2018-11-27 08:30:00',NULL,NULL),(221,27,'2018-11-28 08:30:00',NULL,NULL),(222,27,'2018-11-29 08:30:00',NULL,NULL),(223,27,'2018-11-30 08:30:00',NULL,NULL),(285,8,'2018-10-18 08:30:00','2018-10-14 07:38:37','2018-10-14 07:38:37'),(286,8,'2018-10-19 08:30:00','2018-10-14 07:38:37','2018-10-14 07:38:37'),(287,8,'2018-10-20 08:30:00','2018-10-14 07:38:37','2018-10-14 07:38:37'),(288,8,'2018-10-19 08:30:00','2018-10-14 07:38:37','2018-10-14 07:38:37'),(289,8,'2018-10-17 08:30:00','2018-10-14 07:38:37','2018-10-14 07:38:37'),(290,8,'2018-10-18 08:30:00','2018-10-14 07:38:37','2018-10-14 07:38:37'),(291,8,'2018-10-17 08:30:00','2018-10-14 07:38:37','2018-10-14 07:38:37'),(292,8,'2018-10-18 08:30:00','2018-10-14 07:38:37','2018-10-14 07:38:37'),(293,8,'2018-10-14 14:00:00','2018-10-14 07:38:37','2018-10-14 07:38:37'),(294,8,'2018-10-14 14:00:00','2018-10-14 07:38:37','2018-10-14 07:38:37'),(295,8,'2018-10-14 14:00:00','2018-10-14 07:38:37','2018-10-14 07:38:37'),(296,8,'2018-10-14 14:00:00','2018-10-14 07:38:37','2018-10-14 07:38:37'),(297,8,'2018-10-14 14:00:00','2018-10-14 07:38:37','2018-10-14 07:38:37'),(298,8,'2018-10-12 14:00:00','2018-10-14 07:38:37','2018-10-14 07:38:37'),(332,15,'2018-10-10 08:30:00','2018-10-14 12:17:18','2018-10-14 12:17:18'),(333,15,'2018-10-11 08:30:00','2018-10-14 12:17:18','2018-10-14 12:17:18'),(334,15,'2018-10-19 08:30:00','2018-10-14 12:17:18','2018-10-14 12:17:18'),(335,15,'2018-10-11 14:00:00','2018-10-14 12:17:18','2018-10-14 12:17:18'),(336,15,'2018-10-10 14:00:00','2018-10-14 12:17:18','2018-10-14 12:17:18'),(337,15,'2018-10-12 14:00:00','2018-10-14 12:17:18','2018-10-14 12:17:18'),(338,15,'2018-11-09 08:30:00','2018-10-14 12:17:18','2018-10-14 12:17:18'),(339,15,'2018-11-16 08:30:00','2018-10-14 12:17:18','2018-10-14 12:17:18'),(340,15,'2018-11-15 08:30:00','2018-10-14 12:17:18','2018-10-14 12:17:18'),(341,15,'2018-11-14 08:30:00','2018-10-14 12:17:18','2018-10-14 12:17:18'),(342,15,'2018-11-10 14:00:00','2018-10-14 12:17:18','2018-10-14 12:17:18'),(343,28,'2018-11-01 08:30:00',NULL,NULL),(344,28,'2018-11-02 08:30:00',NULL,NULL),(345,28,'2018-11-03 08:30:00',NULL,NULL),(346,28,'2018-11-05 08:30:00',NULL,NULL),(347,28,'2018-11-06 08:30:00',NULL,NULL),(348,28,'2018-11-07 08:30:00',NULL,NULL),(349,28,'2018-11-08 08:30:00',NULL,NULL),(350,28,'2018-11-09 08:30:00',NULL,NULL),(351,28,'2018-11-10 08:30:00',NULL,NULL),(352,28,'2018-11-12 08:30:00',NULL,NULL),(353,28,'2018-11-13 08:30:00',NULL,NULL),(354,28,'2018-11-14 08:30:00',NULL,NULL),(355,28,'2018-11-15 08:30:00',NULL,NULL),(356,28,'2018-11-16 08:30:00',NULL,NULL),(357,28,'2018-11-17 08:30:00',NULL,NULL),(358,28,'2018-11-19 08:30:00',NULL,NULL),(359,28,'2018-11-20 08:30:00',NULL,NULL),(360,28,'2018-11-21 08:30:00',NULL,NULL),(361,28,'2018-11-22 08:30:00',NULL,NULL),(362,28,'2018-11-23 08:30:00',NULL,NULL),(363,28,'2018-11-24 08:30:00',NULL,NULL),(364,28,'2018-11-26 08:30:00',NULL,NULL),(365,28,'2018-11-27 08:30:00',NULL,NULL),(366,28,'2018-11-28 08:30:00',NULL,NULL),(367,28,'2018-11-29 08:30:00',NULL,NULL),(368,28,'2018-11-30 08:30:00',NULL,NULL),(369,28,'2018-12-01 08:30:00',NULL,NULL),(370,28,'2018-12-03 08:30:00',NULL,NULL),(371,28,'2018-12-04 08:30:00',NULL,NULL),(372,28,'2018-12-05 08:30:00',NULL,NULL),(373,28,'2018-12-06 08:30:00',NULL,NULL),(374,28,'2018-12-07 08:30:00',NULL,NULL),(375,28,'2018-12-08 08:30:00',NULL,NULL),(376,28,'2018-12-10 08:30:00',NULL,NULL),(377,28,'2018-12-11 08:30:00',NULL,NULL),(378,28,'2018-12-12 08:30:00',NULL,NULL),(379,28,'2018-12-13 08:30:00',NULL,NULL),(380,28,'2018-12-14 08:30:00',NULL,NULL),(381,28,'2018-12-15 08:30:00',NULL,NULL),(382,28,'2018-12-17 08:30:00',NULL,NULL),(383,28,'2018-12-18 08:30:00',NULL,NULL),(384,28,'2018-12-19 08:30:00',NULL,NULL),(385,28,'2018-12-20 08:30:00',NULL,NULL),(386,28,'2018-12-21 08:30:00',NULL,NULL),(387,28,'2018-12-22 08:30:00',NULL,NULL),(388,28,'2018-12-24 08:30:00',NULL,NULL),(389,28,'2018-12-25 08:30:00',NULL,NULL),(390,28,'2018-12-26 08:30:00',NULL,NULL),(391,28,'2018-12-27 08:30:00',NULL,NULL),(392,28,'2018-12-28 08:30:00',NULL,NULL),(393,28,'2018-12-29 08:30:00',NULL,NULL),(394,28,'2018-12-31 08:30:00',NULL,NULL),(505,29,'2018-10-01 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(506,29,'2018-10-02 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(507,29,'2018-10-03 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(508,29,'2018-10-04 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(509,29,'2018-10-05 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(510,29,'2018-10-06 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(511,29,'2018-10-08 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(512,29,'2018-10-09 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(513,29,'2018-10-10 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(514,29,'2018-10-11 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(515,29,'2018-10-12 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(516,29,'2018-10-13 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(517,29,'2018-10-15 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(518,29,'2018-10-16 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(519,29,'2018-10-17 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(520,29,'2018-10-18 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(521,29,'2018-10-19 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(522,29,'2018-10-20 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(523,29,'2018-10-22 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(524,29,'2018-10-23 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(525,29,'2018-10-24 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(526,29,'2018-10-25 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(527,29,'2018-10-26 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(528,29,'2018-10-27 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(529,29,'2018-10-29 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(530,29,'2018-10-30 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(531,29,'2018-10-31 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(532,29,'2018-10-26 14:00:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(533,29,'2018-10-25 14:00:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(534,29,'2018-10-18 14:00:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(535,29,'2018-10-19 14:00:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(536,29,'2018-11-01 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(537,29,'2018-11-02 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(538,29,'2018-11-03 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(539,29,'2018-11-05 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(540,29,'2018-11-06 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(541,29,'2018-11-07 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(542,29,'2018-11-08 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(543,29,'2018-11-09 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(544,29,'2018-11-10 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(545,29,'2018-11-12 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(546,29,'2018-11-13 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(547,29,'2018-11-14 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(548,29,'2018-11-15 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(549,29,'2018-11-16 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(550,29,'2018-11-17 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(551,29,'2018-11-19 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(552,29,'2018-11-20 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(553,29,'2018-11-21 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(554,29,'2018-11-22 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(555,29,'2018-11-23 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(556,29,'2018-11-24 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(557,29,'2018-11-26 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(558,29,'2018-11-27 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(559,29,'2018-11-28 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(560,29,'2018-11-29 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(561,29,'2018-11-30 08:30:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(562,29,'2018-11-26 14:00:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(563,29,'2018-11-19 14:00:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(564,29,'2018-11-12 14:00:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(565,29,'2018-11-05 14:00:00','2018-10-15 22:51:08','2018-10-15 22:51:08'),(566,30,'2018-11-01 08:30:00',NULL,NULL),(567,30,'2018-11-02 08:30:00',NULL,NULL),(568,30,'2018-11-03 08:30:00',NULL,NULL),(569,30,'2018-11-05 08:30:00',NULL,NULL),(570,30,'2018-11-06 08:30:00',NULL,NULL),(571,30,'2018-11-07 08:30:00',NULL,NULL),(572,30,'2018-11-08 08:30:00',NULL,NULL),(573,30,'2018-11-09 08:30:00',NULL,NULL),(574,30,'2018-11-10 08:30:00',NULL,NULL),(575,30,'2018-11-12 08:30:00',NULL,NULL),(576,30,'2018-11-13 08:30:00',NULL,NULL),(577,30,'2018-11-14 08:30:00',NULL,NULL),(578,30,'2018-11-15 08:30:00',NULL,NULL),(579,30,'2018-11-16 08:30:00',NULL,NULL),(580,30,'2018-11-17 08:30:00',NULL,NULL),(581,30,'2018-11-19 08:30:00',NULL,NULL),(582,30,'2018-11-20 08:30:00',NULL,NULL),(583,30,'2018-11-21 08:30:00',NULL,NULL),(584,30,'2018-11-22 08:30:00',NULL,NULL),(585,30,'2018-11-23 08:30:00',NULL,NULL),(586,30,'2018-11-24 08:30:00',NULL,NULL),(587,30,'2018-11-26 08:30:00',NULL,NULL),(588,30,'2018-11-27 08:30:00',NULL,NULL),(589,30,'2018-11-28 08:30:00',NULL,NULL),(590,30,'2018-11-29 08:30:00',NULL,NULL),(591,30,'2018-11-30 08:30:00',NULL,NULL),(592,30,'2018-12-01 08:30:00',NULL,NULL),(593,30,'2018-12-03 08:30:00',NULL,NULL),(594,30,'2018-12-04 08:30:00',NULL,NULL),(595,30,'2018-12-05 08:30:00',NULL,NULL),(596,30,'2018-12-06 08:30:00',NULL,NULL),(597,30,'2018-12-07 08:30:00',NULL,NULL),(598,30,'2018-12-08 08:30:00',NULL,NULL),(599,30,'2018-12-10 08:30:00',NULL,NULL),(600,30,'2018-12-11 08:30:00',NULL,NULL),(601,30,'2018-12-12 08:30:00',NULL,NULL),(602,30,'2018-12-13 08:30:00',NULL,NULL),(603,30,'2018-12-14 08:30:00',NULL,NULL),(604,30,'2018-12-15 08:30:00',NULL,NULL),(605,30,'2018-12-17 08:30:00',NULL,NULL),(606,30,'2018-12-18 08:30:00',NULL,NULL),(607,30,'2018-12-19 08:30:00',NULL,NULL),(608,30,'2018-12-20 08:30:00',NULL,NULL),(609,30,'2018-12-21 08:30:00',NULL,NULL),(610,30,'2018-12-22 08:30:00',NULL,NULL),(611,30,'2018-12-24 08:30:00',NULL,NULL),(612,30,'2018-12-25 08:30:00',NULL,NULL),(613,30,'2018-12-26 08:30:00',NULL,NULL),(614,30,'2018-12-27 08:30:00',NULL,NULL),(615,30,'2018-12-28 08:30:00',NULL,NULL),(616,30,'2018-12-29 08:30:00',NULL,NULL),(617,30,'2018-12-31 08:30:00',NULL,NULL);
/*!40000 ALTER TABLE `working_days` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `works`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `works` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `house_types_id` int(11) NOT NULL,
  `rooms` int(11) NOT NULL,
  `bathrooms` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `service_types_id` int(11) NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `work_status_id` int(11) NOT NULL DEFAULT '1',
  `description` text COLLATE utf8mb4_unicode_ci,
  `price` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notification_status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `works` WRITE;
/*!40000 ALTER TABLE `works` DISABLE KEYS */;
INSERT INTO `works` VALUES (4,2,1,1,'2018-10-19 08:30:00',1,'Huelén 11','48836614','Providencia','',1,NULL,18000,'2018-10-15 22:58:50','2018-10-15 22:58:50','Administrador',0),(5,2,1,2,'2018-11-01 08:30:00',1,'Huelén 11','48836614','Providencia','sergio@lobulo.cl',2,'Es una prueba de registro y creación de orden.',19000,'2018-10-15 23:13:00','2018-10-15 23:15:31','Sergio Ávila',0);
/*!40000 ALTER TABLE `works` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

