# ************************************************************
# Sequel Pro SQL dump
# Versión 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.5.5-10.2.15-MariaDB)
# Base de datos: limpiatucasa
# Tiempo de Generación: 2018-10-14 23:44:50 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla added_services
# ------------------------------------------------------------

DROP TABLE IF EXISTS `added_services`;

CREATE TABLE `added_services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `added_services` WRITE;
/*!40000 ALTER TABLE `added_services` DISABLE KEYS */;

INSERT INTO `added_services` (`id`, `name`, `price`, `created_at`, `updated_at`)
VALUES
	(1,'1 hora de planchado',6000,NULL,NULL),
	(2,'2 horas de planchado',12000,NULL,NULL),
	(3,'Limpieza interior de refrigerador',6000,NULL,NULL),
	(4,'Limpieza interior de horno',6000,NULL,NULL);

/*!40000 ALTER TABLE `added_services` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla added_services_works
# ------------------------------------------------------------

DROP TABLE IF EXISTS `added_services_works`;

CREATE TABLE `added_services_works` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `added_service_id` int(11) NOT NULL,
  `work_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `added_services_works` WRITE;
/*!40000 ALTER TABLE `added_services_works` DISABLE KEYS */;

INSERT INTO `added_services_works` (`id`, `added_service_id`, `work_id`, `created_at`, `updated_at`)
VALUES
	(1,2,5,NULL,NULL),
	(2,3,5,NULL,NULL),
	(3,2,6,NULL,NULL),
	(4,1,6,NULL,NULL),
	(5,2,7,NULL,NULL),
	(6,1,7,NULL,NULL),
	(7,1,8,NULL,NULL),
	(8,2,8,NULL,NULL),
	(9,1,9,NULL,NULL),
	(10,2,9,NULL,NULL),
	(11,1,10,NULL,NULL),
	(12,1,11,NULL,NULL),
	(13,2,11,NULL,NULL),
	(14,1,12,NULL,NULL),
	(15,2,12,NULL,NULL),
	(16,1,13,NULL,NULL),
	(17,2,13,NULL,NULL),
	(18,1,14,NULL,NULL),
	(19,2,14,NULL,NULL),
	(20,1,15,NULL,NULL),
	(21,2,15,NULL,NULL),
	(22,1,16,NULL,NULL),
	(23,1,17,NULL,NULL),
	(24,2,17,NULL,NULL),
	(25,1,18,NULL,NULL),
	(26,1,19,NULL,NULL),
	(27,2,19,NULL,NULL),
	(28,1,20,NULL,NULL),
	(29,2,20,NULL,NULL),
	(30,1,21,NULL,NULL),
	(31,2,21,NULL,NULL),
	(32,1,22,NULL,NULL),
	(33,2,22,NULL,NULL),
	(34,1,23,NULL,NULL),
	(35,2,23,NULL,NULL),
	(36,1,24,NULL,NULL),
	(37,1,25,NULL,NULL),
	(38,2,25,NULL,NULL),
	(39,1,26,NULL,NULL),
	(40,2,26,NULL,NULL),
	(41,1,27,NULL,NULL),
	(42,2,27,NULL,NULL),
	(43,1,28,NULL,NULL),
	(44,2,28,NULL,NULL),
	(45,1,29,NULL,NULL),
	(46,2,29,NULL,NULL),
	(47,1,30,NULL,NULL),
	(48,2,30,NULL,NULL),
	(49,1,33,NULL,NULL),
	(50,1,34,NULL,NULL),
	(51,1,35,NULL,NULL),
	(52,2,36,NULL,NULL),
	(53,1,37,NULL,NULL),
	(54,1,38,NULL,NULL),
	(55,1,39,NULL,NULL),
	(56,1,40,NULL,NULL),
	(57,2,41,NULL,NULL),
	(58,2,42,NULL,NULL),
	(59,2,43,NULL,NULL),
	(60,2,44,NULL,NULL),
	(61,2,45,NULL,NULL),
	(62,1,46,NULL,NULL),
	(63,1,47,NULL,NULL),
	(64,1,48,NULL,NULL),
	(65,1,49,NULL,NULL),
	(66,1,50,NULL,NULL),
	(67,1,51,NULL,NULL),
	(68,1,52,NULL,NULL),
	(69,1,53,NULL,NULL),
	(70,1,54,NULL,NULL),
	(71,1,55,NULL,NULL),
	(72,1,56,NULL,NULL),
	(73,1,57,NULL,NULL),
	(74,1,58,NULL,NULL),
	(75,1,59,NULL,NULL),
	(76,1,60,NULL,NULL),
	(77,1,61,NULL,NULL),
	(78,1,62,NULL,NULL),
	(79,1,63,NULL,NULL),
	(80,1,64,NULL,NULL),
	(81,1,65,NULL,NULL);

/*!40000 ALTER TABLE `added_services_works` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla coupons
# ------------------------------------------------------------

DROP TABLE IF EXISTS `coupons`;

CREATE TABLE `coupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla data_rows
# ------------------------------------------------------------

DROP TABLE IF EXISTS `data_rows`;

CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `data_rows` WRITE;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`)
VALUES
	(1,1,'id','number','ID',1,0,0,0,0,0,NULL,1),
	(2,1,'name','text','Name',1,1,1,1,1,1,NULL,2),
	(3,1,'email','text','Email',1,1,1,1,1,1,NULL,3),
	(4,1,'password','password','Password',1,0,0,1,1,0,NULL,4),
	(5,1,'remember_token','text','Remember Token',0,0,0,0,0,0,NULL,5),
	(6,1,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,6),
	(7,1,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,7),
	(8,1,'avatar','image','Avatar',0,0,0,0,0,0,NULL,8),
	(9,1,'user_belongsto_role_relationship','relationship','Role',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}',10),
	(10,1,'user_belongstomany_role_relationship','relationship','Roles',0,0,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}',11),
	(12,1,'settings','hidden','Settings',0,0,0,0,0,0,NULL,12),
	(13,2,'id','number','ID',1,0,0,0,0,0,'',1),
	(14,2,'name','text','Name',1,1,1,1,1,1,'',2),
	(15,2,'created_at','timestamp','Created At',0,0,0,0,0,0,'',3),
	(16,2,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'',4),
	(17,3,'id','number','ID',1,0,0,0,0,0,'',1),
	(18,3,'name','text','Name',1,1,1,1,1,1,'',2),
	(19,3,'created_at','timestamp','Created At',0,0,0,0,0,0,'',3),
	(20,3,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'',4),
	(21,3,'display_name','text','Display Name',1,1,1,1,1,1,'',5),
	(22,1,'role_id','text','Role',1,1,1,1,1,1,NULL,9),
	(23,4,'id','text','Id',1,0,0,0,0,0,NULL,1),
	(24,4,'name','text','Nombre',1,1,1,1,1,1,NULL,2),
	(25,4,'last_name','text','Apellido',1,1,1,1,1,1,NULL,3),
	(26,4,'phone','text','Teléfono',0,1,1,1,1,1,NULL,4),
	(27,4,'rut','text','Rut',1,1,1,1,1,1,NULL,5),
	(28,4,'created_at','timestamp','Creado',0,1,1,1,0,1,NULL,7),
	(29,4,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,8),
	(30,4,'worker_hasone_working_day_relationship','relationship','Días',0,0,0,1,1,1,'{\"model\":\"App\\\\WorkingDay\",\"table\":\"working_days\",\"type\":\"hasOne\",\"column\":\"id\",\"key\":\"id\",\"label\":\"id\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}',10),
	(31,4,'deleted_at','timestamp','Deleted At',0,0,0,0,0,0,NULL,9),
	(32,5,'id','text','Id',1,0,0,0,0,0,NULL,1),
	(34,5,'rooms','number','Cantidad de habitaciones',1,1,1,1,1,1,NULL,3),
	(35,5,'bathrooms','number','Cantidad de baños',1,1,1,1,1,1,NULL,4),
	(36,5,'date','datetime','Fecha',1,1,1,1,1,1,NULL,5),
	(38,5,'address','text','Dirección',1,1,1,1,1,1,NULL,7),
	(39,5,'phone','text','Teléfono',1,1,1,1,1,1,NULL,8),
	(40,5,'city','text','Comuna',1,1,1,1,1,1,NULL,9),
	(41,5,'email','text','Email',1,1,1,1,1,1,NULL,10),
	(42,5,'created_at','timestamp','Creado',0,1,1,1,0,1,NULL,11),
	(43,5,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,12),
	(45,5,'work_hasone_house_type_relationship','relationship','Tipo de vivienda',0,1,1,1,1,1,'{\"model\":\"App\\\\HouseType\",\"table\":\"house_types\",\"type\":\"belongsTo\",\"column\":\"house_types_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}',13),
	(46,5,'house_types_id','select_dropdown','Tipo de vivienda',1,0,1,0,1,0,'{\"default\":\"1\"}',2),
	(47,5,'work_hasone_service_relationship','relationship','Servicios',0,1,1,1,1,1,'{\"model\":\"App\\\\Service\",\"table\":\"services\",\"type\":\"belongsTo\",\"column\":\"service_types_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}',14),
	(48,5,'service_types_id','text','Service Types Id',1,0,0,0,1,0,NULL,6),
	(50,5,'work_belongsto_work_status_relationship','relationship','Estado',0,1,1,1,0,1,'{\"model\":\"App\\\\WorkStatus\",\"table\":\"work_status\",\"type\":\"belongsTo\",\"column\":\"work_status_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}',15),
	(51,5,'work_status_id','text','Work Status Id',1,0,0,0,0,0,NULL,13),
	(52,5,'work_hasmany_worker_relationship','relationship','Trabajadores',0,1,0,1,1,1,'{\"model\":\"App\\\\Worker\",\"table\":\"workers\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"workers_works\",\"pivot\":\"1\",\"taggable\":\"on\"}',16),
	(54,5,'work_belongstomany_added_service_relationship','relationship','Servicios adicionales',0,1,1,1,1,1,'{\"model\":\"App\\\\AddedService\",\"table\":\"added_services\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"added_services_works\",\"pivot\":\"1\",\"taggable\":\"0\"}',17),
	(55,5,'description','text','Descripción',0,1,1,1,1,1,NULL,12),
	(56,5,'price','text','Price',0,1,1,1,1,1,NULL,13),
	(57,4,'email','text','Email',0,1,1,1,1,1,NULL,6),
	(58,5,'name','text','Nombre',0,1,1,1,1,1,NULL,16),
	(59,4,'start_month','text','Mes de inicio',0,1,1,1,1,1,NULL,10),
	(60,1,'last_name','text','Last Name',0,1,1,1,1,1,NULL,8),
	(61,1,'phone','number','Phone',0,1,1,1,1,1,NULL,9);

/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla data_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `data_types`;

CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `data_types` WRITE;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`)
VALUES
	(1,'users','users','Usuario','Usuarios','voyager-person','TCG\\Voyager\\Models\\User','TCG\\Voyager\\Policies\\UserPolicy',NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-07-10 17:36:54','2018-07-11 14:59:47'),
	(2,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','',1,0,NULL,'2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(3,'roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,'','',1,0,NULL,'2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(4,'workers','workers','Agente','Agentes',NULL,'App\\Worker',NULL,'WorkersController',NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-07-11 13:58:33','2018-08-12 06:21:40'),
	(5,'works','works','Trabajo','Trabajos',NULL,'App\\Work',NULL,'WorksController',NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-07-26 17:36:46','2018-08-06 17:35:03');

/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla house_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `house_types`;

CREATE TABLE `house_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `house_types` WRITE;
/*!40000 ALTER TABLE `house_types` DISABLE KEYS */;

INSERT INTO `house_types` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'Casa',NULL,NULL),
	(2,'Departamento',NULL,NULL);

/*!40000 ALTER TABLE `house_types` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla menu_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menu_items`;

CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`)
VALUES
	(1,1,'Inicio','','_self','voyager-boat','#000000',NULL,1,'2018-07-10 17:36:54','2018-10-14 23:43:36','voyager.dashboard','null'),
	(3,1,'Usuarios','','_self','voyager-person','#000000',5,2,'2018-07-10 17:36:54','2018-10-14 23:42:57','voyager.users.index','null'),
	(4,1,'Roles','','_self','voyager-lock',NULL,5,1,'2018-07-10 17:36:54','2018-10-14 23:42:57','voyager.roles.index',NULL),
	(5,1,'Tools','','_self','voyager-tools',NULL,NULL,4,'2018-07-10 17:36:54','2018-10-14 23:42:57',NULL,NULL),
	(6,1,'Menu Builder','','_self','voyager-list',NULL,5,3,'2018-07-10 17:36:54','2018-10-14 23:42:57','voyager.menus.index',NULL),
	(7,1,'Database','','_self','voyager-data',NULL,5,4,'2018-07-10 17:36:54','2018-10-14 23:42:57','voyager.database.index',NULL),
	(9,1,'BREAD','','_self','voyager-bread',NULL,5,6,'2018-07-10 17:36:54','2018-10-14 23:42:57','voyager.bread.index',NULL),
	(12,1,'Agentes','','_self','voyager-people','#000000',NULL,2,'2018-07-11 13:58:33','2018-10-14 23:42:57','voyager.workers.index','null'),
	(13,1,'Ordenes de trabajo','','_self','voyager-hammer','#000000',NULL,3,'2018-07-26 17:36:47','2018-10-14 23:42:57','voyager.works.index','null');

/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla menus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menus`;

CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'admin','2018-07-10 17:36:54','2018-07-10 17:36:54');

/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2014_10_12_000000_create_users_table',1),
	(2,'2014_10_12_100000_create_password_resets_table',1),
	(3,'2016_01_01_000000_add_voyager_user_fields',1),
	(4,'2016_01_01_000000_create_data_types_table',1),
	(5,'2016_05_19_173453_create_menu_table',1),
	(6,'2016_10_21_190000_create_roles_table',1),
	(7,'2016_10_21_190000_create_settings_table',1),
	(8,'2016_11_30_135954_create_permission_table',1),
	(9,'2016_11_30_141208_create_permission_role_table',1),
	(10,'2016_12_26_201236_data_types__add__server_side',1),
	(11,'2017_01_13_000000_add_route_to_menu_items_table',1),
	(12,'2017_01_14_005015_create_translations_table',1),
	(13,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',1),
	(14,'2017_03_06_000000_add_controller_to_data_types_table',1),
	(15,'2017_04_21_000000_add_order_to_data_rows_table',1),
	(16,'2017_07_05_210000_add_policyname_to_data_types_table',1),
	(17,'2017_08_05_000000_add_group_to_settings_table',1),
	(18,'2017_11_26_013050_add_user_role_relationship',1),
	(19,'2017_11_26_015000_create_user_roles_table',1),
	(20,'2018_03_11_000000_add_user_settings',1),
	(21,'2018_03_14_000000_add_details_to_data_types_table',1),
	(22,'2018_03_16_000000_make_settings_value_nullable',1),
	(23,'2018_07_11_195041_create_data_rows_table',2),
	(24,'2018_07_11_195041_create_data_types_table',2),
	(25,'2018_07_11_195041_create_menu_items_table',2),
	(26,'2018_07_11_195041_create_menus_table',2),
	(27,'2018_07_11_195041_create_permission_role_table',2),
	(28,'2018_07_11_195041_create_permissions_table',2),
	(29,'2018_07_11_195041_create_roles_table',2),
	(30,'2018_07_11_195041_create_settings_table',2),
	(31,'2018_07_11_195041_create_translations_table',2),
	(32,'2018_07_11_195041_create_user_roles_table',2),
	(33,'2018_07_11_195041_create_workers_table',2),
	(34,'2018_07_11_195043_add_foreign_keys_to_data_rows_table',2),
	(35,'2018_07_11_195043_add_foreign_keys_to_menu_items_table',2),
	(36,'2018_07_11_195043_add_foreign_keys_to_permission_role_table',2),
	(37,'2018_07_11_195043_add_foreign_keys_to_user_roles_table',2);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla permission_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permission_role`;

CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;

INSERT INTO `permission_role` (`permission_id`, `role_id`)
VALUES
	(1,1),
	(1,2),
	(1,3),
	(1,4),
	(2,1),
	(2,2),
	(3,1),
	(4,1),
	(5,1),
	(6,1),
	(7,1),
	(8,1),
	(9,1),
	(10,1),
	(11,1),
	(11,3),
	(12,1),
	(12,3),
	(13,1),
	(13,3),
	(14,1),
	(14,3),
	(15,1),
	(15,3),
	(16,1),
	(16,3),
	(17,1),
	(17,3),
	(18,1),
	(18,3),
	(19,1),
	(19,3),
	(20,1),
	(20,3),
	(21,1),
	(22,1),
	(23,1),
	(24,1),
	(25,1),
	(26,1),
	(27,1),
	(27,3),
	(28,1),
	(28,3),
	(29,1),
	(29,3),
	(30,1),
	(30,3),
	(31,1),
	(31,3),
	(32,1),
	(32,3),
	(32,4),
	(33,1),
	(33,3),
	(33,4),
	(34,1),
	(34,3),
	(36,1),
	(36,3),
	(37,1);

/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`)
VALUES
	(1,'browse_admin',NULL,'2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(2,'browse_bread',NULL,'2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(3,'browse_database',NULL,'2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(4,'browse_media',NULL,'2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(5,'browse_compass',NULL,'2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(6,'browse_menus','menus','2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(7,'read_menus','menus','2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(8,'edit_menus','menus','2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(9,'add_menus','menus','2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(10,'delete_menus','menus','2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(11,'browse_roles','roles','2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(12,'read_roles','roles','2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(13,'edit_roles','roles','2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(14,'add_roles','roles','2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(15,'delete_roles','roles','2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(16,'browse_users','users','2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(17,'read_users','users','2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(18,'edit_users','users','2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(19,'add_users','users','2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(20,'delete_users','users','2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(21,'browse_settings','settings','2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(22,'read_settings','settings','2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(23,'edit_settings','settings','2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(24,'add_settings','settings','2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(25,'delete_settings','settings','2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(26,'browse_hooks',NULL,'2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(27,'browse_workers','workers','2018-07-11 13:58:33','2018-07-11 13:58:33'),
	(28,'read_workers','workers','2018-07-11 13:58:33','2018-07-11 13:58:33'),
	(29,'edit_workers','workers','2018-07-11 13:58:33','2018-07-11 13:58:33'),
	(30,'add_workers','workers','2018-07-11 13:58:33','2018-07-11 13:58:33'),
	(31,'delete_workers','workers','2018-07-11 13:58:33','2018-07-11 13:58:33'),
	(32,'browse_works','works','2018-07-26 17:36:46','2018-07-26 17:36:46'),
	(33,'read_works','works','2018-07-26 17:36:46','2018-07-26 17:36:46'),
	(34,'edit_works','works','2018-07-26 17:36:46','2018-07-26 17:36:46'),
	(36,'delete_works','works','2018-07-26 17:36:46','2018-07-26 17:36:46'),
	(37,'add_works','works',NULL,NULL);

/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`)
VALUES
	(1,'super_admin','Super Administrador','2018-07-10 17:36:54','2018-07-28 08:00:42'),
	(2,'user','Cliente','2018-07-10 17:36:54','2018-07-10 17:36:54'),
	(3,'admin','Administrador','2018-07-28 08:01:17','2018-07-28 08:01:17'),
	(4,'worker','Agente','2018-08-27 17:26:13','2018-08-27 17:26:13');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla services
# ------------------------------------------------------------

DROP TABLE IF EXISTS `services`;

CREATE TABLE `services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;

INSERT INTO `services` (`id`, `name`, `created_at`, `updated_at`, `price`)
VALUES
	(1,'Limpieza de mantención',NULL,NULL,19000),
	(2,'Aseo Profundo Desocupado',NULL,NULL,25000),
	(3,'Aseo Profundo Amoblado',NULL,NULL,10000),
	(4,'Servicio de planchado',NULL,NULL,24500);

/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla services_works
# ------------------------------------------------------------

DROP TABLE IF EXISTS `services_works`;

CREATE TABLE `services_works` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `work_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`)
VALUES
	(1,'site.title','Site Title','Limpia tu casa','','text',1,'Site'),
	(2,'site.description','Site Description','Limpia tu casa','','text',2,'Site'),
	(3,'site.logo','Site Logo','settings/August2018/VCnQaRYCGjFBjCYJcpUJ.png','','image',3,'Site'),
	(4,'site.google_analytics_tracking_id','Google Analytics Tracking ID',NULL,'','text',4,'Site'),
	(5,'admin.bg_image','Admin Background Image','settings/August2018/Y6ZOMIzby70j1FtcezqP.jpg','','image',5,'Admin'),
	(6,'admin.title','Admin Title','Limpia tu casa','','text',1,'Admin'),
	(7,'admin.description','Admin Description',NULL,'','text',2,'Admin'),
	(8,'admin.loader','Admin Loader','settings/July2018/GY6YbzIymUce8DlYZSDE.gif','','image',3,'Admin'),
	(9,'admin.icon_image','Admin Icon Image','settings/August2018/P2ydbdb10KHZrKLfLyC4.png','','image',4,'Admin'),
	(10,'admin.google_analytics_client_id','Google Analytics Client ID (used for admin dashboard)',NULL,'','text',1,'Admin');

/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla translations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `translations`;

CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla user_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL DEFAULT 2,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `password`, `remember_token`, `last_name`, `phone`, `settings`, `created_at`, `updated_at`)
VALUES
	(11,4,'Agente1','agente1@lobulo.cl','users/default.png','$2y$10$NzMT/NmXghRum8HxyChHRePSIqVBzQzxEgjFqcgLRbm29VNCX3D1W',NULL,NULL,NULL,NULL,'2018-10-14 21:28:34','2018-10-14 21:28:34'),
	(12,4,'Agente2','agente2@lobulo.cl','users/default.png','$2y$10$qts7izP/I5ElmWeI3gm2cuOUk5aT3sPkLvggpkvk69iHMqB7.CDDW',NULL,NULL,NULL,NULL,'2018-10-14 21:28:59','2018-10-14 21:28:59'),
	(13,4,'Agente3','agente3@lobulo.cl','users/default.png','$2y$10$pn0dBt5WQvpi1j45IJNO2u76xGT0zlJlXpPT131pUQYbw6ZeCGrxu',NULL,NULL,NULL,NULL,'2018-10-14 21:29:26','2018-10-14 21:29:26'),
	(14,1,'Administrador','administrador@lobulo.cl','users/default.png','$2y$10$M3OzPcBJsd6W3yrtSG8ef.WMMgH5LqoGPU15SJr982yFmHXPegnQi','8YT5Fv8FwNsGK2AznMG4w8TSxkqMNFGapxVwDAER4mCVcQ5EJ3sNha1j2wY1',NULL,NULL,NULL,'2018-10-14 21:32:48','2018-10-14 21:32:56'),
	(15,2,'Usuario1','usuario1@lobulo.cl','users/default.png','$2y$10$GGH1a2kkH1E6fCj2T3Va9e8elHToPLZSTPq6c/ay.AV1T9wLkgqcu',NULL,NULL,NULL,NULL,'2018-10-14 21:34:01','2018-10-14 21:34:01'),
	(16,2,'Usuario2','usuario2@lobulo.cl','users/default.png','$2y$10$jdLhPUXy7NmfFd0WGpRVnus6rYu4wN/Tnuz7zHiExHFyov0wMbSc.','vGbAhA0j0rHI75VvgkJZXZUFSceFihLQBaYGHHVp423J5kEW1qyzES0TVdW0',NULL,NULL,NULL,'2018-10-14 21:34:27','2018-10-14 21:34:27'),
	(17,4,'Agente4','agente4@lobulo.cl','users/default.png','$2y$10$pKlQlJRF2vzTosbpyLeRteEq7.gCtVkxd3F2hqWgs9mfin5u5vbZ2',NULL,NULL,'48836614',NULL,'2018-10-14 22:17:07','2018-10-14 22:17:07');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla work_status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `work_status`;

CREATE TABLE `work_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `work_status` WRITE;
/*!40000 ALTER TABLE `work_status` DISABLE KEYS */;

INSERT INTO `work_status` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'No pagado',NULL,NULL),
	(2,'Aceptado',NULL,NULL),
	(3,'Realizado',NULL,NULL),
	(4,'Cancelado',NULL,NULL);

/*!40000 ALTER TABLE `work_status` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla worker_quantities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `worker_quantities`;

CREATE TABLE `worker_quantities` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `house_types_id` int(11) DEFAULT NULL,
  `rooms` int(11) DEFAULT NULL,
  `bathrooms` int(11) DEFAULT NULL,
  `services_quantity` int(11) DEFAULT NULL,
  `amount_workers` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `worker_quantities` WRITE;
/*!40000 ALTER TABLE `worker_quantities` DISABLE KEYS */;

INSERT INTO `worker_quantities` (`id`, `house_types_id`, `rooms`, `bathrooms`, `services_quantity`, `amount_workers`, `price`)
VALUES
	(1,1,1,1,1,1,20000),
	(2,1,1,1,2,1,20000),
	(3,1,1,1,3,1,20000),
	(5,1,1,1,4,1,20000),
	(6,1,1,1,5,1,20000),
	(7,1,1,2,1,1,22000),
	(8,1,1,2,2,1,22000),
	(9,1,1,2,3,1,22000),
	(10,1,1,2,4,2,22000),
	(11,1,1,2,5,2,22000),
	(12,1,1,3,1,1,24000),
	(13,1,1,3,2,1,24000),
	(14,1,1,3,3,1,24000),
	(15,1,1,3,4,2,24000),
	(16,1,1,3,5,2,24000),
	(17,1,2,1,1,1,22000),
	(18,1,2,1,2,1,22000),
	(19,1,2,1,3,1,22000),
	(20,1,2,1,4,2,22000),
	(21,1,2,1,5,2,22000),
	(22,1,2,2,1,1,24000),
	(23,1,2,2,2,1,24000),
	(24,1,2,2,3,1,24000),
	(25,1,2,2,4,2,24000),
	(26,1,2,2,5,2,24000),
	(27,1,2,3,1,2,26000),
	(28,1,2,3,2,2,26000),
	(29,1,2,3,3,2,26000),
	(30,1,2,3,4,2,26000),
	(31,1,3,1,1,1,24000),
	(32,1,3,1,2,1,24000),
	(33,1,3,1,3,1,24000),
	(34,1,3,1,4,2,24000),
	(35,1,3,1,5,2,24000),
	(37,1,3,2,1,1,26000),
	(38,1,3,2,2,1,26000),
	(39,1,3,2,3,2,26000),
	(40,1,3,2,4,2,26000),
	(41,1,3,2,5,2,26000),
	(42,1,3,3,1,1,28000),
	(43,1,3,3,2,1,28000),
	(44,1,3,3,3,2,28000),
	(45,1,3,3,4,2,28000),
	(46,1,3,3,5,2,28000),
	(47,1,3,4,1,2,36000),
	(48,1,3,4,2,2,36000),
	(49,1,3,4,3,2,36000),
	(50,1,3,4,4,3,36000),
	(51,1,3,4,5,3,36000),
	(52,1,4,1,1,1,26000),
	(53,1,4,1,2,1,26000),
	(54,1,4,1,3,2,26000),
	(55,1,4,1,4,2,26000),
	(56,1,4,1,5,2,26000),
	(57,1,4,2,1,1,28000),
	(58,1,4,2,2,1,28000),
	(59,1,4,2,3,2,28000),
	(60,1,4,2,4,2,28000),
	(61,1,4,2,5,2,28000),
	(62,1,4,3,1,2,36000),
	(63,1,4,3,2,2,36000),
	(64,1,4,3,3,2,36000),
	(65,1,4,3,4,3,36000),
	(66,1,4,3,5,3,36000),
	(67,1,4,4,1,2,38000),
	(68,1,4,4,2,2,38000),
	(69,1,4,4,3,2,38000),
	(70,1,4,4,4,3,38000),
	(71,1,4,4,5,3,38000),
	(72,1,4,5,1,2,40000),
	(73,1,4,5,2,2,40000),
	(74,1,4,5,3,2,40000),
	(75,1,4,5,4,3,40000),
	(76,1,4,5,5,3,40000),
	(77,1,5,1,1,1,28000),
	(78,1,5,1,2,1,28000),
	(79,1,5,1,3,2,28000),
	(80,1,5,1,4,2,28000),
	(81,1,5,1,5,2,28000),
	(82,1,5,2,1,2,36000),
	(83,1,5,2,2,2,36000),
	(84,1,5,2,3,2,36000),
	(85,1,5,2,4,3,36000),
	(86,1,5,2,5,3,36000),
	(87,1,5,3,1,2,38000),
	(88,1,5,3,2,2,38000),
	(89,1,5,3,3,2,38000),
	(90,1,5,3,4,3,38000),
	(91,1,5,3,5,3,38000),
	(92,1,5,4,1,2,40000),
	(93,1,5,4,2,2,40000),
	(94,1,5,4,3,3,40000),
	(95,1,5,4,4,3,40000),
	(96,1,5,4,5,3,40000),
	(97,1,5,5,1,2,42000),
	(98,1,5,5,2,2,42000),
	(99,1,5,5,3,3,42000),
	(100,1,5,5,4,3,42000),
	(101,1,5,5,5,3,42000),
	(102,1,5,6,1,2,44000),
	(103,1,5,6,2,2,44000),
	(104,1,5,6,3,3,44000),
	(105,1,5,6,4,3,44000),
	(106,1,5,6,5,3,44000),
	(107,1,6,1,1,2,42000),
	(108,1,6,1,2,2,42000),
	(109,1,6,1,3,3,42000),
	(110,1,6,1,4,3,42000),
	(111,1,6,1,5,3,42000),
	(112,1,6,2,1,2,38000),
	(113,1,6,2,2,2,38000),
	(114,1,6,2,3,2,38000),
	(115,1,6,2,4,3,38000),
	(116,1,6,2,5,3,38000),
	(117,1,6,3,1,2,40000),
	(118,1,6,3,2,2,40000),
	(119,1,6,3,3,3,40000),
	(120,1,6,3,4,3,40000),
	(121,1,6,3,5,3,40000),
	(122,1,6,4,1,2,42000),
	(123,1,6,4,2,2,42000),
	(124,1,6,4,3,3,42000),
	(125,1,6,4,4,3,42000),
	(126,1,6,4,5,3,42000),
	(127,1,6,5,1,2,44000),
	(128,1,6,5,2,2,44000),
	(129,1,6,5,3,3,44000),
	(130,1,6,5,4,3,44000),
	(131,1,6,5,5,3,44000),
	(132,1,6,6,1,2,46000),
	(133,1,6,6,2,2,46000),
	(134,1,6,6,3,3,46000),
	(135,1,6,6,4,3,46000),
	(136,1,6,6,5,3,46000),
	(137,1,2,3,5,2,26000),
	(138,2,1,1,1,1,18000),
	(139,2,1,1,2,1,18000),
	(140,2,1,1,3,1,18000),
	(141,2,1,1,4,1,18000),
	(142,2,1,1,5,1,18000),
	(143,2,1,2,1,1,19000),
	(144,2,1,2,2,1,19000),
	(145,2,1,2,3,1,19000),
	(146,2,1,2,4,1,19000),
	(147,2,1,2,5,2,19000),
	(148,2,1,3,1,1,21000),
	(149,2,1,3,2,1,21000),
	(150,2,1,3,3,1,21000),
	(151,2,1,3,4,1,21000),
	(152,2,1,3,5,2,21000),
	(153,2,2,1,1,2,19000),
	(154,2,2,1,2,2,19000),
	(155,2,2,1,3,2,19000),
	(156,2,2,1,4,2,19000),
	(157,2,2,1,5,2,19000),
	(158,2,2,2,1,1,21000),
	(159,2,2,2,2,1,21000),
	(160,2,2,2,3,1,21000),
	(161,2,2,2,4,1,21000),
	(162,2,2,2,5,2,21000),
	(163,2,2,3,1,1,23000),
	(164,2,2,3,2,1,23000),
	(165,2,2,3,3,1,23000),
	(166,2,2,3,4,1,23000),
	(167,2,2,3,5,2,23000),
	(168,2,3,1,1,1,21000),
	(169,2,3,1,2,1,21000),
	(170,2,3,1,3,1,21000),
	(171,2,3,1,4,1,21000),
	(172,2,3,1,5,2,21000),
	(173,2,3,2,1,1,23000),
	(174,2,3,2,2,1,23000),
	(175,2,3,2,3,1,23000),
	(176,2,3,2,4,1,23000),
	(177,2,3,2,5,2,23000),
	(178,2,3,3,1,1,25000),
	(179,2,3,3,2,1,25000),
	(180,2,3,3,3,2,25000),
	(181,2,3,3,4,2,25000),
	(182,2,3,3,5,2,25000),
	(183,2,3,4,1,2,34000),
	(184,2,3,4,2,2,34000),
	(185,2,3,4,3,2,34000),
	(186,2,3,4,4,2,34000),
	(187,2,3,4,5,2,34000),
	(188,2,4,1,1,1,23000),
	(189,2,4,1,2,1,23000),
	(190,2,4,1,3,1,23000),
	(191,2,4,1,4,1,23000),
	(192,2,4,1,5,2,23000),
	(193,2,4,2,1,1,25000),
	(194,2,4,2,2,1,25000),
	(195,2,4,2,3,2,25000),
	(196,2,4,2,4,2,25000),
	(197,2,4,2,5,2,25000),
	(198,2,4,3,1,2,34000),
	(199,2,4,3,2,2,34000),
	(200,2,4,3,3,2,34000),
	(201,2,4,3,4,2,34000),
	(202,2,4,3,5,2,34000),
	(203,2,4,4,1,2,37000),
	(204,2,4,4,2,2,37000),
	(205,2,4,4,3,2,37000),
	(206,2,4,4,4,2,37000),
	(207,2,4,4,5,2,37000),
	(208,2,4,5,1,2,39000),
	(209,2,4,5,2,2,39000),
	(210,2,4,5,3,2,39000),
	(211,2,4,5,4,2,39000),
	(212,2,4,5,5,2,39000),
	(213,2,5,1,1,1,25000),
	(214,2,5,1,2,1,25000),
	(215,2,5,1,3,2,25000),
	(216,2,5,1,4,2,25000),
	(217,2,5,1,5,2,25000),
	(218,2,5,2,1,2,34000),
	(219,2,5,2,2,2,34000),
	(220,2,5,2,3,2,34000),
	(221,2,5,2,4,2,34000),
	(222,2,5,2,5,2,34000),
	(223,2,5,3,1,2,37000),
	(224,2,5,3,2,2,37000),
	(225,2,5,3,3,2,37000),
	(226,2,5,3,4,2,37000),
	(227,2,5,3,5,3,37000),
	(228,2,5,4,1,2,38000),
	(229,2,5,4,2,2,38000),
	(230,2,5,4,3,3,38000),
	(231,2,5,4,4,3,38000),
	(232,2,5,4,5,3,38000),
	(233,2,5,5,1,2,40000),
	(234,2,5,5,2,2,40000),
	(235,2,5,5,3,3,40000),
	(236,2,5,5,4,3,40000),
	(237,2,5,5,5,3,40000),
	(238,2,5,6,1,3,41000),
	(239,2,5,6,2,3,41000),
	(240,2,5,6,3,3,41000),
	(241,2,5,6,4,3,41000),
	(242,2,5,6,5,3,41000),
	(243,2,6,1,1,2,34000),
	(244,2,6,1,2,2,34000),
	(245,2,6,1,3,2,34000),
	(246,2,6,1,4,2,34000),
	(247,2,6,1,5,2,34000),
	(248,2,6,2,1,2,37000),
	(249,2,6,2,2,2,37000),
	(250,2,6,2,3,2,37000),
	(251,2,6,2,4,2,37000),
	(252,2,6,2,5,3,37000),
	(253,2,6,3,1,2,38000),
	(254,2,6,3,2,2,38000),
	(255,2,6,3,3,3,38000),
	(256,2,6,3,4,3,38000),
	(257,2,6,3,5,3,38000),
	(258,2,6,4,1,2,40000),
	(259,2,6,4,2,2,40000),
	(260,2,6,4,3,3,40000),
	(261,2,6,4,4,3,40000),
	(262,2,6,4,5,3,40000),
	(263,2,6,5,1,2,41000),
	(264,2,6,5,2,2,41000),
	(265,2,6,5,3,3,41000),
	(266,2,6,5,4,3,41000),
	(267,2,6,5,5,3,41000),
	(268,2,6,6,1,2,42000),
	(269,2,6,6,2,2,42000),
	(270,2,6,6,3,3,42000),
	(271,2,6,6,4,3,42000),
	(272,2,6,6,5,3,42000);

/*!40000 ALTER TABLE `worker_quantities` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla workers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `workers`;

CREATE TABLE `workers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rut` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(192) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_month` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `workers` WRITE;
/*!40000 ALTER TABLE `workers` DISABLE KEYS */;

INSERT INTO `workers` (`id`, `name`, `last_name`, `phone`, `rut`, `email`, `start_month`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(29,'Agente4','Agente','48836614','176711333','agente4@lobulo.cl',10,'2018-10-14 22:17:07','2018-10-14 22:17:07',NULL);

/*!40000 ALTER TABLE `workers` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla workers_works
# ------------------------------------------------------------

DROP TABLE IF EXISTS `workers_works`;

CREATE TABLE `workers_works` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `worker_id` int(11) DEFAULT NULL,
  `work_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `workers_works` WRITE;
/*!40000 ALTER TABLE `workers_works` DISABLE KEYS */;

INSERT INTO `workers_works` (`id`, `worker_id`, `work_id`)
VALUES
	(1,8,2),
	(2,12,2),
	(5,12,20),
	(6,12,22),
	(7,15,10),
	(8,15,11),
	(9,15,15),
	(10,12,23),
	(11,12,24),
	(12,12,25),
	(13,12,27),
	(14,12,33),
	(15,27,60),
	(16,27,61),
	(17,27,62),
	(18,27,64);

/*!40000 ALTER TABLE `workers_works` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla working_days
# ------------------------------------------------------------

DROP TABLE IF EXISTS `working_days`;

CREATE TABLE `working_days` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `worker_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `working_days` WRITE;
/*!40000 ALTER TABLE `working_days` DISABLE KEYS */;

INSERT INTO `working_days` (`id`, `worker_id`, `date`, `created_at`, `updated_at`)
VALUES
	(179,27,'2018-10-01 08:30:00',NULL,NULL),
	(180,27,'2018-10-02 08:30:00',NULL,NULL),
	(181,27,'2018-10-03 08:30:00',NULL,NULL),
	(182,27,'2018-10-04 08:30:00',NULL,NULL),
	(183,27,'2018-10-05 08:30:00',NULL,NULL),
	(184,27,'2018-10-08 08:30:00',NULL,NULL),
	(185,27,'2018-10-09 08:30:00',NULL,NULL),
	(186,27,'2018-10-10 08:30:00',NULL,NULL),
	(187,27,'2018-10-11 08:30:00',NULL,NULL),
	(188,27,'2018-10-12 08:30:00',NULL,NULL),
	(189,27,'2018-10-15 08:30:00',NULL,NULL),
	(190,27,'2018-10-16 08:30:00',NULL,NULL),
	(191,27,'2018-10-17 08:30:00',NULL,NULL),
	(192,27,'2018-10-18 08:30:00',NULL,NULL),
	(193,27,'2018-10-19 08:30:00',NULL,NULL),
	(194,27,'2018-10-22 08:30:00',NULL,NULL),
	(195,27,'2018-10-23 08:30:00',NULL,NULL),
	(196,27,'2018-10-24 08:30:00',NULL,NULL),
	(197,27,'2018-10-25 08:30:00',NULL,NULL),
	(198,27,'2018-10-26 08:30:00',NULL,NULL),
	(199,27,'2018-10-29 08:30:00',NULL,NULL),
	(200,27,'2018-10-30 08:30:00',NULL,NULL),
	(201,27,'2018-10-31 08:30:00',NULL,NULL),
	(202,27,'2018-11-01 08:30:00',NULL,NULL),
	(203,27,'2018-11-02 08:30:00',NULL,NULL),
	(204,27,'2018-11-05 08:30:00',NULL,NULL),
	(205,27,'2018-11-06 08:30:00',NULL,NULL),
	(206,27,'2018-11-07 08:30:00',NULL,NULL),
	(207,27,'2018-11-08 08:30:00',NULL,NULL),
	(208,27,'2018-11-09 08:30:00',NULL,NULL),
	(209,27,'2018-11-12 08:30:00',NULL,NULL),
	(210,27,'2018-11-13 08:30:00',NULL,NULL),
	(211,27,'2018-11-14 08:30:00',NULL,NULL),
	(212,27,'2018-11-15 08:30:00',NULL,NULL),
	(213,27,'2018-11-16 08:30:00',NULL,NULL),
	(214,27,'2018-11-19 08:30:00',NULL,NULL),
	(215,27,'2018-11-20 08:30:00',NULL,NULL),
	(216,27,'2018-11-21 08:30:00',NULL,NULL),
	(217,27,'2018-11-22 08:30:00',NULL,NULL),
	(218,27,'2018-11-23 08:30:00',NULL,NULL),
	(219,27,'2018-11-26 08:30:00',NULL,NULL),
	(220,27,'2018-11-27 08:30:00',NULL,NULL),
	(221,27,'2018-11-28 08:30:00',NULL,NULL),
	(222,27,'2018-11-29 08:30:00',NULL,NULL),
	(223,27,'2018-11-30 08:30:00',NULL,NULL),
	(285,8,'2018-10-18 08:30:00','2018-10-14 12:38:37','2018-10-14 12:38:37'),
	(286,8,'2018-10-19 08:30:00','2018-10-14 12:38:37','2018-10-14 12:38:37'),
	(287,8,'2018-10-20 08:30:00','2018-10-14 12:38:37','2018-10-14 12:38:37'),
	(288,8,'2018-10-19 08:30:00','2018-10-14 12:38:37','2018-10-14 12:38:37'),
	(289,8,'2018-10-17 08:30:00','2018-10-14 12:38:37','2018-10-14 12:38:37'),
	(290,8,'2018-10-18 08:30:00','2018-10-14 12:38:37','2018-10-14 12:38:37'),
	(291,8,'2018-10-17 08:30:00','2018-10-14 12:38:37','2018-10-14 12:38:37'),
	(292,8,'2018-10-18 08:30:00','2018-10-14 12:38:37','2018-10-14 12:38:37'),
	(293,8,'2018-10-14 14:00:00','2018-10-14 12:38:37','2018-10-14 12:38:37'),
	(294,8,'2018-10-14 14:00:00','2018-10-14 12:38:37','2018-10-14 12:38:37'),
	(295,8,'2018-10-14 14:00:00','2018-10-14 12:38:37','2018-10-14 12:38:37'),
	(296,8,'2018-10-14 14:00:00','2018-10-14 12:38:37','2018-10-14 12:38:37'),
	(297,8,'2018-10-14 14:00:00','2018-10-14 12:38:37','2018-10-14 12:38:37'),
	(298,8,'2018-10-12 14:00:00','2018-10-14 12:38:37','2018-10-14 12:38:37'),
	(332,15,'2018-10-10 08:30:00','2018-10-14 17:17:18','2018-10-14 17:17:18'),
	(333,15,'2018-10-11 08:30:00','2018-10-14 17:17:18','2018-10-14 17:17:18'),
	(334,15,'2018-10-19 08:30:00','2018-10-14 17:17:18','2018-10-14 17:17:18'),
	(335,15,'2018-10-11 14:00:00','2018-10-14 17:17:18','2018-10-14 17:17:18'),
	(336,15,'2018-10-10 14:00:00','2018-10-14 17:17:18','2018-10-14 17:17:18'),
	(337,15,'2018-10-12 14:00:00','2018-10-14 17:17:18','2018-10-14 17:17:18'),
	(338,15,'2018-11-09 08:30:00','2018-10-14 17:17:18','2018-10-14 17:17:18'),
	(339,15,'2018-11-16 08:30:00','2018-10-14 17:17:18','2018-10-14 17:17:18'),
	(340,15,'2018-11-15 08:30:00','2018-10-14 17:17:18','2018-10-14 17:17:18'),
	(341,15,'2018-11-14 08:30:00','2018-10-14 17:17:18','2018-10-14 17:17:18'),
	(342,15,'2018-11-10 14:00:00','2018-10-14 17:17:18','2018-10-14 17:17:18'),
	(343,29,'2018-10-01 08:30:00',NULL,NULL),
	(344,29,'2018-10-02 08:30:00',NULL,NULL),
	(345,29,'2018-10-03 08:30:00',NULL,NULL),
	(346,29,'2018-10-04 08:30:00',NULL,NULL),
	(347,29,'2018-10-05 08:30:00',NULL,NULL),
	(348,29,'2018-10-06 08:30:00',NULL,NULL),
	(349,29,'2018-10-08 08:30:00',NULL,NULL),
	(350,29,'2018-10-09 08:30:00',NULL,NULL),
	(351,29,'2018-10-10 08:30:00',NULL,NULL),
	(352,29,'2018-10-11 08:30:00',NULL,NULL),
	(353,29,'2018-10-12 08:30:00',NULL,NULL),
	(354,29,'2018-10-13 08:30:00',NULL,NULL),
	(355,29,'2018-10-15 08:30:00',NULL,NULL),
	(356,29,'2018-10-16 08:30:00',NULL,NULL),
	(357,29,'2018-10-17 08:30:00',NULL,NULL),
	(358,29,'2018-10-18 08:30:00',NULL,NULL),
	(359,29,'2018-10-19 08:30:00',NULL,NULL),
	(360,29,'2018-10-20 08:30:00',NULL,NULL),
	(361,29,'2018-10-22 08:30:00',NULL,NULL),
	(362,29,'2018-10-23 08:30:00',NULL,NULL),
	(363,29,'2018-10-24 08:30:00',NULL,NULL),
	(364,29,'2018-10-25 08:30:00',NULL,NULL),
	(365,29,'2018-10-26 08:30:00',NULL,NULL),
	(366,29,'2018-10-27 08:30:00',NULL,NULL),
	(367,29,'2018-10-29 08:30:00',NULL,NULL),
	(368,29,'2018-10-30 08:30:00',NULL,NULL),
	(369,29,'2018-10-31 08:30:00',NULL,NULL),
	(370,29,'2018-11-01 08:30:00',NULL,NULL),
	(371,29,'2018-11-02 08:30:00',NULL,NULL),
	(372,29,'2018-11-03 08:30:00',NULL,NULL),
	(373,29,'2018-11-05 08:30:00',NULL,NULL),
	(374,29,'2018-11-06 08:30:00',NULL,NULL),
	(375,29,'2018-11-07 08:30:00',NULL,NULL),
	(376,29,'2018-11-08 08:30:00',NULL,NULL),
	(377,29,'2018-11-09 08:30:00',NULL,NULL),
	(378,29,'2018-11-10 08:30:00',NULL,NULL),
	(379,29,'2018-11-12 08:30:00',NULL,NULL),
	(380,29,'2018-11-13 08:30:00',NULL,NULL),
	(381,29,'2018-11-14 08:30:00',NULL,NULL),
	(382,29,'2018-11-15 08:30:00',NULL,NULL),
	(383,29,'2018-11-16 08:30:00',NULL,NULL),
	(384,29,'2018-11-17 08:30:00',NULL,NULL),
	(385,29,'2018-11-19 08:30:00',NULL,NULL),
	(386,29,'2018-11-20 08:30:00',NULL,NULL),
	(387,29,'2018-11-21 08:30:00',NULL,NULL),
	(388,29,'2018-11-22 08:30:00',NULL,NULL),
	(389,29,'2018-11-23 08:30:00',NULL,NULL),
	(390,29,'2018-11-24 08:30:00',NULL,NULL),
	(391,29,'2018-11-26 08:30:00',NULL,NULL),
	(392,29,'2018-11-27 08:30:00',NULL,NULL),
	(393,29,'2018-11-28 08:30:00',NULL,NULL),
	(394,29,'2018-11-29 08:30:00',NULL,NULL),
	(395,29,'2018-11-30 08:30:00',NULL,NULL);

/*!40000 ALTER TABLE `working_days` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla works
# ------------------------------------------------------------

DROP TABLE IF EXISTS `works`;

CREATE TABLE `works` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `house_types_id` int(11) NOT NULL,
  `rooms` int(11) NOT NULL,
  `bathrooms` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `service_types_id` int(11) NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `work_status_id` int(11) NOT NULL DEFAULT 1,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notification_status` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
