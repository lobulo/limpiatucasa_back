var final_price, total_price, house_type, main_service, discount = 0
var total_schedules = 1
var added_services = []
var schedules = []

var today = new Date();
var tomorrow = new Date();
tomorrow.setDate(today.getDate() + 1);

var schedule_focused

function set_service(service_id,name,price){
  $(".info-pasos-room-total p").html(name)
  main_service = service_id
}

function verifySchedulesEmpty(){
  // verify that the schedules are not empty
  // let empty = 0;
  let full_text_schedules = 0
  $('.input-date').each(function(){
    if($(this).val().trim() != '' ){
      full_text_schedules++
    }
  })
  // verify that  $('.btn_hour.active').length == full_text_schedules
  if($('.btn-additional-other.active').length == full_text_schedules) return false
  else return true
}

function calculatePrice(){ 
  if(verifySchedulesEmpty() == false) {
    var baths = $("[name='bathrooms']").val() ? $("[name='bathrooms']").val() : 0;
    var rooms = $("[name='rooms']").val() ? $("[name='rooms']").val() : 0;
    // console.log(house_type,main_service,baths,rooms,JSON.stringify(added_services),JSON.stringify(schedules));    
    $.ajax({
      url:'/price_workers',
      method:'POST',
      data:{_token:$("[name=_token]").val(),house_type:house_type,main_service:main_service,baths:baths,rooms:rooms,added_services : JSON.stringify(added_services), schedules:JSON.stringify(schedules)},
      success:function(data){
        if(data.data){
          if(!data.data['total_price']){
            $('.content-paso-04').css('visibility','visible')
            $('.content-paso-05').css('visibility','hidden')
            $(".precio-resumen").html(currencyFormat(data.data['partial_price']));
          }  else {
            $('.content-paso-04, .content-paso-05').css('visibility','visible')
            $(".precio-resumen").html(currencyFormat(data.data['partial_price']));
            $(".valor-total").html(currencyFormat(total + data.data['total_price']));
          }     
        }else {
          $(".precio-resumen").html(currencyFormat(0));
          $(".valor-total").html(currencyFormat(0));
          alert('No existe detalle de precio para estas cantidades de Habitaciones y Baños')
        }

/*         var total = 0;
        $(".btn-additional-hora.active").each(function(index){
          if($(this)){
            total += $(this).data('price');
          }
        });
        if(data.data){
          $(".precio-resumen").html(currencyFormat(data.data['price']));
          $(".valor-total").html(currencyFormat(total + data.data['price']));
          $("[name='quantity']").val(data.data['amount_workers']);
          $("[name='price']").val(total + data.data['price']);
        } */
        console.log(data);
      }
    })
  }
  else {
    alert('verifique que los calendarios sean completados')
  }
}

//varias fechas por orden
function ShowDetailsSchedulesSidebar(){
  total_schedules != 1 ? $(".multiplier").html(" x "+total_schedules) : $(".multiplier").html('')
  if (total_schedules > 3 ) {
    $('.codigo-descuento').hide()
    $('.total-descuento').html('10% Descuento en el total')
    discount = 10
  } else {
    discount = 0
    $('.total-descuento').html('')
  } 
}

function substractSchedule(el){
  //sacamos el calendario del array schedules, pero antes hay que obtener el date del input y las horas del boton hour active
  console.log($(el));
  // let el_text = $(el.target).siblings().find('.input-date.dateInput');
  // console.log(el_text);
/* ∫∫∫ */
  //eliminamos todo el contenedor:
  // $(el_text).parent().parent().remove();
}

function getHtmlOptionsFromArray(object_options){
  options_html=''
  object_options.forEach(element => {
    console.log(element);
    options_html+=`<option value=${element.id}>${element.name}</option>`;                                           
  });
  return options_html
}

function addSchedule(){
  op_html = getHtmlOptionsFromArray(obj_added_services)
  total_schedules++
  html_schedule = `
  <div class="workdate" id=workdate_${total_schedules}>
    <div class="content-btn-hour">
      <span class="btn-building btn-hour btn-additional-other hour-8" onclick="$(&quot;input#date-${total_schedules}&quot;).val($(&quot;input#dateInput-${total_schedules}&quot;).val()+&quot 08:30&quot);toggleHour(this);">08:30</span>
      <span class="btn-building btn-hour btn-additional-other hour-14" onclick="$(&quot;input#date-${total_schedules}&quot;).val($(&quot;input#dateInput-${total_schedules}&quot;).val()+&quot 14:00&quot);toggleHour(this);">14:00</span>
    </div>
    <div class="form-group content-schedule" style="visibility : hidden">
      <input type="text" id="dateInput-${total_schedules}" class="form-control input-date dateInput" autocomplete=off readonly="readonly">  
      <select id="sel_mul_1" class="selectpicker" multiple>${op_html}</select>
      <span class="btn-building btn-add" onclick="substractSchedule($(this))" >-</span>
    </div>
  </div>
  `

  $("#extra_dates").append(html_schedule)
  input_shedule_added = $('.workdate').last().find('.input-date').eq(0)
  setScheduleFormat(input_shedule_added)
}

function setScheduleFormat(input_schedule){
  input_schedule.datepicker({
    startDate: tomorrow, //added by abel
    endDate: new Date(new Date().getFullYear(), new Date().getMonth()+2, 0),
    daysOfWeekDisabled: [0],
    multidate: false,
    language: 'es',
    weekStart: 1,
    format: 'yyyy-mm-dd',
    // datesDisabled:disabled_dates['am'],
  });
  input_schedule.datepicker().on('changeDate', function(e) {
    if(schedule_focused!=$(e.target).val()){
      // let hour = $('#'+e.target.id).parent().parent().find('.btn-additional-other.active').html()
      let hour = $(e.target).parent().parent().find('.btn-additional-other.active').html()
      let date = $(e.target).val() + ' ' + hour
      if (schedules.indexOf(date) != -1){
        alert('Elija otro horario, éste ya fue seleccionado')
        e.target.value='';
      } else {
        schedules.push(date)
        // calculatePrice();
      } 
    }
  });
}

function handleTerms(checkbox){
  (checkbox.checked == true) ? $("#submit").removeClass("disabled") : $("#submit").addClass("disabled")
}

function currencyFormat(num) {
  return '$' + num.toFixed(0).replace(/(\\d)(?=(\\d{3})+(?!\\d))/g, '$1.')
}

function toggleHour(el){
  if(!$(el).hasClass('active')){
    let id_grandfather = $(el).parent().parent().attr('id')
    let num_schedule = id_grandfather.substr('workdate_'.length)
    $(el).hasClass('hour-8') ? $('#dateInput-'+num_schedule).datepicker('setDatesDisabled', disabled_dates['am']) : $('#dateInput-'+num_schedule).datepicker('setDatesDisabled', disabled_dates['pm'])
    //si al cambiar la hora, el calendario ya tenia una fecha, antes de vaciar el input text, hay que eliminar del array schedules, esa fecha 
    if($('#dateInput-'+num_schedule).val() != '' ) {
      let date_temp = $('#dateInput-'+num_schedule).val() + ' ' + $(el).siblings('.btn-additional-other').html
      schedules.splice(schedules.indexOf(date_temp), 1 )
    }
    $('#dateInput-'+num_schedule).val('')
    $(el).siblings('.btn-additional-other').removeClass('active')
    $(el).toggleClass('active');
    $(el).parent().siblings('.content-schedule').css('visibility', 'visible')
  }
}

function coupon(code){
  if (code != '') {
    $.ajax({
      url:'/validate_coupon',
      method:'POST',
      data:{_token:$("[name=_token]").val(),code:code},
      success:function(data){
        if (data.discount) {
          $('#discount').val(data.discount)
          $('#msg-coupon').html('Cupón válido por un '+data.discount+'% de descuento en el total')
          total_price()
        }
        else{
          $('#msg-coupon').html('Cupón inválido.')
          total_price()
        }
      }
    })
  }
}

// added by abel - cuando se cambia de type_house o  servicio, las cantidades de baños y habitasciones,  los servicios adicionales y los calendarios, se tienen que resetar
function reset_related_controls(){
  $('.content-paso-04').addClass('disabled'); // - sacar del sidebar detalle, el resumen del servicio
  $('.services-added.row').html('') // - vaciar las rows de servicios adicionales en el sidebar de detalle
  // - descontar / sacar los precios y nombres de serviios adicionales
  if ($('.btn-additional.btn-additional-hora.active').length > 0){
    $('.control_added').remove()
  }
  $('.btn-additional').removeClass('active') // - desactivar los span servicios_adicionales
  $('.content-additional-services, .content-additional-schedule, .content-additional-data, .content-paso-04, .content-paso-05').css('visibility','hidden')
  //eliminar calendarios
  $('#dateInput-1').val('')
  $('#date-1').val('');
  $('.content-btn-hour').css('visibility','hidden')
  $('.btn-hour').removeClass('active')
  $('#extra_dates').html('')
}

$(document).ready(function(){
  console.log(obj_added_services);
  $('.content-services, .content-rooms, .content-additional-services, .content-additional-schedule, .content-additional-data, .content-paso-04, .content-paso-05').css('visibility','hidden')

  $('.btn-home').click(function(e){
    let id_control = $(this).attr("id")
    house_type = id_control.substr('house_id_'.length)
    $('.btn-home').removeClass('active');
    $(this).addClass('active');
    $('.content-paso-02').removeClass('disabled');
    $('.content-services').fadeIn();
    $('.content-services').css('visibility', 'visible')
    $('.btn-services').removeClass('active')
    $("[name=house_types_id]").val('')
  });

  $('.btn-services').click(function(){
    $('.btn-services').removeClass('active');
    $(this).addClass('active');
    $('.content-paso-03').removeClass('disabled');
    $('.content-rooms').fadeIn();
    $('.content-rooms').css('visibility', 'visible')
    reset_related_controls()
    $('.last-rooms, .first-rooms').val('')
  });
  
  $('.last-rooms, .first-rooms').on('change', function(){
    if( $('.first-rooms').val() == '' ||  $('.last-rooms').val() == ''){
      reset_related_controls()
    }else {
      $('.content-paso-04').removeClass('disabled');
      $('.info-pasos-room-bed').removeClass('disabled');
      $('.info-pasos-room-room').removeClass('disabled');
      $('.content-additional-services').fadeIn();
      $('.content-additional-schedule').fadeIn();
      $('.info-pasos-room-room, .info-pasos-room-total, .text-total, .valor-total').removeClass('disabled');
      $('.content-additional-services').css('visibility', 'visible')
      $('.content-additional-schedule').css('visibility', 'visible')
      $('.content-additional-data').css('visibility', 'visible')
      $('.content-btn-hour').css('visibility','visible')
      $(".info-pasos-room-bed span").html($("[name='rooms']").val())
      $(".info-pasos-room-room span").html($("[name='bathrooms']").val());
      calculatePrice();
    }
  })

  $('.content-btn-additional span.btn-additional').on('click', function(){
    let id_control = $(this).attr('id')
    let id_added_service = id_control.substr('aditional_service_'.length)
    $(this).hasClass('active') ? added_services.splice(added_services.indexOf(id_added_service), 1 ) : added_services.push(id_added_service)
    $(this).toggleClass('active')
    console.log(added_services)
    if ($('.btn-additional.btn-additional-hora.active').length == 0) {
      $('.services-added.row').html('')
    } else {
      $('.services-added').html('<div class="col-lg-12"><h5>Servicios Adicionales</h5></div>')
      $('.btn-additional.btn-additional-hora.active').each(function( index ) {
        $('.services-added').append('<div class="col-lg-6">\
        <div class="info-pasos-room info-pasos-room-plancha">\
        <span>'+$(this).html()+'</span>\
        <p>$'+$( this ).data('price')+'</p>\
        </div>\
        </div>');
      });
    }
    calculatePrice();
  })

  // evento que permite capturar la fecha actual del calendario, antes de ser cambiada por el datepicker
  $('.input-date').focus(function(e){
    schedule_focused = e.target.value;
  })

  setScheduleFormat($('#dateInput-1'))

  $('.form-check-input-pago').click(function(){
    $('.btn-next, .codigo-descuento').removeClass('disabled');
  });

  $('#label-si').click(function(){
    $('.content-password').fadeIn();
  });
  $('#label-no').click(function(){
    $('.content-password').fadeOut();
  });

  $("#newWork").on('submit',function(e){
    if($("#password").val() != $("#re_password").val()){
      e.preventDefault();
      alert("Las contraseñas tienen que ser las mismas");
    }
  });

  //Resumen servicio
  $('[data-toggle="tooltip"]').tooltip();

  $.fn.datepicker.dates['es'] = {
    days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dic"],
    today: "Hoy",
    clear: "Limpiar",
    format: "dd/mm/yyyy",
    titleFormat: "MM yyyy",
    weekStart: 1
  };

  //form validation
  jQuery.extend(jQuery.validator.messages, {
    required: "Este campo es obligatorio.",
    remote: "Por favor, rellena este campo.",
    email: "Por favor, escribe una dirección de correo válida",
    url: "Por favor, escribe una URL válida.",
    date: "Por favor, escribe una fecha válida.",
    dateISO: "Por favor, escribe una fecha (ISO) válida.",
    number: "Por favor, escribe un número entero válido.",
    digits: "Por favor, escribe sólo dígitos.",
    creditcard: "Por favor, escribe un número de tarjeta válido.",
    equalTo: "Por favor, escribe el mismo valor de nuevo.",
    accept: "Por favor, escribe un valor con una extensión aceptada.",
    maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
    minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
    rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
    range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
    max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
    min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
  });
  $("#newWork").validate({
    rules: {
      email: { required: true },
      name: { required: true },
      address: { required: true },
      city: { required: true },
      phone: { required: true, minlength:8,maxlength:8},
      name: { required: true }
    },
    messages: {
      "email": {
        required: function () { toastr.error('Debes ingresar un correo electrónico válido') },
      },
      "name": {
        required: function () { toastr.error('Debes ingresar tu nombre') },
      },
      "address": {
        required: function () { toastr.error('Debes ingresar tu dirección') },
      },
      "city": {
        required: function () { toastr.error('Debes ingresar tu comuna') },
      },
      "phone": {
        required: function () { toastr.error('Debes ingresar tu teléfono') },
      },
    }
  });
});
