var final_price, total_price, house_type, main_service, discount = 0

var total_schedules = 1
var added_services = []
var schedules = []
var arr_obj_schedules = []
var current_last_id = 0;
var qty_schedules = 0

// la siguente variable se setea manualmente para despues compararla al momento de elegir un servicio acioanal. esto tiene que cambiarse si se modifica el orden en la base de datos
var manual_settings1 = 'Limpieza de mantención'
var selected_service = ''
var today = new Date();
var tomorrow = new Date();
tomorrow.setDate(today.getDate() + 1);

var schedule_focused

function set_service(service_id,name,price){
  $(".info-pasos-room-total p").html(name)
  main_service = service_id
}
// funcion que muestra las selecciones actuales de sitio, servicio, num habitaciones y numero de baños
function showSelection(){
  $('#span_type_house').html($('.btn-home.active>small').html())
  $('#span_main_service').html($('.btn-services.active').html())
  $('#span_rooms').html($('.first-rooms').val())
  $('#span_baths').html($('.last-rooms').val())
}

function resetAddSchedule(){
  $('#div_schedule, #div_paid, #div_submit').hide()
  arr_obj_schedules.length =  0
  qty_schedules = 0
  current_last_id = 0;
  $('.cont_schedule').remove();
  $('.cont_schedules').css('visibility', 'visible')
  $('.content-additional-data').css('visibility', 'visible')
  node_schedule = addedSchedule()
  setEventsShedules(node_schedule)
  $('.cont_schedules').append(node_schedule)
  selected_service == manual_settings1  ? $('.cont_selectpicker').css('display','block') : $('.cont_selectpicker').css('display','none')
}
function reserveSchedules(){
  let empty_controls = false
  let different_password = false
  $('input[required],select[required]').each(function(i,req){
    if($(req).val().trim()=='') {
      empty_controls = true
      return false
    }
  })
  if($('input[name=registration]:checked').val()=='1'){
    if($('#password').val().trim() != $('#re_password').val().trim()){
      different_password = true
      alert('las contraseñas para el registro de usuario no coinciden')
    }
  }
  let length = 0;
  arr_obj_schedules.forEach( () => { length++ }) //obetenemos la longitud real del array
  if(length>0 && empty_controls === false && different_password === false){
    var baths = $("[name='bathrooms']").val() ? $("[name='bathrooms']").val() : 0;
    var rooms = $("[name='rooms']").val() ? $("[name='rooms']").val() : 0;
    var name = $('#nameInput').val();
    var address = $('#addressInput').val();
    var comuna = $('#comunaInput').val();
    var phone = $('#phoneInput').val();
    var email = $('#mailInput').val();
    var pass = $('#password').val()
    var observations = $('#observations').val()
    $.ajax({
      url:'/price_workers',
      method:'POST',
      data:{
        _token:$("[name=_token]").val(),
        option:'reserve',
        house_type:house_type,
        main_service:main_service,
        baths:baths,
        rooms:rooms,
        schedules:JSON.stringify(arr_obj_schedules),
        name:name,
        address:address,
        comuna:comuna,
        phone:phone,
        email:email,
        pass:pass,
        observations:observations
      },
      success:function(data){
        console.log(data)
        if(data.status ===  true ){
          window.location.href = data.url_order_paid;    
        }else {
          alert(data.msg);
        }
      }
    }); 
  }
}
function calculatePrice(){
  let length = 0;
  arr_obj_schedules.forEach( () => { length++ }) //obetenemos la longitud real del array
  if(length>0){
    var baths = $("[name='bathrooms']").val() ? $("[name='bathrooms']").val() : 0;
    var rooms = $("[name='rooms']").val() ? $("[name='rooms']").val() : 0;
    // console.log(house_type,main_service,baths,rooms,JSON.stringify(added_services),JSON.stringify(schedules));
    $.ajax({
      url:'/price_workers',
      method:'POST',
      data:{
        _token:$("[name=_token]").val(),
        option:'calculate',
        house_type:house_type,
        main_service:main_service,
        baths:baths,rooms:rooms,
        schedules:JSON.stringify(arr_obj_schedules)
      },
      success:function(data){
        console.log(data)
        if(data.status === false ){
          alert(data.msg);
          $('#div_schedule, #div_paid, #div_submit').hide()
        }else {
          $('#div_schedule, #div_paid, #div_submit').show()
          let service_price = data.service_price
          showSelection()
          let el_sche = document.getElementById('div_schedule')
          let el_paid = document.getElementById('div_paid');
          $('.schedule_row, .paid_row').remove();

          // Render schedules
          data.schedules.forEach( sche => {
            let el_sche_row = document.createElement('div');
            el_sche_row.classList.add('schedule_row')

            let el_sche_first_col = document.createElement('div');
            el_sche_first_col.classList.add('schedule_first_col')
            el_sche_first_col.innerHTML = `<span>+ ${sche.datetime}</span><span>$ ${number_format(data.service_price)}</span>`
            el_sche_row.append(el_sche_first_col)
            if(sche.added_services.length>0){
              let el_sche_second_col = document.createElement('div');
              el_sche_second_col.classList.add('schedule_second_col')
              sche.added_services.forEach( as => {
                let el_added_service_row = document.createElement('div');
                el_added_service_row.classList.add('added_service_row')
                el_added_service_row.innerHTML=`<span>+ ${as.name}</span><span>$ ${number_format(as.price)}</span>`
                el_sche_second_col.append(el_added_service_row)
              })
              el_sche_row.append(el_sche_second_col)
            }
            el_sche.append(el_sche_row)
          })
          // Render, sub_total an Total
          let el_sub_total = document.createElement('div');
          let el_discount = document.createElement('div');
          let el_total = document.createElement('div');

          el_sub_total.classList.add('paid_row')
          el_discount.classList.add('paid_row')
          el_total.classList.add('paid_row')
          // [el_sub_total,el_discount,el_total].classList.add('paid_row')

          el_sub_total.innerHTML = `<span>Total Servicios Solicitados: </span><span>$  &nbsp${number_format(data.partial_price)}</span>`
          el_discount.innerHTML = `<span>Descuento &nbsp(${data.discount} %): </span><span>$  &nbsp ${number_format(data.total_discount)}</span>`
          el_total.innerHTML = `<span>Total a Pagar: </span><span>$  &nbsp${number_format(data.total_price)}</span>`

          el_paid.append(el_sub_total)
          el_paid.append(el_discount)
          el_paid.append(el_total);
          // $('#div_resume').fadeIn( "slow");
        }
      }
    })
  }
}

function handleTerms(checkbox){
  (checkbox.checked == true) ? $("#submit").removeClass("disabled") : $("#submit").addClass("disabled")
}

function currencyFormat(num) {
  return '$' + num.toFixed(0).replace(/(\\d)(?=(\\d{3})+(?!\\d))/g, '$1.')
}

/*
function coupon(code){
  if (code != '') {
    $.ajax({
      url:'/validate_coupon',
      method:'POST',
      data:{_token:$("[name=_token]").val(),code:code},
      success:function(data){
        if (data.discount) {
          $('#discount').val(data.discount)
          $('#msg-coupon').html('Cupón válido por un '+data.discount+'% de descuento en el total')
          total_price()
        }
        else{
          $('#msg-coupon').html('Cupón inválido.')
          total_price()
        }
      }
    })
  }
}
*/

$(document).ready(function(){
  // $('#div_').hide();
  // $('#div_resume>div').hide();
  $('#div_schedule, #div_paid, #div_submit').hide();
  $('.content-services, .content-rooms, .content-additional-services, .cont_schedules, .content-additional-data, .content-paso-04, .content-paso-05').css('visibility','hidden')

  $('.btn-home').click(function(e){
    $('#div_schedule, #div_paid, #div_submit').hide();
    selected_service='';
    let id_control = $(this).attr("id")
    house_type = id_control.substr('house_id_'.length)
    $('.btn-home').removeClass('active');
    $(this).addClass('active');
    $('.last-rooms, .first-rooms').val('')
    showSelection()
    $('.content-paso-02').removeClass('disabled');
    $('.content-services').fadeIn();
    $('.content-services').css('visibility', 'visible')
    $('.btn-services').removeClass('active')
    $("[name=house_types_id]").val('')
    $('.content-rooms').css('visibility', 'hidden')
    $('.cont_schedules').css('visibility', 'hidden')
    $('.content-additional-data').css('visibility', 'hidden')
    $('.last-rooms, .first-rooms').val('')
    $('.cont_schedule').remove();
  });

  $('.btn-services').click(function(){
    if(this.innerHTML != selected_service){
      $('#div_schedule, #div_paid, #div_submit').hide()
      $('.btn-services').removeClass('active');
      $(this).addClass('active');
      showSelection()
      selected_service = this.innerHTML
      $('.last-rooms, .first-rooms').val('')
      
      if(main_service == 4 ){ // otra configuracion manual -- si elegimos servicio de planchado
        $('.content-rooms').css('visibility', 'hidden')
        $('.content-rooms').css('display','none') //css('visibility', 'hidden')
        resetAddSchedule();
      }else{
        $('.content-rooms').css('visibility', 'visible')
        resetAddSchedule();
        $('.content-rooms').css('display','block')//css('visibility', 'visible')
        $('.cont_schedules').css('visibility', 'hidden')
        $('.content-additional-data').css('visibility', 'hidden')
      }
    }
  });

  $('.last-rooms, .first-rooms').on('change', function(){
    showSelection()
    if( $('.first-rooms').val() != null &&  $('.last-rooms').val() != null){ // solo se actuará cuando ambos sean distintos del vacios
      $('.cont_schedules').css('visibility', 'visible')
      $('.content-additional-data').css('visibility', 'visible')
      if( arr_obj_schedules.filter(e=>e!=null).length > 0) { // agregamos el primer cont_schedule
        // ya existen cont_schedule (s), para lo cual solo hay que  recalcular el precio ante un cambio de cantidad de habitaciones y/o baños
        calculatePrice();
      }
    }
  })

  $('.form-check-input-pago').click(function(){
    $('.btn-next, .codigo-descuento').removeClass('disabled');
  });

  $('#label-si').click(function(){
    $('.content-password').fadeIn();
    $('#password, #re_password').prop('required',true);
  });

  $('#label-no').click(function(){
    $('.content-password').fadeOut();
    $('#password, #re_password').prop('required',false);
  });

  $("#newWork").submit(function(e){
    e.preventDefault();
    reserveSchedules();
  });

  $('#newWork').on('keyup keypress', function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
      e.preventDefault();
      return false;
    }
  });


  //Resumen servicio
  $('[data-toggle="tooltip"]').tooltip();

  $.fn.datepicker.dates['es'] = {
    days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dic"],
    today: "Hoy",
    clear: "Limpiar",
    format: "dd/mm/yyyy",
    titleFormat: "MM yyyy",
    weekStart: 1,
  };

  //form validation
  jQuery.extend(jQuery.validator.messages, {
    required: "Este campo es obligatorio.",
    remote: "Por favor, rellena este campo.",
    email: "Por favor, escribe una dirección de correo válida",
    url: "Por favor, escribe una URL válida.",
    date: "Por favor, escribe una fecha válida.",
    dateISO: "Por favor, escribe una fecha (ISO) válida.",
    number: "Por favor, escribe un número entero válido.",
    digits: "Por favor, escribe sólo dígitos.",
    creditcard: "Por favor, escribe un número de tarjeta válido.",
    equalTo: "Por favor, escribe el mismo valor de nuevo.",
    accept: "Por favor, escribe un valor con una extensión aceptada.",
    maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
    minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
    rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
    range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
    max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
    min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
  });
  $("#newWork").validate({
    rules: {
      email: { required: true },
      name: { required: true },
      address: { required: true },
      city: { required: true },
      phone: { required: true, minlength:8,maxlength:8},
      name: { required: true }
    },
    messages: {
      "email": {
        required: function () { toastr.error('Debes ingresar un correo electrónico válido') },
      },
      "name": {
        required: function () { toastr.error('Debes ingresar tu nombre') },
      },
      "address": {
        required: function () { toastr.error('Debes ingresar tu dirección') },
      },
      "city": {
        required: function () { toastr.error('Debes ingresar tu comuna') },
      },
      "phone": {
        required: function () { toastr.error('Debes ingresar tu teléfono') },
      },
      "password": {
        required: function () { toastr.error('Debes ingresar Contraseña') },
      }
    }
  });
});
