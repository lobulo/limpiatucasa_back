
//* FUNCTION QUE INTENTA ENCONTRAR UN REGISTRO ANTERIOR IGUAL A LA HORA Y FECHA ACTUAL QUE SE ESTA INTENTANDO AGREGAR
//* si encuentra, devuelve el indice del cont_schedule	
function findSchedule(hour, calendar){
	let found = false
	leng_array = arr_obj_schedules.length
	arr_obj_schedules.forEach( (el,idx)=> {
		if(el!=null){
			if(el.hour == hour &&  el.calendar == calendar )
				found = idx
		}
	})
	return found
}

//* FUNCION QUE INICIALZA UN SELETPICKER
function initializeSelectpicker(selectpickercito){
	$(selectpickercito).selectpicker('deselectAll')
	$(selectpickercito).selectpicker('refresh');
	selectpickercito.parentNode.parentNode.parentNode.querySelector('.filter-option-inner-inner').innerHTML = 'Ningun Serv. Adicional'
	selectpickercito.parentNode.parentNode.parentNode.querySelector('.filter-option').style.background = '#ededed'
	selectpickercito.parentNode.parentNode.parentNode.querySelector('.filter-option').style.color = 'black'
}

//* FUNCTION FOR SET UP SETTINGS OF NODE SCHEDULE
function setEventsShedules(node){
	let node_datepicker = node.querySelector('.datepicker')
	let node_selectpicker = node.querySelector('.selectpicker')

	//* ENVENTOS PARA LOS  SPAN ADDED/REMOVE  SCHEDULES
	if(node.querySelector('.added_schedule')){
		node.querySelector('.added_schedule').addEventListener('click', ev => {
			node_schedule = addedSchedule()
			setEventsShedules(node_schedule)
			$('.cont_schedules').append(node_schedule)
			selected_service == manual_settings1  ? $('.cont_selectpicker').css('display','block') : $('.cont_selectpicker').css('display','none')
		})
	}
	if(node.querySelector('.remove_schedule'))
		node.querySelector('.remove_schedule').addEventListener('click', ev => { 
			index_cont = ev.target.parentNode.parentNode.id
			if ( index_cont != undefined ){
				// arr_obj_schedules.splice( arr_obj_schedules.indexOf(index_cont), 1 );
				arr_obj_schedules.forEach((el,idx)=>{
					if(idx == index_cont ) {
						arr_obj_schedules[idx]=null
						calculatePrice();
					}
				})
			}
			removeSchedule(ev.target) 

		})

	//* EVENTOS PARA LOS SPAN BOTONES HORA
	node.querySelectorAll('.span_hour').forEach( el => {
		el.addEventListener('click', ev => {
			if(!ev.target.classList.contains('active')){
				ev.target.classList.add('active')
				ev.target.parentNode.nextSibling.querySelector('.datepicker').value != ''
				if(ev.target.parentNode.nextSibling.querySelector('.datepicker').value != '' ){ //quiere decir que el calendario antes fue agregado al arreglo y hay que sacarlo y volver a calcular el precio, y volver a pintar el resumen
					// arr_obj_schedules.splice( arr_obj_schedules.indexOf(ev.target.parentNode.parentNode.id), 1 );
					// arr_obj_schedules=arr_obj_schedules.filter((e,idx)=> { return !(idx==ev.target.parentNode.parentNode.id)})
					arr_obj_schedules.forEach((el,idx)=>{
						if(idx == ev.target.parentNode.parentNode.id ) {
							arr_obj_schedules[idx]=null
							calculatePrice();
						}
					})
				}
				node_datepicker.disabled = false
				node_datepicker.value = ''
				$(node_selectpicker).attr('disabled',true)
				initializeSelectpicker(node_selectpicker)
				// node_selectpicker.disabled = true;
				if (ev.target.innerHTML == '8:30' ) {
					$(node_datepicker).datepicker('setDatesDisabled', disabled_dates['am'])
					// $(node_datepicker).datepicker('setDatesDisabled', ['2018-12-24', '2018-12-25'])
					ev.target.nextSibling.classList.remove('active')
				}else {
					$(node_datepicker).datepicker('setDatesDisabled', disabled_dates['pm'])
					// $(node_datepicker).datepicker('setDatesDisabled', ['2018-12-24', '2018-12-25'])
					ev.target.previousSibling.classList.remove('active')
				}
			}
		})
	})

	//* EVENTOS PARA EL DATEPICKER
	$(node_datepicker).focus(function(e){ schedule_focused = e.target.value; })
	$(node_datepicker).on('changeDate', (ev) => {
		if(schedule_focused!=$(ev.target).val()){
			index_find = findSchedule(ev.target.parentNode.previousSibling.querySelector('.active').innerHTML, ev.target.value)
			index_cont = ev.target.parentNode.parentNode.id
			if(schedule_focused != '') { // ESTAMOS MODIFICANDO UN SCHEDULE EXISTENTE
				if ( index_find === false || index_find === parseInt(index_cont) ) { // SI NO SE ENCONTRÓ o  SI SE ENCONTRO PERO ES EL MISMO
					arr_obj_schedules[index_cont].hour = ev.target.parentNode.previousSibling.querySelector('.active').innerHTML
					arr_obj_schedules[index_cont].calendar = ev.target.value
					arr_obj_schedules[index_cont].added_services = []
					$(node_selectpicker).attr('disabled',false)
					initializeSelectpicker(node_selectpicker)
					calculatePrice();

				}else {
					alert('Esta hora y fecha ya fueron elegidas')
					$(node_datepicker).datepicker('setDate',schedule_focused)
				}
			}else {
				if ( index_find === false ){
					arr_obj_schedules[index_cont] = {
						hour : ev.target.parentNode.previousSibling.querySelector('.active').innerHTML,
						calendar : ev.target.value,
						added_services : []
					}
					$(node_selectpicker).attr('disabled',false)
					initializeSelectpicker(node_selectpicker)
					calculatePrice();
				}else {
					alert('Esta hora y fecha ya fueron elegidas')
					$(node_datepicker).datepicker('setDate',schedule_focused)
				}
			}
			// console.log(arr_obj_schedules)
		}
	})

	//* EVENTOS PARA EL SELECTPICKER
	node_selectpicker.addEventListener('change', ev => {
		node_values = $(ev.target).val()
		qty_selected_items = node_values.length
		if(qty_selected_items == 0) {
			console.log('= 0 ',qty_selected_items)
			initializeSelectpicker(node_selectpicker)
		} else {
			// console.log('> 0 ',qty_selected_items)
			node.querySelector('.filter-option').style.background = '#4BB68D'
			node.querySelector('.filter-option').style.color = 'white'
			if(qty_selected_items == 1){
				ev.target.nextSibling.querySelector('.filter-option-inner-inner').textContent = qty_selected_items + ' servicio adicional'
				// console.log(ev.target.nextSibling.querySelector('.filter-option-inner-inner').textContent)
			}else{
				ev.target.nextSibling.querySelector('.filter-option-inner-inner').textContent = qty_selected_items + ' servicios adicionales' 
				// console.log(ev.target.nextSibling.querySelector('.filter-option-inner-inner').textContent)
			}
		}
		arr_obj_schedules[ev.target.parentNode.parentNode.parentNode.id].added_services = node_values
		// console.log(arr_obj_schedules[ev.target.parentNode.parentNode.parentNode.id])
		calculatePrice();
	})
}

//*  FUNCTION FOR REMOVE SCHEDULE CONAINER
function removeSchedule(sch){
	sch.parentNode.parentNode.remove();
	qty_schedules--
	// console.log('se resto y ahora el valor es: ', qty_schedules);
}

//* FUNCTION FOR ADD SCHEDULE CONTAINER
function addedSchedule(){
	current_last_id++
	// console.log('entro');
	let cont_schedule = document.createElement('div'); //contenedor de la agenda
	cont_schedule.id=current_last_id;
	cont_schedule.classList.add('cont_schedule')
	let cont_hours = document.createElement('div'); //contenedor de botones horas
	let cont_calendar = document.createElement('div'); //contenedor del calendario
	cont_calendar.classList.add('cont_calendar')
	let cont_selectpicker	= document.createElement('div'); //contenedor para servicios adicionales
	cont_selectpicker.classList.add('cont_selectpicker')
	let cont_add_remove = document.createElement('div'); //contenedor para agrear o quitar agendas

	//* ADDED BUTONS FOR HOURS
	let el_span_hour_8 = document.createElement('span')
	let el_span_hour_14 = document.createElement('span')
	el_span_hour_8.classList.add('span_hour')
	el_span_hour_14.classList.add('span_hour')
	el_span_hour_8.innerHTML = '8:30'
	el_span_hour_14.innerHTML = '14:00'
	cont_hours.append(el_span_hour_8)
	cont_hours.append(el_span_hour_14)

	//* ADDED DATEPICKER
	let el_input_calendar =  document.createElement('input')
	el_input_calendar.type = 'text'
	el_input_calendar.readOnly = 'readonly'
	el_input_calendar.disabled = 'true'
	el_input_calendar.classList.add('datepicker')
	$(el_input_calendar).datepicker({
		startDate: tomorrow, //added by abel
		endDate: new Date(new Date().getFullYear(), new Date().getMonth()+2, 0),
		daysOfWeekDisabled: [0],
		//autoHide: false,
		autoclose: true,
		multidate: false,
		language: 'es',
		weekStart: 1,
		format: 'yyyy-mm-dd',
		// showOn: 'button',
		// buttonImageOnly: true,
		// buttonImage: "b_calendar..png",
	});
	cont_calendar.append(el_input_calendar)

	//* ADDED SELECTPICKER
	let el_select_added_services = document.createElement('select')
	el_select_added_services.multiple = true;
	options_html_added_services=''
	array_added_services.forEach(element => {
	options_html_added_services+=`<option value=${element.id}>${element.name}</option>`;
	});
	el_select_added_services.innerHTML = options_html_added_services
	el_select_added_services.classList.add('selectpicker')
	cont_selectpicker.append(el_select_added_services)


	//* ADDED BUTTON ADD / REMOVE SCHEDULES
	el_span_add_schedule = document.createElement('span')
	el_span_remove_schedule = document.createElement('span')
	if(qty_schedules == 0 ){
		el_span_add_schedule.innerHTML = '+'
		el_span_add_schedule.classList.add('added_schedule')
		//agregamos el img "?"
		// el_question = document.createElement('img');
		// el_question.src = './assets/images/img_question.gif'
		// el_question.classList.add('img_question')
		el_tooltip = document.createElement('a')
		el_tooltip.innerHTML = '<img src="./assets/images/img_question.gif" class="img_question">'
		el_tooltip.title="agregar calendario"
		el_tooltip.classList.add('tooltiptex')
		cont_add_remove.append(el_span_add_schedule)
		// cont_add_remove.append(el_question)
		cont_add_remove.append(el_tooltip)
	}else {
		el_span_remove_schedule.innerHTML = '-'
		el_span_remove_schedule.classList.add('remove_schedule')
		cont_add_remove.append(el_span_remove_schedule)
	}

	//* ADDED INTO CONTAINERS
	cont_schedule.append(cont_hours)
	cont_schedule.append(cont_calendar)
	cont_schedule.append(cont_selectpicker)
	cont_schedule.append(cont_add_remove);
	
	$(el_select_added_services).attr('disabled',true)
	initializeSelectpicker(el_select_added_services)

	qty_schedules++;
	return cont_schedule
}

