var final_price = 0;

//added by abel
var today = new Date();
var tomorrow = new Date();
tomorrow.setDate(today.getDate() + 1);

function add_service(el,id){
  var baths = $("[name='bathrooms']").val() ? $("[name='bathrooms']").val() : 0;
  var house_type = $("[name=house_types_id]").val();
  var rooms = $("[name='rooms']").val() ? $("[name='rooms']").val():0;
  if($(el).hasClass('active')){
    $("#added-service-"+id).remove();
  }
  else{
    $("<input>").attr({
      type:'hidden',
      id: 'added-service-'+id,
      name: 'work_belongstomany_added_service_relationship[]',
      value:id
    }).appendTo('#newWork')

  }
  var amount_sevices = $("[name='work_belongstomany_added_service_relationship[]']").length;
  update_price(house_type,baths,rooms,amount_sevices);
}

function handleTerms(checkbox){
  if(checkbox.checked == true){
    $("#submit").removeClass("disabled");
  }else{
    $("#submit").addClass("disabled");
  }
}

function currencyFormat(num) {
  return '$' + num.toFixed(0).replace(/(\\d)(?=(\\d{3})+(?!\\d))/g, '$1.')
}

function update_price(){
  var baths = $("[name='bathrooms']").val() ? $("[name='bathrooms']").val() : 0;
  var house_type = $("[name=house_types_id]").val() ? $("[name=house_types_id]").val():0;
  var rooms = $("[name='rooms']").val() ? $("[name='rooms']").val() : 0;

  // var rooms = $(this).val();
  var amount_sevices = $("[name='work_belongstomany_added_service_relationship[]']").length ? $("[name='work_belongstomany_added_service_relationship[]']").length : 1;

  $.ajax({
    url:'/amount_workers',
    method:'POST',
    data:{_token:$("[name=_token]").val(),house_type:house_type,baths:baths,rooms:rooms,amount_sevices:amount_sevices},
    success:function(data){
      var total = 0;
      $(".btn-additional-hora.active").each(function(index){
        if($(this)){
          total += $(this).data('price');
        }
      });
      if(data.data){
        $(".precio-resumen").html(currencyFormat(data.data['price']));
        $(".valor-total").html(currencyFormat(total + data.data['price']));
        $("[name='quantity']").val(data.data['amount_workers']);
        $("[name='price']").val(total + data.data['price']);

      }

    }
  })
}

//varias fechas por orden
var total_dates = 1
function set_dates(){
  $("#extra_dates").append('<div class="workdate" id=workdate_'+total_dates+'><div class="form-group">    <input type="text" id="dateInput-'+total_dates+'" class="form-control input-date dateInput" autocomplete=off>    <input type="hidden" id="date-'+total_dates+'" name="dates[]">  </div>  <div class="content-btn-hour"><span class="btn-building btn-hour btn-additional-other" id="'+total_dates+'-hour-8" onclick="$(&quot;input#date-'+total_dates+'&quot;).val($(&quot;input#dateInput-'+total_dates+'&quot;).val()+&quot 08:30&quot);toggleHour(this);">08:30</span>    <span class="btn-building btn-hour btn-additional-other" id="'+total_dates+'-hour-14" onclick="$(&quot;input#date-'+total_dates+'&quot;).val($(&quot;input#dateInput-'+total_dates+'&quot;).val()+&quot 14:00&quot);toggleHour(this);">14:00</span><span class="btn-building btn-add" onclick="$(this).parent().parent().remove();total_dates--;setMultiplier();" >-</span></div></div>')
  $('.dateInput').each(function(){
    $(this).datepicker({
      startDate: tomorrow, //added by abel
      endDate: new Date(new Date().getFullYear(), new Date().getMonth()+2, 0), //added by abel
      language: 'es', //added by abel
      multidate: false,
      format: 'yyyy-mm-dd',
    });
    $(this).datepicker().on('changeDate', function(e) {
      //get hour
      var number = $(e.currentTarget).attr("id").split("-")[1];
      //get hour
      var hour = $("#date-"+number).val().split(" ")[1];
      if(hour != null){
        $("#date-"+number).val(e.format(0,"yyyy-mm-dd")+" "+hour);
      }
      else{
        $("#date-"+number).val(e.format(0,"yyyy-mm-dd"));
      }
      // $("#date-"+number).val(e.format(0,"YYYY-MM-DD"))
      // `e` here contains the extra attributes
    });

  });
  total_dates++
  setMultiplier();
}

function setMultiplier(){
  $(".multiplier").html("x"+total_dates);
  total_price();
}

function set_service(service_id,name,price){
  $("input[name=service_types_id]").val(service_id)
  $(".info-pasos-room-total p").html(name)
  $(".precio-resumen").html(currencyFormat(price))

  total_price()
}

function toggleHour(el){
  if($(el).attr("id").includes('hour-8')){
    var prefix = $(el).attr("id").split("-")[0];
    $('#dateInput-'+prefix).datepicker('setDatesDisabled', disabled_dates['am']);
    var id = $(el).attr("id").replace('8','14');
    $("#"+id).removeClass('active');
  }
  else{
    var prefix = $(el).attr("id").split("-")[0];
    $('#dateInput-'+prefix).datepicker('setDatesDisabled', disabled_dates['pm']);
    var id = $(el).attr("id").replace('14','8');
    $("#"+id).removeClass('active');
  }
  $(el).toggleClass('active');
  $('.content-additional-data').fadeIn();
  $('.form-check').removeClass('disabled');
}

function total_price(){
  var price = $('.info-pasos-room-total .precio-resumen').html().replace('$', '');
  var discount = price * parseInt($('#discount').val()) / 100;
  var final_price = price - discount;
  $(".valor-total").html(currencyFormat(final_price*total_dates))
  $("[name='price']").val(final_price*total_dates);
}

function coupon(code){
  if (code != '') {
    $.ajax({
      url:'/validate_coupon',
      method:'POST',
      data:{_token:$("[name=_token]").val(),code:code},
      success:function(data){
        if (data.discount) {
          $('#discount').val(data.discount)
          $('#msg-coupon').html('Cupón válido por un '+data.discount+'% de descuento en el total')
          total_price()
        }
        else{
          $('#msg-coupon').html('Cupón inválido.')
          total_price()
        }
      }
    })
  }
}

// added by abel - cuando se cambia de type_house o  servicio, las cantidades de baños y habitasciones,  los servicios adicionales y los calendarios, se tienen que resetar
function reset_related_controls(){
  $('.content-paso-03').removeClass('disabled');
  $('.info-pasos-room-bed').removeClass('disabled');
  $('.info-pasos-room-room, .info-pasos-room-total, .text-total, .valor-total').removeClass('disabled');
  $('.btn-next, .codigo-descuento').removeClass('disabled');
  $('#services-2planchado').toggleClass('disabled');
  $('#2-planchado').toggleClass('btn-additional-disabled');
  $('#1-planchado').toggleClass('btn-additional-disabled');
  $('.info-pasos-room-plancha').toggleClass('disabled');
  $('.info-pasos-room-plancha-2').toggleClass('disabled');
  $('.info-pasos-room-refri').toggleClass('disabled');
  $('.info-pasos-room-horno').toggleClass('disabled');


}

$(document).ready(function(){
  $('.content-services, .content-additional-services, .content-additional-schedule, .content-additional-data, .content-rooms').css('visibility','hidden')
  //paso1
  $('.content-building span').on('click', function(){
    $('.content-services').css('visibility', 'visible')
    // $('.content-building').append('<div class="disabled-box"></div>')
  })
  //paso 2
  $('.content-services span.btn-building.btn-services').on('click', function(){
    $('.content-rooms').css('visibility', 'visible')
    // $('.content-services').append('<div class="disabled-box"></div>')
  })
  //paso 3
  $('.last-rooms, .first-rooms').on('change', function(){
    if ($('.first-rooms').val() + $('.last-rooms').val() > 2) {
      $('.content-additional-services').css('visibility', 'visible')
      $('.content-additional-schedule').css('visibility', 'visible')
      $('.content-additional-data').css('visibility', 'visible')
    }
  })
  //paso 4
  $('.content-btn-additional span.btn-additional').on('click', function(){
    if ($('.btn-additional.btn-additional-hora.active').length == 0) {
      $('.services-added.row').html('')
    }
    $('.services-added').html('<div class="col-lg-12"><h5>Servicios Adicionales</h5></div>')
    $('.btn-additional.btn-additional-hora.active').each(function( index ) {
      $('.services-added').append('<div class="col-lg-6">\
      <div class="info-pasos-room info-pasos-room-plancha">\
      <span>'+$(this).html()+'</span>\
      <p>$'+$( this ).data('price')+'</p>\
      </div>\
      </div>');
    });
  })
  //paso 5
  $('.content-additional-schedule').on('change', 'input.dateInput', function() {
    if ($('btn-building.active').length > 0) {
      $('.content-additional-data').css('visibility', 'visible')
      if (total_dates >= 4) {
        console.log('descuento')
        total_price()
      }
    }
  });
  $('#dateInput-0').on('focus',function(e){
    if ($('#dateInput-0').val() != '') {
      $('.form-check.form-check-inline, .content-paso.content-paso-04 , .main-date .content-btn-hour').show()
      //fake click hours
      // $('#0-hour-8').click()
    }
  })
  //calculo del total
  $('body').on('change', 'input', function() {
    if (total_dates >= 4) {
      $('.codigo-descuento').hide()
      $('.total-descuento').html('10% Descuento en el total')
      $('#discount').val(10)
      total_price()
    }
  });

  $('.btn-home').click(function(){
    $('.btn-home').removeClass('active');
    $(this).addClass('active');
    $('.content-paso-02').removeClass('disabled');
    $('.content-services').fadeIn();
  });
  $('.btn-services').click(function(){
    $('.btn-services').removeClass('active');
    $(this).addClass('active');
    $('.content-paso-03').removeClass('disabled');
    $('.content-rooms').fadeIn();
  });
  $('.first-rooms').click(function(){
    $('.content-paso-04').removeClass('disabled');
    $('.info-pasos-room-bed').removeClass('disabled');
  });
  $('.last-rooms').click(function(){
    $('.content-additional-services').fadeIn();
    $('.content-additional-schedule').fadeIn();
    $('.info-pasos-room-room, .info-pasos-room-total, .text-total, .valor-total').removeClass('disabled');
  });
  $('.form-check-input-pago').click(function(){
    $('.btn-next, .codigo-descuento').removeClass('disabled');
  });
  $('#services-1planchado').click(function(){
    $(this).toggleClass('active');
    $('#services-2planchado').toggleClass('disabled');
  });
  $('#1-planchado').click(function(){
    $('#2-planchado').toggleClass('btn-additional-disabled');
    $(this).toggleClass('active');
  });
  $('#2-planchado').click(function(){
    $('#1-planchado').toggleClass('btn-additional-disabled');
    $(this).toggleClass('active');
  });







  $('.input-date').click(function(){
    $('.content-btn-hour').fadeIn();
  });
  $('#1-planchado').click(function(){
    $('.info-pasos-room-plancha').toggleClass('disabled');
  });
  $('#2-planchado').click(function(){
    $('.info-pasos-room-plancha-2').toggleClass('disabled');
  });
  $('#limp-refri').click(function(){
    $('.info-pasos-room-refri').toggleClass('disabled');
  });
  $('#limp-horno').click(function(){
    $('.info-pasos-room-horno').toggleClass('disabled');
  });
  $('.textarea-form').click(function(){
    $('.content-register').fadeIn();
  });
  $('#label-si').click(function(){
    $('.content-password').fadeIn();
  });
  $('#label-no').click(function(){
    $('.content-password').fadeOut();
  });

  // para vista Registro
  $("#registroForm").on('submit',function(e){
    e.preventDefault();
    if($("#passInput").val() != $("#passInput_re").val()){
      e.preventDefault();
      alert("Las contraseñas tienen que coincidir");
    }
  });


  $("#newWork").on('submit',function(e){
    if($("#password").val() != $("#re_password").val()){
      e.preventDefault();
      alert("Las contraseñas tienen que ser las mismas");
    }
  });

  //Resumen servicio
  $('[data-toggle="tooltip"]').tooltip();

  $.fn.datepicker.dates['es'] = {
    days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dic"],
    today: "Hoy",
    clear: "Limpiar",
    format: "dd/mm/yyyy",
    titleFormat: "MM yyyy",
    weekStart: 1
  };
  $('.dateInput').datepicker({
    startDate: tomorrow, //added by abel
    endDate: new Date(new Date().getFullYear(), new Date().getMonth()+2, 0),
    // startDate: '1d',
    // endDate: '1m',
    daysOfWeekDisabled: [0],
    multidate: false,
    language: 'es',
    weekStart: 1,
    format: 'yyyy-mm-dd',
    datesDisabled:disabled_dates['am'],
  });

  $(".dateInput").datepicker().on('changeDate', function(e) {
    var number = $(e.currentTarget).attr("id").split("-")[1];
    //get hour
    var hour = $("#date-"+number).val().split(" ")[1];
    if(hour != null){
      $("#date-"+number).val(e.format(0,"yyyy-mm-dd")+" "+hour);
    }
    else{
      $("#date-"+number).val(e.format(0,"yyyy-mm-dd"));
    }
  });

  //Main form
  $("[name='rooms']").on('change',function(){

    $(".info-pasos-room-bed span").html($(this).val());
    update_price();
  })

  $("[name='bathrooms']").on('change',function(){
    $(".info-pasos-room-room span").html($(this).val());
    update_price();
  })

  //form validation
  jQuery.extend(jQuery.validator.messages, {
    required: "Este campo es obligatorio.",
    remote: "Por favor, rellena este campo.",
    email: "Por favor, escribe una dirección de correo válida",
    url: "Por favor, escribe una URL válida.",
    date: "Por favor, escribe una fecha válida.",
    dateISO: "Por favor, escribe una fecha (ISO) válida.",
    number: "Por favor, escribe un número entero válido.",
    digits: "Por favor, escribe sólo dígitos.",
    creditcard: "Por favor, escribe un número de tarjeta válido.",
    equalTo: "Por favor, escribe el mismo valor de nuevo.",
    accept: "Por favor, escribe un valor con una extensión aceptada.",
    maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
    minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
    rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
    range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
    max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
    min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
  });
  $("#newWork").validate({
    rules: {
      email: { required: true },
      name: { required: true },
      address: { required: true },
      city: { required: true },
      phone: { required: true, minlength:8,maxlength:8},
      name: { required: true }
    },
    messages: {
      "email": {
        required: function () { toastr.error('Debes ingresar un correo electrónico válido') },
      },
      "name": {
        required: function () { toastr.error('Debes ingresar tu nombre') },
      },
      "address": {
        required: function () { toastr.error('Debes ingresar tu dirección') },
      },
      "city": {
        required: function () { toastr.error('Debes ingresar tu comuna') },
      },
      "phone": {
        required: function () { toastr.error('Debes ingresar tu teléfono') },
      },
    }
  });
});
