@include('site.header')


    </div>


    <div class="wrap-princ">

      <div class="container">
        <div class="row">

          <div class="col-12">

            <div class="card-login">
            <form action="/orden" id="newWork" method="POST">
            {{ csrf_field() }}

              <div class="row">
                <div class="col-lg-7">
                    <div class="box-selector">
                      <h4>¡Bienvenido! Reserva tu hora aquí @if (!Auth::user()) <br><small>Si tienes una cuenta, <a href="/admin" class="login-link"> ingresa aquí </a></small>@endif</h4>

                      <div class="content-building">
                        <p>1. Elige si vas a agendar para casa o departamento</p>
                        <input type="hidden" name="house_types_id">
                        @foreach(App\HouseType::get() as $house)
                          <span class="btn-building btn-home" onclick="$(&quot;input[name=house_types_id]&quot;).val({{$house->id}})"><i class="fas fa-{{ $house->name =='Casa' ? 'home' : 'building'}}"></i></span>
                        @endforeach
                      </div>

                      <div class="content-services">
                        <p>2. Servicios</p>
                        <input type="hidden" name="service_types_id">
                        @foreach(App\Service::get() as $service)
                        <div class="content-services-btn">
                          <span class="btn-building btn-services" onclick="$(this).toggleClass('active');set_service({{$service->id}},'{{$service->name}}',{{$service->price}})">{{$service->name}}</span>
                          <button class="link-detalle" type="button" data-toggle="modal" data-target="#services_{{$service->id}}_Modal">Detalle</button>
                        </div>
                        @endforeach
                        <div class="content-services-btn">
                          <span class="btn-services-vidrios" data-toggle="modal" data-target="#servicesVidrioModal"> Limpieza<br> de vidrios</span>
                          <button class="link-detalle"></button>
                        </div>
                      </div>

                      <div class="content-rooms">
                        <p>3. Selecciona el número de habitaciones y baños <button class="link-detalle" type="button" data-toggle="modal" data-target="#services_1_Modal">Detalle</button></p>
                        <div class="row">
                        <div class="input-group mb-4">
                          <select class="form-control first-rooms" name="rooms">
                            <option value="">-</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                          </select>



                          <div class="input-group-append">
                            <i class="fas fa-bed"></i>
                          </div>
                        </div>
                        <div class="input-group mb-4">

                          <select class="form-control last-rooms" name="bathrooms">
                            <option value="">-</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                          </select>
                          <div class="input-group-append">
                            <i class="fas fa-bath"></i>
                          </div>
                        </div>
                      </div>
                      </div>

                      @php

                      $added_services = App\AddedService::get();
                      @endphp
                      <div class="content-additional-services">
                        <p>4. Servicios adicionales (Opcional)</p>
                        <button type="button" id="tooltip-additional" class="btn btn-secondary" data-toggle="tooltip" data-placement="right" title="Aquí te dejamos estimativos de cuánto se plancha en un hora. Ejemplo 1: 8 camisas. Ejemplo 2: 5 camisas, 3 poleras, 2 pantalones. Ejemplo 3: 4 camisas, 4 blusas, 3 pantalones."><i class="far fa-question-circle"></i></button>

                        <div class="content-btn-additional">
                          <ul>
                            @foreach($added_services as $services)
                            <li><span class="btn-additional btn-additional-hora" onclick="add_service(this,{{$services->id}});$(this).toggleClass('active');" data-price='{{$services->price}}'>{{$services->name}}</span></li>
                            @endforeach
                          </ul>
                        </div>
                      </div>

                      <div class="content-additional-schedule">
                        <p>5. Selecciona tu horario</p>

                        <div class="workdate" id="main-date">
                          <div class="form-group" >
                            <input type="text" id="dateInput-0" class="form-control input-date dateInput" required autocomplete=off>
                            <input type="hidden" id="date-0" name="dates[]">
                          </div>
                          <div class="content-btn-hour" style="display:none">
                            <span class="btn-building btn-hour btn-additional-other" id="0-hour-8" onclick="$(&quot;input#date-0&quot;).val($(&quot;input#dateInput-0&quot;).val()+' 08:30');toggleHour(this);">08:30</span>
                            <span class="btn-building btn-hour btn-additional-other" id="0-hour-14" onclick="$(&quot;input#date-0&quot;).val($(&quot;input#dateInput-0&quot;).val()+' 14:00');toggleHour(this);">14:00</span>

                            <span class="btn-building btn-add" onclick="javascript:set_dates()">+</span>
                          </div>
                         </div>
                        <div id="extra_dates">

                        </div>



                        <p><small>10% de descuento al agendar aseo 4 veces o más dentro de un mismo mes</small> </p>
                      </div>

                      <div class="content-additional-data">
                        <p>6. Datos Personales</p>

                        <div class="row">
                          <form>
                            <div class="form-group col-lg-6">
                              <div class="form-group">
                                <input type="text" class="form-control" id="nameInput" placeholder="Nombre y Apellido" name="name" value="@if (Auth::user()){{Auth::user()->name}}@endif" required>
                              </div>
                            </div>
                            <div class="form-group col-lg-6">
                              <div class="form-group">
                                <input type="text" class="form-control" id="addressInput" placeholder="Dirección" name="address" required>
                              </div>
                            </div>
                            <div class="form-group col-lg-6">
                              <div class="form-group">
                                <select class="form-control" name="city" id="comunaInput" required>
                                  <option value="">Seleciona una comuna</option>
                                  <option value="Providencia">Providencia</option>
                                  <option value="Ñuñoa">Ñuñoa</option>
                                  <option value="Las Condes">Las Condes</option>
                                  <option value="Vitacura">Vitacura</option>
                                </select>
                              </div>
                            </div>
                            <div class="form-group col-lg-6">
                              <div class="form-group">
                                <input type="text" class="form-control" id="phoneInput" placeholder="Teléfono" name="phone" value="@if (Auth::user()){{Auth::user()->phone}}@endif" required>
                                  <span class="placephone">+56 9 </span>
                              </div>
                            </div>
                            <div class="form-group col-lg-6">
                              <div class="form-group">
                                <input type="email" class="form-control" id="mailInput" placeholder="Email" name="email" value="@if (Auth::user()){{Auth::user()->email}}@endif" required @if (Auth::user()){{'disabled'}}@endif>
                              </div>
                            </div>
                        </div>

                      </div>

                      <div class="content-additional-data">
                        <p>7. Comentarios (opcional)</p>

                        <div class="row">
                          <div class="col-lg-12">
                            <div class="form-group">
                              <div class="form-group">
                                <textarea class="form-control textarea-form" id="exampleFormControlTextarea1" form="newWork" name='description' rows="5"></textarea>
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>
                      @php
                      if(Auth::guest())
                        {
                      @endphp
                      <div class="content-additional-data">
                        <p>8. ¿Deseas Registrarse?</p>

                        <div class="check-register">
                          <div class="form-check form-check-yn form-check-inline" id="label-si">
                            <input class="form-check-input" type="radio" name="registration" id="inlineRadio1" value="1">
                            <label class="form-check-label" for="inlineRadio1">Si</label>
                          </div>
                          <div class="form-check form-check-inline" id="label-no">
                            <input class="form-check-input" type="radio" name="registration" id="inlineRadio2" value="0">
                            <label class="form-check-label" for="inlineRadio2">No</label>
                          </div>
                        </div>

                        <div class="content-password" style="display: none">
                          <div class="row">
                            <div class="form-group col-lg-6">
                              <div class="form-group">
                                <input type="password" class="form-control" id="password" name="password" placeholder="Introduce contraseña">
                              </div>
                            </div>
                            <div class="form-group col-lg-6">
                              <div class="form-group">
                                <input type="password" class="form-control" id="re_password" name="re_password" placeholder="Repetir contraseña">
                              </div>
                            </div>
                            </form>
                          </div>

                        </div>

                      </div>
                      @php
                      }
                      @endphp
                    </div>

                </div>

                <div class="col-lg-5 p-0">
                  <div class="box-selector-green h-100">

                      <div class="content-paso content-paso-01">
                        <h3>Elige el lugar que vas a agendar</h3>
                        <p>Selecciona si es casa o departamento donde quieres que vayamos</p>
                      </div>

                      <div class="content-paso content-paso-02 ">
                        <h3>Selecciona el horario</h3>
                        <p>Selecciona el la fecha y el horario que quieres el servicio de limpieza</p>
                      </div>

                      <div class="content-paso content-paso-03 ">
                        <h3>Revisa tus datos</h3>
                        <p>Chequea tus datos personales y los datos antes solicitados</p>
                      </div>

                      <div class="content-paso content-paso-04 " style="display:none">
                        <h3>Resumen del servicio</h3>
                        <div class="content-info-pasos">
                          <div class="info-pasos-room info-pasos-room-bed ">
                            <span></span>
                            <i class="fas fa-bed"></i>
                          </div>
                          <div class="info-pasos-room info-pasos-room-room ">
                            <span></span>
                            <i class="fas fa-bath"></i>
                          </div>
                          <div class="info-pasos-room info-pasos-room-total ">
                            <p></p>
                            <span class="precio-resumen"></span>
                          </div>
                        </div>

                        <div class="content-additional-pasos">
                          <div class="row">
                            <div class="services-added row"></div>

                          </div>
                        </div>
                      </div>
                      <div class="content-paso content-paso-04 " style="display:none">
                        <h3>Resumen de pago</h3>
                        <div class="info-pasos-pago">
                          <label style="color:white;cursor:pointer;border:1px solid white;padding:4px;border-radius:4px;max-width:130px;" onclick="$('.codigo-descuento').show();$(this).remove()">¿Tienes un Cupón?</label>
                          <div class="form-group row codigo-descuento" style="display:none;">
                            <label for="codigoInput" class="col-sm-4 col-form-label cupon">Cupón de descuento</label>
                            <div class="col-sm-5">
                              <input type="text" class="form-control input-codigo" name="codigoInput" id="codigoInput">
                              <input type="hidden" class="form-control input-codigo" name="discount" id="discount" value="0">
                            </div>
                            <div class="col-sm-3">
                              <button type="button" class="btn btn-primary btn-sm" onclick="coupon($('#codigoInput').val())">Validar</button>
                            </div>
                            <div class="col-sm-12">
                              <p id="msg-coupon"></p>
                            </div>
                          </div>
                          <div class="contenedor-valor">
                            <span class="text-total ">Total</span>
                            <span class="valor-total "></span>
                          </div>

                          <span class="multiplier" style="display:none;"></span>
                          <span class="total-descuento"></span>

                          <div class="form-check form-check-inline " >
                            <input class="form-check-input form-check-input-pago" type="checkbox" id="inlineCheckbox1" value="option1" onchange="handleTerms(this);" required>
                            <label class="form-check-label" for="inlineCheckbox1">He leído y acepto políticas de Limpia Tu Casa </label>
                          </div>

                          <button type="submit" id="submit" class="btn btn-primary btn-next disabled">Pagar</button>
                          <input type="hidden" name="quantity">
                          <input type="hidden" name="price">
                        </div>

                    </form>
                  </div>
                  </div>
                </div>

              </div>
            </div>

          </div>
        </div>
      </div>



    </div>
    <!-- MODAL -->

    <div class="modal fade modal-services" id="services_1_Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Limpieza de Mantención</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Limpieza de lavaplatos, campana, interior de microondas, mesones, superficie de los muebles. Limpieza externa de aparatos eléctricos como el refrigerador, horno, otros. Aspirar, trapear o encerar el piso. Arrojar la basura.</p>
            <p>Limpieza de la ducha, tina, lavamano, inodoro, espejos y superficies de los muebles. Aspirar y trapear el piso. Arrojar la basura. Habitaciones: Hacer la cama, cambiar sábanas (si el cliente lo estima) y doblar la ropa. Aspirar y trapear el piso. Limpieza de los muebles y superficies visibles. Arrojar la basura.</p>
            <p>Otras áreas (living-comedor, loggia, pasillos, escaleras, terraza): Limpieza de los muebles y superficies visibles. Aspirar y trapear el piso. Arrojar la basura. Puedes contratar nuestros servicios adicionales: limpieza interna del horno, limpieza interna del refrigerador y planchado (1 o 2 horas).</p>
            <p>Para más informacion haz click <a href="http://www.limpiatucasa.cl/FAQ/">AQUÍ</a>.</p>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade modal-services" id="services_2_Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Aseo Profundo Desocupado</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Servicio pre y post mudanza. Una limpieza en detalle para cuando dejas el hogar que arriendas, llegas a tu nueva casa o la quieres poner a la venta. Incluye: máquinas, productos y herramientas de limpieza; limpieza muebles de cocina (interior y exterior); limpieza baldosas de cocina, loggia y baños (pisos y paredes); limpieza de ventanas siempre que no haya riesgo de caída.</p>
            <p>Para más informacion haz click <a href="http://www.limpiatucasa.cl/FAQ/">AQUÍ</a>.</p>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade modal-services" id="services_3_Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Aseo Profundo Amoblado</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>¿No has realizado en tu hogar una limpieza en detalle? Ponemos a tu dispocisión este servicio. Incluye: máquinas, productos y herramientas de limpieza; limpieza muebles de cocina (interior y exterior); limpieza baldosas de cocina, loggia y baños (pisos y paredes); limpieza de ventanas siempre que no haya riesgo de caída; mover muebles y camas...</p>
            <p>Para más informacion haz click <a href="http://www.limpiatucasa.cl/FAQ/">AQUÍ</a></p>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade modal-services" id="servicesVidrioModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Limpieza de Vidrio</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Para agendar este servicio contactarse a <a href="mailto:reserva@limpiatucasa.cl">reserva@limpiatucasa.cl</a></p>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>
@include('site.footer')
