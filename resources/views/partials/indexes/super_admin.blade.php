<style media="screen">
.resumen h2, .resumen h3{
  color: #47b68b;
}
.resumen h3{
  font-size: 15px;
}
</style>
<div class="row">
  <div class="col-lg-6 resumen">
    <div class="card shadow">
      <div class="card-header">
        <p class="d-inline">Accesos rápidos</p>
      </div>
      <div class="card-body">

        <div class="col-lg-6 text-center">
          <a href="/admin/workers/create" class="btn btn-warning">Nuevo agente</a>
        </div>
        <div class="col-lg-6 text-center">
          <a href="/admin/users?get=clients" class="btn btn-warning">Ver clientes</a>
        </div>

      </div>
    </div>
  </div>
  <div class="col-lg-6 resumen">
    <div class="card shadow">
      <div class="card-header">
        <p class="d-inline">Total de ventas</p>
      </div>
      <div class="card-body">
        @php
        $total_price = App\Work::select('price')->sum('price');
        $total_price_not = App\Work::select('price')->where('work_status_id','2')->sum('price');
      @endphp
      <div class="col-lg-12 text-center">
        <h2>Total: ${{$total_price}}</h2>
        <h3>Pagado: ${{$total_price_not}}</h3>
      </div>

    </div>
  </div>
</div>
<div class="col-lg-12">
  <div class="card shadow">
    <div class="card-header">
      <p class="d-inline">Últimas Órdenes de trabajo</p>
    </div>
    <div class="card-body">
      <table class="table" id="workers_table">
        <thead>
          <tr>
            <th scope="col">Nombre</th>
            <th scope="col">Teléfono</th>
            <th scope="col">Fecha</th>
            <th scope="col">Dirección</th>
            <th scope="col">Estado</th>
            <th scope="col">Monto</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          @php
          // $works = App\Work::get(); // commented by abel -- para mostrar las ordenes por fecha descendentemente
          $works = App\Work::orderBy('created_at', 'desc')->get();
          // $works = App\Work::where('work_status_id', '=', 2)->orderBy('created_at', 'desc')->get();
          $work_status = ['','No pagado','Aceptado','Realizado','Cancelado'];

          @endphp
          @foreach($works as $work)
            <tr>
              <td>{{$work->name}}</td>
              <td>{{$work->phone}}</td>
              <td>{{$work->date}}</td>
              <td>{{$work->address}}</td>
              <td>{{$work_status[$work->work_status_id]}}</td>
              <td>${{$work->price}}</td>
              <td><a href="/admin/works/{{ $work->id }}/edit">Ver Orden</a></td>
            </tr>
          @endforeach
        </tbody>
      </table>

    </div>
  </div>
</div>
<div class="col-lg-12">
  <div class="card shadow">
    <div class="card-header">
      <p class="d-inline">Listado Agentes de Limpieza</p>
    </div>
    <div class="card-body">
      <table class="table" id="workers_table">
        <thead>
          <tr>
            <th scope="col">Nombre</th>
            <th scope="col">Email</th>
            <th scope="col">Teléfono</th>
            <th scope="col">Fecha Último Servicios</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          @php
          $workers = App\Worker::get();
          @endphp
          @if ($workers)

            @foreach($workers as $worker)
              @php
              $last_work = $worker->works()->orderBy('date','DESC')->first();
              @endphp
              <tr>
                <td>{{$worker->name}} {{$worker->lastname}}</td>
                <td>{{$worker->email}}</td>
                <td>{{$worker->phone}}</td>
                <td>@if ($last_work){{$last_work->date}}@endif</td>
                  <td><a href="/admin/workers/{{ $worker->id }}/edit">Ver Perfil</a></td>
                </tr>
              @endforeach

            @endif
          </tbody>
        </table>

      </div>
    </div>
  </div>

  <div class="col-lg-12">
    <div class="card shadow">
      <div class="card-header">
        <p class="d-inline">Listado Clientes</p>
      </div>
      <div class="card-body">
        <table class="table" id="clients_table">
          <thead>
            <tr>
              <th scope="col">Nombre</th>
              <th scope="col">Email</th>
              <th scope="col">Teléfono</th>
              <th scope="col">Fecha Último Servicios</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            @php
            $clients = App\User::where('role_id', 2)->get();
            @endphp
            @foreach($clients as $client)
              <tr>
                <td>{{$client->name}} {{$client->lastname}}</td>
                <td>{{$client->email}}</td>
                <td>{{$client->phone}}</td>
                <td>-</td>
                <td><a href="/admin/users/{{ $client->id }}/edit">Ver Perfil</a></td>
              </tr>
            @endforeach
          </tbody>
        </table>


      </div>
    </div>
  </div>

</div>
