@php
  $worker = App\Worker::where('email','=',Auth::user()->email)->first();
  $works = $worker->works()->where('work_status_id',2)->orWhere('work_status_id',3)->get();
  $last_work = $worker->getNextWork();
@endphp

<div class="row">
    @if($last_work)
    @php
      $date = \Carbon\Carbon::parse($last_work->date);
    @endphp
    <div class="col-lg-3">
      <div class="card shadow">
        <div class="card-header">
          Próxima Visita
        </div>
        <div class="card-body text-center">
          <h6 class="title-day">{{$date->format('d M y')}}</h6>
          <h6 class="title-hour">{{$date->format('h:i')}} hrs</h6>
        </div>
      </div>
    </div>
    @endif

    <div class="col-lg-12">
      <div class="card shadow">
        <div class="card-header">
          <p class="d-inline">Listado de Trabajos</p>
        </div>
        <div class="card-body">
          <table class="table" id="works_table">
            <thead>
              <tr>
                <th scope="col">Nombre</th>
                <th scope="col">Dirección</th>
                <th scope="col">Teléfono</th>
                <th scope="col">Fecha</th>
                <th scope="col">Hora</th>
                <th scope="col">Estado</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
            @foreach($works as $work)
              @php
                $date= Carbon\Carbon::parse($work->date);
                $work_status_id = ['','', 'Aceptado','Realizado'];
              @endphp
              <tr>
                <td>{{$work->name}}</td>
                <td>{{$work->address}}</td>
                <td>{{$work->phone}}</td>
                <td>{{ $date->format('d M Y')}}</td>
                <td>{{ $date->format('H:i')}}</td>
                <td>
                  {{$work_status_id[$work->work_status_id]}}
                </td>
                <td>
                  <a href="/admin/works/{{$work->id}}">Ver orden</a>
                </td>
              </tr>
            @endforeach

            </tbody>
          </table>


        </div>
      </div>
    </div>

    <div class="col-lg-12">
      <div class="wrapper-footer">
        <div class="footer-bottom-copy">
          Limpiatucasa.cl ® Todos los derechos reservados | Contacto: reserva@limpiatucasa.cl | N° de contacto: + 56 9 79866479 |
          <a target="_self" href="http://www.limpiatucasa.cl/politicas-ltc/">Políticas LTC</a> | Desarrollado por
          <a href="http://www.lobulo.cl" target="_blank">Lóbulo</a>
        </div>
      </div>
    </div>

  </div>
