@php
$userEmail = Auth::user()->email;
$works = App\Work::where('email',$userEmail)->where('work_status_id',2)->get();
$last_work = App\Work::where('email',$userEmail)->take(1)->orderBy('date', 'ASC')->first();
@endphp

<div class="row">

  <div class="col-lg-12">
    <div class="col-lg-6">
      <div class="card shadow">
        <div class="card-header">
          Próxima Visita
        </div>
        <div class="card-body text-center">
          @if ($last_work)
            @php
              $date= Carbon\Carbon::parse($last_work->date);
            @endphp
            <h6 class="title-day">{{ $date->format('d M Y')}}</h6>
            <h6 class="title-hour">{{ $date->format('h:i')}} hrs</h6>
          @else
            No hay servicios
          @endif
        </div>
      </div>
    </div>

    <div class="col-lg-6">
      <div class="card shadow">
        <div class="card-header">
          Acceso rápido
        </div>
        <div class="card-body text-center">
          <a href="/" class="btn btn-warning"> Agendar visita </a>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-12">
    <div class="card shadow">
      <div class="card-header">
        <p class="d-inline">Listado Historico de Trabajo</p>
      </div>
      <div class="card-body">


        <table class="table" id="works_table">
          <thead>
            <tr>
              <th scope="col">Nombre</th>
              <th scope="col">Dirección</th>
              <th scope="col">Teléfono</th>
              <th scope="col">Servicio</th>
              <th scope="col">Agentes</th>
              <th scope="col">Fecha</th>
              <th scope="col">Hora</th>
              <th scope="col">Estado</th>
            </tr>
          </thead>
          <tbody>
            @foreach($works as $work)
              @php
              $date= Carbon\Carbon::parse($work->date);
              $work_status_id = ['','', 'Aceptado','Realizado'];

              @endphp
              <tr>
                <td>{{$work->name}}</td>
                <td>{{$work->address}}</td>
                <td>{{$work->phone}}</td>
                <td>{{$work->service()->first()->name}}</td>
              <td>{{join(",",$work->workers()->pluck("workers.name")->all())}}</td>
                <td>{{ $date->format('d M Y')}}</td>
                <td>{{ $date->format('H:i')}}</td>
                <td>
                  {{$work_status_id[$work->work_status_id]}}
                </td>
              </tr>
            @endforeach

          </tbody>
        </table>

      </div>
    </div>
  </div>

  <div class="col-lg-12">
    <div class="wrapper-footer">
      <div class="footer-bottom-copy">
        Limpiatucasa.cl ® Todos los derechos reservados | Contacto: reserva@limpiatucasa.cl | N° de contacto: + 56 9 79866479 |
        <a target="_self" href="http://www.limpiatucasa.cl/politicas-ltc/">Políticas LTC</a> | Desarrollado por
        <a href="http://www.lobulo.cl" target="_blank">Lóbulo</a>
      </div>
    </div>
  </div>

</div>
