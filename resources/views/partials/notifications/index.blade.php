<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <h4>¡Hola {{$name}}! </h4>
    <p>{{$text}}</p>
    <p>Saludos.</p>
    <p><small>Equipo LTC</small> </p>
    <div style="background-color:#47b68b;">
      <img src="http://www.limpiatucasa.cl/wp-content/uploads/2014/07/marca.png" alt="Limpia Tu Casa">
    </div>
  </body>
</html>
