<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <h4>Hola {{$nombreWorker}}, </h4>
    <p>{{$textWorker}}</p>
    <p>Gracias</p>
    <p><small>Equipo de Limpia tu Casa</small> </p>
  </body>
</html>
