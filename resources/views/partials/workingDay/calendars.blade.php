
@php
  setlocale(LC_ALL,"es_ES");
@endphp
<div class="row workingMonths">
  <div class="col-lg-12">
    <h3>Mes {{date("F")}}</h3>
    <div class="row">
      <div class="col-lg-6">
        <h4>Jornada 8:30 AM</h4>
        <div data-date-format="yyyy-mm-dd" class="datepick_actual_am"></div>
      </div>
      <div class="col-lg-6">
        <h4>Jornada 14:00 PM</h4>
        <div data-date-format="yyyy-mm-dd" class="datepick_actual_pm"></div>
      </div>
    </div>
  </div>
  <div class="col-lg-12">
    <h3>Mes {{date("F",strtotime("last day of next month"))}}</h3>
    <div class="row">
      <div class="col-lg-6">
        <h4>Jornada 8:30 AM</h4>
        <div data-date-format="yyyy-mm-dd" class="datepick_next_am"></div>
      </div>
      <div class="col-lg-6">
        <h4>Jornada 14:00 PM</h4>
        <div data-date-format="yyyy-mm-dd" class="datepick_next_pm"></div>
      </div>
    </div>
  </div>
</div>
