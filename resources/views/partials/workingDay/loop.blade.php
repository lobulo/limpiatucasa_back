<div id="days" class="form-group col-md-12" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                    {{ $row->slugify }}
                                    <label for="name">{{ $row->display_name }}</label>
                                    <div id="deleted">
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                        <p>{{date("F", mktime(0, 0, 0, $month, 1))}}</p>
                                        </div>
                                        <div class="col-md-6">
                                        <p>{{date("F", mktime(0, 0, 0, $month+1, 1))}}</p>
                                        </div>

                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <button id="add_day1" class="btn btn-success"><i class="voyager-plus"></i></button>
                                        </div>
                                        <div class="col-md-6">
                                            <button id="add_day2" class="btn btn-success"><i class="voyager-plus"></i></button>
                                        </div>

                                    </div>
                                    <div id="days1" class="col-md-6" >

@foreach($rows_actual as $data)   
                                    <div  class="col-md-12" id="day_left_{{$data->id}}">
                                        <div class="col-md-4">
                                                <label for="name">Día de la semana</label>
                                                <select class="form-control" name="working_day[{{$data->id}}][day_of_week]" id="day_of_week">
                                                    <option value="1" {{ ( $data->day_of_week == '1' ) ? 'selected' : '' }}>Lunes</option>
                                                    <option value="2" {{ ( $data->day_of_week == '2' ) ? 'selected' : '' }}>Martes</option>
                                                    <option value="3" {{ ( $data->day_of_week == '3' ) ? 'selected' : '' }}>Miercoles</option>
                                                    <option value="4" {{ ( $data->day_of_week == '4' ) ? 'selected' : '' }}>Jueves</option>
                                                    <option value="5" {{ ( $data->day_of_week == '5' ) ? 'selected' : '' }}>Viernes</option>
                                                </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="name">Horário:</label>
                                            <div class="input-group col-md-12">
                                                <select class="form-control" name="working_day[{{$data->id}}][start]" id="start_0" >
                                                    <option value="08:00" {{ ( $data->start == '08:30' ) ? 'selected' : '' }}>08:30</option>
                                                    <option value="14:00" {{ ( $data->start == '14:00' ) ? 'selected' : '' }}>14:00</option>
                                                </select>
                                                <!-- <input type="text" id="start_0" value="{{$data->start}}" class="form-control" name="working_day[{{$data->id}}][start]" value="09:30">
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span> -->
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="name">Eliminar:</label>
                                            <div class="input-group col-md-12">
                                               <a class="del_day btn btn-danger" onclick="remove('left',{{$data->id}})" href="javascript:;" data-id="0"><i class="voyager-x"></i></a>
                                            </div>
                                        </div>

                                    </div>
@endforeach
</div>

<div class="col-md-6" id="days2" >

@foreach($rows_next as $data)   
                                
                                        <div class="col-md-12" style="float:right !important" id="day_right_{{$data->id}}">
                                            <div class="col-md-4">
                                                    <label for="name">Día de la semana</label>
                                                    <select class="form-control" name="working_day[{{$data->id}}][day_of_week]" id="day_of_week">
                                                        <option value="1" {{ ( $data->day_of_week == '1' ) ? 'selected' : '' }}>Lunes</option>
                                                        <option value="2" {{ ( $data->day_of_week == '2' ) ? 'selected' : '' }}>Martes</option>
                                                        <option value="3" {{ ( $data->day_of_week == '3' ) ? 'selected' : '' }}>Miercoles</option>
                                                        <option value="4" {{ ( $data->day_of_week == '4' ) ? 'selected' : '' }}>Jueves</option>
                                                        <option value="5" {{ ( $data->day_of_week == '5' ) ? 'selected' : '' }}>Viernes</option>
                                                    </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="name">Horário:</label>
                                                <div class="input-group col-md-12">
                                                    <select class="form-control" name="working_day[{{$data->id}}][start]" id="start_0" >
                                                        <option value="08:00" {{ ( $data->start == '08:30' ) ? 'selected' : '' }}>08:30</option>
                                                        <option value="14:00" {{ ( $data->start == '14:00' ) ? 'selected' : '' }}>14:00</option>
                                                    </select>
                                                    <!-- <input type="text" id="start_0" value="{{$data->start}}" class="form-control" name="working_day[{{$data->id}}][start]" value="09:30">
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span> -->
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="name">Eliminar:</label>
                                                <div class="input-group col-md-12">
                                                   <a class="del_day btn btn-danger" onclick="remove('right',{{$data->id}})" href="javascript:;" data-id="0"><i class="voyager-x"></i></a>
                                                </div>
                                            </div>
    
                                        </div>
@endforeach
</div>

</div>
