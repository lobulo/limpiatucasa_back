@include('site.header')



  </div>


  <div class="wrap-princ">

    <div class="container">
      <div class="row">
        <div class="col-12">

          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="index.html">Ingreso</a></li>
              <li class="breadcrumb-item active" aria-current="page">Registro</li>
            </ol>
          </nav>

          <div class="card-login">
            <div class="row">
              <div class="col-lg-6">
                <div class="row  align-items-center">
                  <div class="col-lg-12">
                    <div class="content-info-login">
                      <div class="box-login">

                        <form method='POST' action="/registro" id="registroForm">
                        {{ csrf_field() }}
                        <div class="alert alert-danger" style="{{ (!empty($error) ? 'display:block' : 'display:none') }}">
                          {{(!empty($error) ? $error : '') }}
                        </div>
                          <div class="form-group">
                            <input type="text" class="form-control" name="name" id="nombreInput" placeholder="Nombre">
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control" name="last_name" id="nombreInput" placeholder="Apellido">
                          </div>
                          <div class="form-group">
                            <input type="email" class="form-control" name="email" id="emailInput" placeholder="Email">
                          </div>
                          <div class="form-group">
                            <input type="tel" class="form-control" name="phone" id="telefonoInput" placeholder="Teléfono">
                          </div>
                          <div class="form-group">
                            <input type="password" class="form-control" name="password" id="passInput" placeholder="Contraseña">
                          </div>
                          <div class="form-group">
                            <input type="password" class="form-control" name="re_password" id="passInput_re" placeholder="Confirmar Contraseña">
                          </div>
                          <button type="submit" class="btn btn-primary float-right">Registrate</button>
                        </form>

                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-lg-6 p-0">
                <div class="content-bg-login"></div>
              </div>

            </div>
          </div>

        </div>
      </div>
    </div>



  </div>
@include('site.footer')
