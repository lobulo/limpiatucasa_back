
<div id="footer-bottom" class="light">
  <div class="container">

    <div class="footer-bottom-inner">
      <div class="row">

        <div class="col-12">
          <div class="footer-bottom-copy">
            Limpiatucasa.cl ® Todos los derechos reservados | Contacto: reserva@limpiatucasa.cl | N° de contacto: + 56 9 79866479 |
            <a target="_self" href="http://www.limpiatucasa.cl/politicas-ltc/">Políticas LTC</a> | Desarrollado por
            <a href="http://www.lobulo.cl" target="_blank">Lóbulo</a>
            <img src="{{ URL::asset('/assets/images/logo-corfo.png') }}" alt="" width="200px">
          </div>
        </div>

      </div>
    </div>

  </div>
</div>




<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.7/js/mdb.min.js"></script>
<script src="/js/additional-methods.min.js"></script>
<script src="/assets/js/format_number.js"></script>
<script src="/assets/js/schedule.js"></script>
<script src="/assets/js/app.js"></script>
</body>
</html>
