<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Limpia Tu Casa - Servicio Limpieza Domiciliario</title>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
      <!-- <link href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet"> -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />
      <link href="/assets/css/old-css.css" rel="stylesheet">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">
      <link href="/assets/css/app.css" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.7/css/mdb.min.css" />

  </head>
  <body>

    <div id="header-wrap">

      <div id="top-header">
        <div class="container">
          <div class="row">
            <div class="col-12">

              <div class="company-info">
                <div class="info"><i class="icon fa fa-phone"></i>+ 56 9 79866479</div>
                <div class="info"><i class="icon fa fa-envelope"></i><a href="mailto:reserva@limpiatucasa.cl">reserva@limpiatucasa.cl</a></div>
              </div>

              <div class="top-header-nav">
                <ul class="social-links">
                  <li><a class="lol-facebook" target="_blank" href="https://www.facebook.com/limpiatucasa.cl" title="Facebook">Facebook</a></li>
                  <li><a class="lol-twitter" target="_blank" href="https://www.twitter.com/@limpiatucasa" title="Twitter">Twitter</a></li>
                  <li><a class="lol-instagram" href="http://instagram.com/limpiatucasa.cl/" title="Instagram">Instagram</a></li>
                </ul>

                <div class="top-header-menu block">
                  <div class="menu-traduccion-container">
                    <ul id="menu-traduccion" class="menu">
                      <li id="menu-item-819-en" class="lang-item lang-item-52 lang-item-en menu-item menu-item-type-custom menu-item-object-custom menu-item-819-en"><a href="/" hreflang="en-US">English</a></li>
                      <li id="menu-item-819-fr" class="lang-item lang-item-54 lang-item-fr menu-item menu-item-type-custom menu-item-object-custom menu-item-819-fr"><a href="/" hreflang="fr-FR">Français</a></li>
                      <li id="menu-item-819-es" class="lang-item lang-item-61 lang-item-es current-lang menu-item menu-item-type-custom menu-item-object-custom current_page_item menu-item-home menu-item-819-es"><a href="/" hreflang="es-ES">Español</a></li>
                    </ul>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>

      <header id="branding" role="banner">
        <div class="container">
          <div class="row">
            <div class="col-12">

              <div id="logo">
                <a href="/" title="Limpia Tu Casa – Servicio Limpieza Domiciliario">
                  <img src="http://www.limpiatucasa.cl/wp-content/uploads/2014/07/marca.png" alt="Limpia Tu Casa – Servicio Limpieza Domiciliario" id="desktop-logo">
                </a>
              </div>

              <nav id="nav-menu" role="navigation">
                <h3 class="assistive-text">Main menu</h3>
                <div class="skip-link">
                  <a class="assistive-text" href="#content" title="Skip to primary content">Skip to primary content</a>
                </div>
                <div class="skip-link">
                  <a class="assistive-text" href="#sidebar" title="Skip to secondary content">Skip to secondary content</a>
                </div>
                <div class="menu-menu-espanol-container">
                  <ul id="menu-menu-espanol" class="sf-menu sf-js-enabled sf-arrows">
                    <li id="menu-item-648" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-648"><a href="/">Inicio</a>
                    </li>
                    <li id="menu-item-965" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-7 current_page_item menu-item-965"><a href="/reserva/">Agendar</a></li>
                    <li id="menu-item-656" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-656"><a href="/">Quiénes Somos</a></li>
                    <li id="menu-item-657" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-657"><a href="/">¿Cómo funciona?</a></li>
                    <li id="menu-item-662" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-662"><a href="/">Comunas</a></li>
                    <li id="menu-item-660" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-660"><a href="/">FAQ</a></li>
                    <li id="menu-item-661" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-661"><a href="/">Contacto</a></li>
                  </ul>
                </div>
              </nav>

              <nav id="mobile-nav-menu" class="cart-no" role="navigation">
                <div class="mobile-nav-menu-inner" style="visibility: visible; opacity: 1;">
                  <select id="mobile-primary-menu" class="dropdownmenu hasCustomSelect" style="-webkit-appearance: menulist-button; width: 190px; position: absolute; opacity: 0; height: 34px; font-size: 10px;">
                    <option value="/">Inicio</option>
                    <option value="/reserva/">Agendar</option>
                    <option value="/">Quiénes Somos</option>
                    <option value="/">¿Cómo funciona?</option>
                    <option value="/">Comunas</option>
                    <option value="/">FAQ</option>
                    <option value="/">Contacto</option>
                  </select>
                  <span class="mobile-select dropdownmenu" style="display: inline-block;"><span class="mobile-selectInner" style="width: 160px; display: inline-block;">Inicio</span></span>
                </div>
              </nav>

            </div>
          </div>
        </div>
      </header>
