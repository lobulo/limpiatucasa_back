@include('site.header')

    </div>
    <div class="wrap-princ">
      <div class="container">
      <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))

      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div> <!-- end .flash-message -->
        <div class="row">

          <div class="col-12">

            <div class="card-login">
            {{-- <form action="/orden" id="newWork" method="POST"> --}}
            <form action="#" id="newWork" method="">

            {{ csrf_field() }}

              <div class="row">
                <div class="col-lg-7">
                    <div class="box-selector">
                      <h4>¡Bienvenido! Reserva tu hora aquí @if (!Auth::user()) <br><small>Si tienes una cuenta, <a href="/admin" class="login-link"> INGRESA AQUÍ </a></small>@endif</h4>
                      <div class="content-building">
                        <p class="step_title">(*). Elige si vas a agendar para casa o departamento</p>
                        <input type="hidden" name="house_types_id">
                        @foreach(App\HouseType::get() as $house)
                          <span class="btn-building btn-home" id="house_id_{{$house->id}}"><i class="fas fa-{{ $house->name =='Casa' ? 'home' : 'building'}}"></i>&nbsp&nbsp<small>{{$house->name}}</small></span>
                        @endforeach
                      </div>

                      <div class="content-services">
                        <p class="step_title">(*). Selecciona el  tipo de Servicio</p>
                        <input type="hidden" name="service_types_id">
                        @foreach(App\Service::get() as $service)
                          @if ($service->id != 2 && $service->id != 3 )
                            <div class="content-services-btn">
                              <span class="btn-building btn-services step" onclick="$(this).toggleClass('active');set_service({{$service->id}},'{{$service->name}}',{{$service->price}})">{{$service->name}}</span>
                              <button class="link-detalle" type="button" data-toggle="modal" data-target="#services_{{$service->id}}_Modal">Detalle</button>          
                            </div>
                          @endif
                        @endforeach
                        <div class="content-services-btn">
                          <span class="btn-services-vidrios step" data-toggle="modal" data-target="#servicesVidrioModal"> Limpieza<br> de vidrios</span>
                          <button class="link-detalle"></button>
                        </div>
                      </div>

                      <div class="content-rooms">
                        <p class="step_title">(*). Selecciona el número de habitaciones y baños {{-- <button class="link-detalle" type="button" data-toggle="modal" data-target="#services_1_Modal">Detalle</button></p> --}}
                        <div class="row">
                        <div class="input-group mb-4">
                          <select class="form-control first-rooms" name="rooms">
                            <option value="" selected= true disabled="disabled">-</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                          </select>

                          <div class="input-group-append">
                            <i class="fas fa-bed"></i>
                          </div>
                        </div>
                        <div class="input-group mb-4">

                          <select class="form-control last-rooms" name="bathrooms">
                            <option value="" selected= true disabled="disabled">-</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                          </select>
                          <div class="input-group-append">
                            <i class="fas fa-bath"></i>
                          </div>
                        </div>
                      </div>
                      </div>
                      <div class="cont_schedules">
                          <p class="step_title">(*). Selecciona el horario y Servicios Adicionales si lo requiere</p>
                          <p class="text_discount">
                            <span>Recibe 10% de descuento al agendar 4 fechas o más en una misma orden dentro de un mes calendario.</span>
                          </p>
                      </div>
                      {{-- <div class="content-additional-schedule"> 
                        <p>5. Selecciona tu horario</p>
                        <p><small>10% de descuento al agendar aseo 4 veces o más dentro de un mismo mes</small> </p>
                      </div> --}}
                      <div class="content-additional-data">
                        <p class="step_title">(*). Ingrese sus Datos Personales</p>

                        <div class="row">
                          <form>
                            <div class="form-group col-lg-6">
                              <div class="form-group">
                                <input type="text" class="form-control" id="nameInput" placeholder="Nombre y Apellido" name="name" value="@if (Auth::user()){{Auth::user()->name." ".Auth::user()->last_name}}@endif" required>
                              </div>
                            </div>
                            <div class="form-group col-lg-6">
                              <div class="form-group">
                                <input type="text" class="form-control" id="addressInput" placeholder="Dirección" name="address" required>
                              </div>
                            </div>
                            <div class="form-group col-lg-6">
                              <div class="form-group">
                                <select class="form-control" name="city" id="comunaInput" required>
                                  <option value="">Seleciona una comuna</option>
                                  {{-- <option value="Huechuraba">Huechuraba</option> --}}
                                  <option value="La Reina">La Reina</option>
                                  <option value="Providencia">Providencia</option>
                                  <option value="Ñuñoa">Ñuñoa</option>
                                  <option value="Las Condes">Las Condes</option>
                                  <option value="Santiago">Santiago</option>
                                  <option value="Vitacura">Vitacura</option>
                                </select>
                              </div>
                            </div>
                            <div class="form-group col-lg-6">
                              <div class="form-group">
                                <input type="text" class="form-control" id="phoneInput" placeholder="Teléfono" name="phone" value="@if (Auth::user()){{Auth::user()->phone}}@endif" required>
                                  <span class="placephone">+56 9 </span>
                              </div>
                            </div>
                            <div class="form-group col-lg-6">
                              <div class="form-group">
                                <input type="email" class="form-control" id="mailInput" placeholder="Email" name="email" value="@if (Auth::user()){{Auth::user()->email}}@endif" required @if (Auth::user()){{'disabled'}}@endif>
                              </div>
                            </div>
                        </div>

                      </div>

                      <div class="content-additional-data">
                        <p class="step_title">(*). Observaciones (Ej: ¿Hay mascotas? - ¿Retirar las llaves en conserjería?)</p>

                        <div class="row">
                          <div class="col-lg-12">
                            <div class="form-group">
                              <div class="form-group">
                                <textarea class="form-control textarea-form" id="observations" form="newWork" name='description' rows="5"></textarea>
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>
                      @php
                      if(Auth::guest())
                        {
                      @endphp
                      <div class="content-additional-data">
                        <p class="step_title">(*). ¿Deseas Registrarse?</p>

                        <div class="check-register">
                          <div class="form-check form-check-yn form-check-inline" id="label-si">
                            <input class="form-check-input" type="radio" name="registration" id="inlineRadio1" value="1">
                            <label class="form-check-label" for="inlineRadio1">Si</label>
                          </div>
                          <div class="form-check form-check-inline" id="label-no">
                            <input class="form-check-input" type="radio" name="registration" id="inlineRadio2" value="0">
                            <label class="form-check-label" for="inlineRadio2">No</label>
                          </div>
                        </div>

                        <div class="content-password" style="display: none">
                          <div class="row">
                            <div class="form-group col-lg-6">
                              <div class="form-group">
                                <input type="password" class="form-control" id="password" name="password" placeholder="Introduce contraseña">
                              </div>
                            </div>
                            <div class="form-group col-lg-6">
                              <div class="form-group">
                                <input type="password" class="form-control" id="re_password" name="re_password" placeholder="Repetir contraseña">
                              </div>
                            </div>
                            </form>
                          </div>

                        </div>

                      </div>
                      @php
                      }
                      @endphp
                    </div>

                </div>

                <div class="col-lg-5 p-0">
                  <div class="box-selector-green h-100">
                      <div class="content-paso content-paso-03">
                        <h3>Resumen de Servicios y Horarios</h3>
                        {{-- <p>Chequea tus datos personales y los datos antes solicitados</p> --}}
                        <div id="div_resume">
                          <div class="resume_row" id="div_resume_row_one">
                            <div class="resume_first_col"><span class="resume_column_title">Lugar: </span><span id="span_type_house" class="resume_column_value"></span></div>
                            <div class="resume_second_col"><span class="resume_column_title">Servicio: </span><span id="span_main_service" class="resume_column_value"></span></div>
                          </div>
                          <div class="resume_row" id="div_resume_row_two">
                            <div id="" class="resume_first_col"><span class="resume_column_title">Habitaciones: </span><span id="span_rooms" class="resume_column_value"></span></div>
                            <div id="" class="resume_second_col"><span class="resume_column_title">Baños: </span><span id="span_baths" class="resume_column_value"></span></div>
                          </div>
                          <div id="div_schedule" >
                              <p>Horarios:</p>
                          </div>
                          <div id="div_paid" >
                          </div>
                          <div id="div_submit">
                            <div class="form-check form-check-inline " >
                                <input class="form-check-input form-check-input-pago" type="checkbox" id="inlineCheckbox1" value="option1" onchange="handleTerms(this);">
                                <!-- <label class="form-check-label" id="label_accept_policies" for="inlineCheckbox1">--></label><a href="http://www.limpiatucasa.cl/politicas-ltc/" target="_blank"><label class="form-check-label" id="label_accept_policies">He leído y acepto políticas de Limpia Tu Casa </label></a>
                            </div>
                            <button type="" id="submit" class="btn btn-primary btn-next disabled">Reservar y Pagar</button>
                        </div>
                      </div>
                    </form>
                  </div>
                  </div>
                </div>

              </div>
            </div>

          </div>
        </div>
      </div>



    </div>
    <!-- MODAL -->

    <div class="modal fade modal-services" id="services_1_Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Limpieza de Mantención</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Limpieza de lavaplatos, campana, interior de microondas, mesones, superficie de los muebles. Limpieza externa de aparatos eléctricos como el refrigerador, horno, otros. Aspirar, trapear o encerar el piso. Arrojar la basura.</p>
            <p>Limpieza de la ducha, tina, lavamano, inodoro, espejos y superficies de los muebles. Aspirar y trapear el piso. Arrojar la basura. Habitaciones: Hacer la cama, cambiar sábanas (si el cliente lo estima) y doblar la ropa. Aspirar y trapear el piso. Limpieza de los muebles y superficies visibles. Arrojar la basura.</p>
            <p>Otras áreas (living-comedor, loggia, pasillos, escaleras, terraza): Limpieza de los muebles y superficies visibles. Aspirar y trapear el piso. Arrojar la basura. Puedes contratar nuestros servicios adicionales: limpieza interna del horno, limpieza interna del refrigerador y planchado (1 o 2 horas).</p>
            <p>Para más informacion haz click <a href="http://www.limpiatucasa.cl/FAQ/">AQUÍ</a>.</p>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade modal-services" id="services_2_Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Aseo Profundo Desocupado</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Servicio pre y post mudanza. Una limpieza en detalle para cuando dejas el hogar que arriendas, llegas a tu nueva casa o la quieres poner a la venta. Incluye: máquinas, productos y herramientas de limpieza; limpieza muebles de cocina (interior y exterior); limpieza baldosas de cocina, loggia y baños (pisos y paredes); limpieza de ventanas siempre que no haya riesgo de caída.</p>
            <p>Para más informacion haz click <a href="http://www.limpiatucasa.cl/FAQ/">AQUÍ</a>.</p>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade modal-services" id="services_3_Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Aseo Profundo Amoblado</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>¿No has realizado en tu hogar una limpieza en detalle? Ponemos a tu dispocisión este servicio. Incluye: máquinas, productos y herramientas de limpieza; limpieza muebles de cocina (interior y exterior); limpieza baldosas de cocina, loggia y baños (pisos y paredes); limpieza de ventanas siempre que no haya riesgo de caída; mover muebles y camas...</p>
            <p>Para más informacion haz click <a href="http://www.limpiatucasa.cl/FAQ/">AQUÍ</a></p>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>


    <div class="modal fade modal-services" id="services_4_Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Servicio Planchado 4hrs.</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Aquí te dejamos estimativos de cuánto se plancha en una hora.
            Ejemplo 1: 8 camisas. Ejemplo 2: 5 camisas, 3 poleras, 2 pantalones. Ejemplo 3: 4 camisas, 4 blusas, 3 pantalones</p>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade modal-services" id="servicesVidrioModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Limpieza de Vidrio</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Para agendar este servicio contactarse a <a href="mailto:reserva@limpiatucasa.cl">reserva@limpiatucasa.cl</a></p>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
      var disabled_dates=JSON.parse('{!!json_encode($disabled_dates)!!}');
      var array_services = JSON.parse('{!!json_encode($object_services)!!}'); //lo pasamos desde el controlador pero no usamos por ahora
      var array_added_services = JSON.parse('{!!json_encode($object_added_services)!!}');
      // disabled_dates['am']=[];
      // disabled_dates['pm']=[];
      // var dates  = 
      // console.log(disabled_dates);
    </script>
@include('site.footer')
