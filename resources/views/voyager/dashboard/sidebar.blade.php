@if (Auth::user()->role_id == 2)
  @php
  $userEmail = Auth::user()->email;
  $works = App\Work::where('email',$userEmail)->get();
  $last_work = App\Work::where('email',$userEmail)->take(1)->orderBy('date', 'ASC')->first();
  @endphp
@endif

<div class="navbar-sidenav">
  <div class="logo-dashboard">
    <a href="/admin">
    <img src="http://www.limpiatucasa.cl/wp-content/uploads/2014/07/marca.png" alt="Limpia Tu Casa – Servicio Limpieza Domiciliario" id="desktop-logo">
    </a>
  </div>
  <div class="sidebar-info">
    @if (Auth::user()->role_id != 1)
      <div class="name-sidebar-info">
        <img src="{{ $user_avatar }}" alt="" >
        ¡Bienvendo! <p>{{ ucwords(Auth::user()->name) }}</p>
      </div>
      <div class="contact-sidebar-info">
        <ul>
          <li>Teléfono: <strong>+56 9 {{ Auth::user()->phone }}</strong></li>
          <li>Email: <strong>{{ Auth::user()->email }}</strong></li>
          @if (Auth::user()->role_id == 2)
            @if ($last_work)
              <li>Último Servicio: <strong>{{$last_work->date}}</strong></li>
            @else
              No hay servicios
            @endif
          @endif

        </ul>
      </div>
      <a href="{{ route('voyager.users.edit', Auth::user()->id) }}"><span class="btn-edit" data-toggle="modal" data-target="#editModal">Editar</span></a>
    @endif
    @if (Auth::user()->role_id == 1)
      <h3>Administrador</h3>
      <a href="{{ route('voyager.users.edit', Auth::user()->id) }}"><span class="btn-edit" data-toggle="modal" data-target="#editModal">Editar perfíl</span></a>
    @endif
    <div class="nav-dash">
      @if (Auth::user()->role_id == 1)
        {!! menu('admin', 'admin_menu') !!}
      @endif
      <form action="{{ route('voyager.logout') }}" method="POST">
        {{ csrf_field() }}
        <button type="submit" class="btn-singout">
          Cerrar Sesión
        </button>
      </form>
      <img src="/assets/images/logo-corfo.png" alt="" style="width:200px;margin-top:50px;">
    </div>
  </div>
</div>
