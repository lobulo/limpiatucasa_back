<div class="row">

      <div class="col-lg-12">
        <div class="card shadow">
          <div class="card-header">
            <p class="d-inline">Listado Agentes de Limpieza</p>
            <p>admin</p>
          </div>
          <div class="card-body">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">Nombre</th>
                  <th scope="col">Email</th>
                  <th scope="col">Teléfono</th>
                  <th scope="col">Fecha Último Servicios</th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
                @php
                  $workers = App\Worker::get();

                  @endphp
                  @foreach($workers as $worker)
                    <tr>
                      <td>{{$worker->name}} {{$worker->lastname}}</td>
                      <td>{{$worker->email}}</td>
                      <td>{{$worker->phone}}</td>
                      <td>{{$worker->id}}</td>
                      <td><a href="/admin/workers/{{ $worker->id }}/edit">Ver Perfil</a></td>
                    </tr>
                  @endforeach

              </tbody>
            </table>

            <nav aria-label="Page navigation example">
              <ul class="pagination justify-content-center">
                <li class="page-item">
                  <a class="page-link" href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                  </a>
                </li>
                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                  <a class="page-link" href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                  </a>
                </li>
              </ul>
            </nav>

          </div>
        </div>
      </div>

      <div class="col-lg-12">
        <div class="card shadow">
          <div class="card-header">
            <p class="d-inline">Listado Clientes</p>
            <form class="form-inline float-right">
              <div class="input-group">
                <input class="form-control" type="text" placeholder="Buscar">
                <span class="input-group-append">
                  <button class="btn btn-primary" type="button">
                    <i class="fa fa-search"></i>
                  </button>
                </span>
              </div>
            </form>
          </div>
          <div class="card-body">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">Nombre</th>
                  <th scope="col">Email</th>
                  <th scope="col">Teléfono</th>
                  <th scope="col">Fecha Último Servicios</th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>David Washington</td>
                  <td>matt.thompson@gmail.com</td>
                  <td>+56912345678</td>
                  <td>10 Oct 2017</td>
                  <td><a href="clientes.html">Ver Perfil</a></td>
                </tr>
                <tr>
                  <td>David Washington</td>
                  <td>matt.thompson@gmail.com</td>
                  <td>+56912345678</td>
                  <td>10 Oct 2017</td>
                  <td><a href="clientes.html">Ver Perfil</a></td>
                </tr>
                <tr>
                  <td>David Washington</td>
                  <td>matt.thompson@gmail.com</td>
                  <td>+56912345678</td>
                  <td>10 Oct 2017</td>
                  <td><a href="clientes.html">Ver Perfil</a></td>
                </tr>

              </tbody>
            </table>

            <nav aria-label="Page navigation example">
              <ul class="pagination justify-content-center">
                <li class="page-item">
                  <a class="page-link" href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                  </a>
                </li>
                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                  <a class="page-link" href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                  </a>
                </li>
              </ul>
            </nav>

          </div>
        </div>
      </div>

    </div>
