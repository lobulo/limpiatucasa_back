
@extends('voyager.master')

@section('content')


@if (Auth::user()->role_id == 1)
  @include('partials.indexes.super_admin')

@elseif(Auth::user()->role_id == 2)
  @include('partials.indexes.user')

@elseif(Auth::user()->role_id == 4)

  @include('partials.indexes.worker')

@else
    <div class="row">

      <div class="col-lg-12">
        <div class="card shadow">
          <div class="card-header">
            <p class="d-inline">Listado Agentes de Limpieza</p>
            <form class="form-inline float-right">
              <div class="input-group">
                <input class="form-control" type="text" placeholder="Buscar">
                <span class="input-group-append">
                  <button class="btn btn-primary" type="button">
                    <i class="fa fa-search"></i>
                  </button>
                </span>
              </div>
            </form>
          </div>
          <div class="card-body">

            <nav aria-label="Page navigation example">
              <ul class="pagination justify-content-center">
                <li class="page-item">
                  <a class="page-link" href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                  </a>
                </li>
                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                  <a class="page-link" href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                  </a>
                </li>
              </ul>
            </nav>

          </div>
        </div>
      </div>

    </div>

  @endif

@stop
@section('javascript')
  <script type="text/javascript">
  // $('#workers_table').DataTable({
  //   "searching": true,
  //   "ordering": false,
  //   "info":     false,
  //   "pagingType": "full_numbers",
  //   "language":  {
  //     "search":         "Buscar",
  //     paginate: {
  //       first: " ",
  //       last: " ",
  //       next: " ",
  //       previous: " "
  //     }
  //   },
  //   "bLengthChange": false
  // });
  $('#clients_table, #works_table, #workers_table').DataTable({
    "searching": true,
    "ordering": false,
    "info":     false,
    "pagingType": "full_numbers",
    "language":  {
      "search":         "Buscar",
      paginate: {
        first: " ",
        last: " ",
        next: " ",
        previous: " "
      }
    },
    "bLengthChange": false
  });
  </script>
@stop
