<div class="navbar-sidenav">
  <div class="logo-dashboard">
    <img src="http://www.limpiatucasa.cl/wp-content/uploads/2014/07/marca.png" alt="Limpia Tu Casa – Servicio Limpieza Domiciliario" id="desktop-logo">
  </div>
  <div class="sidebar-info">
  @if (Auth::user()->role_id != 1)
      <div class="name-sidebar-info">
        <img src="{{ $user_avatar }}" alt="" >
        <p>{{ ucwords(Auth::user()->name) }}</p>
      </div>
      <div class="contact-sidebar-info">
        <ul>
          <li>Teléfono: <strong>+56 9 12345678</strong></li>
          <li>Email: <strong>{{ Auth::user()->email }}</strong></li>
          <li>Último Servicio: <strong>20 junio 2018</strong></li>
        </ul>
      </div>
      {{-- <span class="btn-edit" data-toggle="modal" data-target="#editModal">Editar</span> --}}
      <a href="{{ route('voyager.users.edit', Auth::user()->id) }}"><span class="btn-edit" data-toggle="modal" data-target="#editModal">Editar</span></a>

    @endif
    @if (Auth::user()->role_id == 1)
      <h3>Administrador</h3>
    @endif
    <div class="nav-dash">
      @if (Auth::user()->role_id == 1)
      {!! menu('admin', 'admin_menu') !!}
      @endif
      <form action="{{ route('voyager.logout') }}" method="POST">
          {{ csrf_field() }}
          <button type="submit" class="btn-singout">
            Cerrar Sesión
          </button>
      </form>
      <img src="/assets/images/logo-corfo.png" alt="" width="200px;margin-top:50px;">
    </div>
  </div>
</div>
