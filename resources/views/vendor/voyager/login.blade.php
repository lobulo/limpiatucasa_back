@include('site.header')

</div>



<div class="wrap-princ">

  <div class="container">
    <div class="row">
      <div class="col-12">

        <div class="card-login">
          <div class="row">
            <div class="col-lg-6">
              <div class="row  align-items-center">

              </div>
              <div class="content-info-login">
                <div class="box-login">


                  <form action="{{ route('voyager.login') }}" method="POST">
                    {{ csrf_field() }}

                    <div class="input-group">
                      <div class="input-group-prepend">
                        <i class="fas fa-user input-group-text"></i>
                      </div>
                      <input type="text" name="email" id="email" value="{{ old('email') }}" placeholder="{{ __('voyager::generic.email') }}" class="form-control"  aria-describedby="basic-addon1" required>
                    </div>


                    <div class="input-group">
                      <div class="input-group-prepend">
                        <i class="fas fa-lock input-group-text"></i>
                      </div>
                      <input type="password" name="password" class="form-control" placeholder="{{ __('voyager::generic.password') }}"  aria-describedby="basic-addon1" required>
                    </div>
                    

                    <button type="submit" class="btn btn-primary float-right">
                      <span class="signin">Ingresar</span>
                    </button>

                  </form>

                  <div style="clear:both"></div>

                  @if(!$errors->isEmpty())
                    <div class="alert alert-red">
                      <ul class="list-unstyled">
                        @foreach($errors->all() as $err)
                          <li>{{ $err }}</li>
                        @endforeach
                      </ul>
                    </div>
                  @endif
                {{--  
                  <div class="separator-login text-center">
                    <span> o </span>
                  </div>

                 <div class="text-center">
                    <a href="/" class="btn btn-register text-center">Ingresar como invitado</a>
                  </div> --}}

                </div>
              </div>
            </div>

            <div class="col-lg-6 p-0">
              <div class="content-bg-login"></div>
            </div>

          </div>
        </div>

      </div>
    </div>
  </div>



</div>
@include('site.footer')
