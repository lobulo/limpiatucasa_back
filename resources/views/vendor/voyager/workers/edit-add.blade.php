@extends('voyager::master')

@section('css')
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href='/css/bootstrap-clockpicker.min.css'></link>
  <link rel="stylesheet" href='/css/jquery-clockpicker.min.css'></link>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />

@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
  <h1 class="page-title">
    <i class="{{ $dataType->icon }}"></i>
    {{ __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
  </h1>
  @include('voyager::multilingual.language-selector')
@stop

@section('content')
  <div class="page-content edit-add container-fluid">
    <div class="row">
      <div class="col-md-12">

        <div class="panel panel-bordered">

          <!-- form start -->
          <form role="form"
          class="form-edit-add"
          action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
            method="POST" enctype="multipart/form-data">
            <!-- PUT Method if we are editing -->
            @if(!is_null($dataTypeContent->getKey()))
              {{ method_field("PUT") }}
            @endif

            <!-- CSRF TOKEN -->
            {{ csrf_field() }}

            <div class="panel-body">

              @if (count($errors) > 0)
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
              @endif

              <!-- Adding / Editing -->
              @php
              $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};
              @endphp

              @foreach($dataTypeRows as $row)
                <!-- GET THE DISPLAY OPTIONS -->
                @php
                $options = json_decode($row->details);
                @endphp


                @if ($options && isset($options->legend) && isset($options->legend->text))
                  <legend class="text-{{$options->legend->align or 'center'}}" style="background-color: {{$options->legend->bgcolor or '#f0f0f0'}};padding: 5px;">{{$options->legend->text}}</legend>
                @endif
                @if ($options && isset($options->formfields_custom))
                  @include('voyager::formfields.custom.' . $options->formfields_custom)


                @elseif ($row->display_name == 'Días')

                  @if (is_null($dataTypeContent->getKey()))
                    <div id="days" class="form-group col-md-12" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                      {{ $row->slugify }}
                      <label for="name">{{ $row->display_name }}</label>
                      <!-- <button id="add_day" class="btn btn-success"><i class="voyager-plus"></i></button> -->
                      <div class="row">
                        <div class="input-group col-md-12" id="day_0">
                          <div class="col-md-3">
                            <label for="name">Dia de la semana</label>
                            <select class="form-control" name="day_of_week[]" id="day_of_week">
                              <option value="1">Lunes</option>
                              <option value="2">Martes</option>
                              <option value="3">Miercoles</option>
                              <option value="4">Jueves</option>
                              <option value="5">Viernes</option>
                            </select>
                          </div>
                          <div class="col-md-3">
                            <label for="name">Horario</label>
                            <div class="input-group">
                              <select name="start[]" id="start_0" >
                                <option value="08:00" {{ ( $data->start == '08:30' ) ? 'selected' : '' }}>08:30</option>
                                <option value="14:00" {{ ( $data->start == '14:00' ) ? 'selected' : '' }}>14:00</option>
                              </select>
                              <!-- <input type="text" id="start_0" class="form-control" name="start[]" value="09:30">
                              <span class="input-group-addon">
                              <span class="glyphicon glyphicon-time"></span>
                            </span> -->
                          </div>
                        </div>
                        <div class="col-md-3">
                          <label for="name">Eliminar:</label>
                          <div class="input-group  col-md-12">
                            <a class="del_day btn btn-danger" onclick="$('#day_0').remove()" href="javascript:;" data-id="0"><i class="voyager-x"></i></a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                @else
                  @php
                  $month = date('Y-m');
                  $nextMonth = date('Y-m', strtotime('last day of next month'));
                  $working_days_actual_am = $options->model::select(DB::raw("DATE(date) as d"))->where('worker_id','=',$dataTypeContent->getKey())->where(DB::raw("date_format(date,'%Y-%m')"),'=',$month)->where(DB::raw('TIME(date)'),'=','08:30:00')->pluck('d')->toArray();
                  $working_days_actual_pm = $options->model::select(DB::raw("DATE(date) as d"))->where('worker_id','=',$dataTypeContent->getKey())->where(DB::raw("date_format(date,'%Y-%m')"),'=',$month)->where(DB::raw('TIME(date)'),'=','14:00:00')->pluck('d')->toArray();

                  $working_days_next_am = $options->model::select(DB::raw("DATE(date) as d"))->where('worker_id','=',$dataTypeContent->getKey())->where(DB::raw("date_format(date,'%Y-%m')"),'=',$nextMonth)->where(DB::raw('TIME(date)'),'=','08:30:00')->pluck('d')->toArray();
                  $working_days_next_pm = $options->model::select(DB::raw("DATE(date) as d"))->where('worker_id','=',$dataTypeContent->getKey())->where(DB::raw("date_format(date,'%Y-%m')"),'=',$nextMonth)->where(DB::raw('TIME(date)'),'=','14:00:00')->pluck('d')->toArray();

                  @endphp
                  @include('partials.workingDay.calendars')
                @endif


              @else

                <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-6" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                  {{ $row->slugify }}
                  <label for="name">{{ $row->display_name }}</label>
                  @include('voyager::multilingual.input-hidden-bread-edit-add')
                  @if($row->type == 'relationship')
                    @include('voyager::formfields.relationship')
                  @else
                    {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                  @endif

                  @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                    {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                  @endforeach
                </div>


              @endif
            @endforeach

          </div><!-- panel-body -->

          <div class="panel-footer">
            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
          </div>
        </form>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
        enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
        <input name="image" id="upload_file" type="file"
        onchange="$('#my_form').submit();this.value='';">
        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
        {{ csrf_field() }}
      </form>

    </div>
  </div>
</div>
</div>

<div class="modal fade modal-danger" id="confirm_delete_modal">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"
        aria-hidden="true">&times;</button>
        <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
      </div>

      <div class="modal-body">
        <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
        <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}
        </button>
      </div>
    </div>
  </div>
</div>
<!-- End Delete File Modal -->
@stop

@section('javascript')
  <script src='/js/bootstrap-clockpicker.min.js'></script>
  <script src='/js/jquery-clockpicker.min.js'></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>

  <script>
  var params = {}
  var $image

  var count=1

  function remove(side,id){
    $('#deleted').append("<input type='hidden' name='delete[]' value='"+id+"'>");
    $('#day_'+side+'_'+id).remove()
  }

  function formatDates(dates,format){
    var aux = [];
    dates.forEach(function(date){
      aux.push(moment(date).format(format));
    });
    return aux;
  }

  function changeAM(date,format){

  }

  $('document').ready(function () {
    $.fn.datepicker.dates['es'] = {
      days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
      daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
      daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
      months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
      monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dic"],
      today: "Hoy",
      clear: "Limpiar",
      format: "dd/mm/yyyy",
      titleFormat: "MM yyyy",
      weekStart: 0
    };

    var working_days_actual_am = JSON.parse('{!!json_encode($working_days_actual_am)!!}');
    var working_days_actual_pm = JSON.parse('{!!json_encode($working_days_actual_pm)!!}');
    var working_days_next_am = JSON.parse('{!!json_encode($working_days_next_am)!!}');
    var working_days_next_pm = JSON.parse('{!!json_encode($working_days_next_pm)!!}');



    // var working_days = ["2018-10-08","2018-10-07"];

    $(".datepick_next_pm,.datepick_next_am").datepicker({
      startDate: new Date(new Date().getFullYear(), new Date().getMonth()+1,1),
      endDate: new Date(new Date().getFullYear(), new Date().getMonth()+2,0),
      useCurrent: false,
      defaultViewDate:new Date(new Date().getFullYear(), new Date().getMonth()+1,1),
      dateFormat: 'yyyy-mm-dd',
      language: 'es',
      multidate: true,
      language: 'es',
      weekStart: 1,
      startView: "days",
      maxViewMode: "days"
    });


    $(".datepick_actual_am, .datepick_actual_pm").datepicker({
      dateFormat: 'yyyy-mm-dd',
      language: 'es',
      startDate: new Date(new Date().getFullYear(), new Date().getMonth(),1),
      endDate: new Date(new Date().getFullYear(), new Date().getMonth()+1,0),
      // defaultViewDate:new Date(new Date().getFullYear(), new Date().getMonth(),1),
      multidate: true,
      language: 'es',
      weekStart: 1,
      startView: "days",
      maxViewMode: "days"
    });



    //Set dates


    $(".datepick_actual_am").datepicker("setDates",working_days_actual_am);
    $(".datepick_actual_pm").datepicker("setDates",working_days_actual_pm);
    $(".datepick_next_am").datepicker("setDates",working_days_next_am);
    $(".datepick_next_pm").datepicker("setDates",working_days_next_pm);



    $(".form-edit-add").on('submit',function(e){
      var working_days_actual_am = formatDates($(".datepick_actual_am").datepicker("getDates"),"YYYY-MM-DD 08:30:00");
      // console.log(working_days_actual_am);
      var working_days_actual_pm = formatDates($(".datepick_actual_pm").datepicker("getDates"),"YYYY-MM-DD 14:00:00");
      // console.log($(".datepick_actual_pm").datepicker("getDates"));
      var working_days_next_am = formatDates($(".datepick_next_am").datepicker("getDates"),"YYYY-MM-DD 08:30:00");
      // console.log(working_days_next_am);
      var working_days_next_pm = formatDates($(".datepick_next_pm").datepicker("getDates"),"YYYY-MM-DD 14:00:00");
      // console.log(working_days_next_pm);
      var arr = working_days_actual_am.concat(working_days_actual_pm,working_days_next_am,working_days_next_pm);
      arr.forEach(function(date){
        $(".form-edit-add").append("<input type='hidden' name='working_days[]' value='"+date+"' />");
      });
    });


    $('.clockpicker').clockpicker({
      donetext:'Listo',
      autoclose:true,

    });


    $("#end_0").on('change input',function(){
      var id = $(this).attr('id').replace('end','start');
      if($(this).val() < $("#"+id).val()){
        alert("El horario de termino debe ser mayor que el inicio");
        $(this).val($("#"+id).val());
      }
    });

    $('.toggleswitch').bootstrapToggle();

    //Init datepicker for date fields if data-datepicker attribute defined
    //or if browser does not handle date inputs
    $('.form-group input[type=date]').each(function (idx, elt) {
      if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
        elt.type = 'text';
        $(elt).datetimepicker($(elt).data('datepicker'));
      }
    });

    @if ($isModelTranslatable)
    $('.side-body').multilingual({"editing": true});
    @endif

    $('.side-body input[data-slug-origin]').each(function(i, el) {
      $(el).slugify();
    });

    $('.form-group').on('click', '.remove-multi-image', function (e) {
      e.preventDefault();
      $image = $(this).siblings('img');

      params = {
        slug:   '{{ $dataType->slug }}',
        image:  $image.data('image'),
        id:     $image.data('id'),
        field:  $image.parent().data('field-name'),
        _token: '{{ csrf_token() }}'
      }

      $('.confirm_delete_name').text($image.data('image'));
      $('#confirm_delete_modal').modal('show');
    });

    $('#confirm_delete').on('click', function(){
      $.post('{{ route('voyager.media.remove') }}', params, function (response) {
        if ( response
          && response.data
          && response.data.status
          && response.data.status == 200 ) {

            toastr.success(response.data.message);
            $image.parent().fadeOut(300, function() { $(this).remove(); })
          } else {
            toastr.error("Error removing image.");
          }
        });

        $('#confirm_delete_modal').modal('hide');
      });
      $('[data-toggle="tooltip"]').tooltip();
    });
    </script>
  @stop
